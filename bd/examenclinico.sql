-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 04, 2022 at 03:43 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `examenclinico`
--

-- --------------------------------------------------------

--
-- Table structure for table `cita`
--

CREATE TABLE `cita` (
  `id_cit` int(11) NOT NULL,
  `identificacion_cit` varchar(15) DEFAULT NULL,
  `apellido_cit` varchar(150) DEFAULT NULL,
  `nombre_cit` varchar(150) DEFAULT NULL,
  `telefono_cit` varchar(15) DEFAULT NULL,
  `servicios_cit` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cita`
--

INSERT INTO `cita` (`id_cit`, `identificacion_cit`, `apellido_cit`, `nombre_cit`, `telefono_cit`, `servicios_cit`) VALUES
(2, '', '78', '878', '878', 'CARDIOLOGIA'),
(3, '', '', '', '', ''),
(4, '', '', '', '', ''),
(5, '', '', '', '', ''),
(6, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `cliente`
--

CREATE TABLE `cliente` (
  `id_cli` int(11) NOT NULL,
  `identificacion_cli` varchar(15) DEFAULT NULL,
  `apellido_cli` varchar(150) DEFAULT NULL,
  `nombre_cli` varchar(150) DEFAULT NULL,
  `telefono_cli` varchar(15) DEFAULT NULL,
  `direccion_cli` varchar(250) DEFAULT NULL,
  `email_cli` varchar(250) DEFAULT NULL,
  `estado_cli` varchar(25) DEFAULT NULL,
  `fk_id_hcat` int(11) DEFAULT NULL,
  `foto_cli` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cliente`
--

INSERT INTO `cliente` (`id_cli`, `identificacion_cli`, `apellido_cli`, `nombre_cli`, `telefono_cli`, `direccion_cli`, `email_cli`, `estado_cli`, `fk_id_hcat`, `foto_cli`) VALUES
(1, '0504427774', 'Valencia', 'Luis', '8980998841527', 'Santan', 'Luis23@gmail.com', 'ACTIVO', 24, 'WhatsApp Image 2022-07-16 at 8.23.29 PM.jpeg'),
(11, '0504278945', 'Valencia', 'Luis', '0998841527789', 'Santan', 'Luis23@gmail.com', 'INACTIVO', 23, 'Screenshot 2022-07-07 094306.png'),
(12, '0504427774', 'Cruz', 'Manuel', '5930998841527', 'San Jose', 'manuel@gmail.com', 'ACTIVO', 22, 'person_1.jpg'),
(13, '0504427774', 'Achig', 'Rosa Elena', '0987456123593', 'San Jose', 'rosa@gmail.com', 'ACTIVO', 25, 'image_3.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `empleado`
--

CREATE TABLE `empleado` (
  `id_emp` int(11) NOT NULL,
  `nombre_emp` varchar(150) DEFAULT NULL,
  `apellido_emp` varchar(150) DEFAULT NULL,
  `direccion_emp` varchar(250) DEFAULT NULL,
  `email_emp` varchar(250) DEFAULT NULL,
  `telefono_emp` varchar(15) DEFAULT NULL,
  `foto_emp` varchar(15) DEFAULT NULL,
  `fk_id_esp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `empleado`
--

INSERT INTO `empleado` (`id_emp`, `nombre_emp`, `apellido_emp`, `direccion_emp`, `email_emp`, `telefono_emp`, `foto_emp`, `fk_id_esp`) VALUES
(1, 'Veronica', 'Taipe', 'Guaytacama', 'vero@gmail.com', '0998841527', 'about.jpg', 5),
(2, 'Vanessa', 'Puco', 'San Jose', 'vane@gmail.com', '0998841527', 'doc-4.jpg', 7),
(3, 'jjh', 'hhjh', 'jjh', 'vane@gmail.com', '0998841527', 'bg_1.jpg', 7);

-- --------------------------------------------------------

--
-- Table structure for table `especialidadmedica`
--

CREATE TABLE `especialidadmedica` (
  `id_esp` bigint(20) UNSIGNED NOT NULL,
  `nombre_esp` varchar(150) DEFAULT NULL,
  `descripcion_esp` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `especialidadmedica`
--

INSERT INTO `especialidadmedica` (`id_esp`, `nombre_esp`, `descripcion_esp`) VALUES
(5, 'PAPAS', 'Productor'),
(7, 'ZANAHORIAS', 'Productor'),
(8, 'BROCOLI', 'Productor');

-- --------------------------------------------------------

--
-- Table structure for table `figuras`
--

CREATE TABLE `figuras` (
  `id_fig` bigint(20) UNSIGNED NOT NULL,
  `nombre_fig` varchar(150) DEFAULT NULL,
  `marca_fig` varchar(150) DEFAULT NULL,
  `dimension_fig` varchar(150) DEFAULT NULL,
  `caracteristicas_fig` varchar(150) DEFAULT NULL,
  `cantidad_fig` varchar(10) DEFAULT NULL,
  `precio_fig` decimal(5,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `figuras`
--

INSERT INTO `figuras` (`id_fig`, `nombre_fig`, `marca_fig`, `dimension_fig`, `caracteristicas_fig`, `cantidad_fig`, `precio_fig`) VALUES
(3, 'Super Saiyan 4 Vegeta5', 'TAMASHII NATIONS', '1.5 x 3.9 x 5.1 pulgadas', 'Super Saiyan 4 Vegeta, as seen in ', '30', '65.00');

-- --------------------------------------------------------

--
-- Table structure for table `hcategoria`
--

CREATE TABLE `hcategoria` (
  `id_hcat` bigint(20) UNSIGNED NOT NULL,
  `nombre_hcat` varchar(150) DEFAULT NULL,
  `descripcion_hcat` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hcategoria`
--

INSERT INTO `hcategoria` (`id_hcat`, `nombre_hcat`, `descripcion_hcat`) VALUES
(22, 'ELECTRICISTA', 'PROFESIONAL'),
(23, 'CARPINTERO', 'PROFESIONAL'),
(24, 'OBRERO', 'PROFESIONAL'),
(25, 'EMPLEADA DOMESTICA', 'PROFESIONAL');

-- --------------------------------------------------------

--
-- Table structure for table `hproducto`
--

CREATE TABLE `hproducto` (
  `id_hpro` bigint(20) UNSIGNED NOT NULL,
  `nombre_hpro` varchar(150) DEFAULT NULL,
  `cantidad_hpro` varchar(150) DEFAULT NULL,
  `precio_hpro` decimal(5,2) DEFAULT NULL,
  `descripcion_hpro` varchar(150) DEFAULT NULL,
  `fk_id_hcat` int(11) DEFAULT NULL,
  `doc_hpro` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hproducto`
--

INSERT INTO `hproducto` (`id_hpro`, `nombre_hpro`, `cantidad_hpro`, `precio_hpro`, `descripcion_hpro`, `fk_id_hcat`, `doc_hpro`) VALUES
(4, 'Procesador i7', '74', '50.40', 'intel', 22, ''),
(6, 'Procesador i7', '51', '350.50', 'intel', 22, ''),
(10, '', '', '0.00', '', 0, ''),
(11, '', '', '0.00', '', 0, ''),
(12, '', '', '0.00', '', 0, ''),
(13, '', '', '0.00', '', 0, ''),
(14, 'Procesador i8', '55', '99.98', ' hhihijijij', 22, ''),
(15, '', '', '0.00', '', 0, ''),
(16, '', '', '0.00', '', 0, ''),
(17, '', '', '0.00', '', 0, ''),
(18, 'Procesador i8', '8', '23.00', 'intel2', 22, ''),
(19, '', '', '0.00', '', 0, ''),
(20, '.,mm', '50', '50.75', 'intel', 22, ''),
(21, '', NULL, '0.00', '', 0, ''),
(22, '', NULL, '0.00', '', 0, ''),
(24, '', NULL, '0.00', '', 0, ''),
(25, '', NULL, '0.00', '', 0, ''),
(26, '', NULL, '0.00', '', 0, ''),
(27, '', NULL, '0.00', '', 0, ''),
(28, '', '', '0.00', '', 0, ''),
(4, 'Procesador i7', '74', '50.40', 'intel', 22, ''),
(6, 'Procesador i7', '51', '350.50', 'intel', 22, ''),
(10, '', '', '0.00', '', 0, ''),
(11, '', '', '0.00', '', 0, ''),
(12, '', '', '0.00', '', 0, ''),
(13, '', '', '0.00', '', 0, ''),
(14, 'Procesador i8', '55', '99.98', ' hhihijijij', 22, ''),
(15, '', '', '0.00', '', 0, ''),
(16, '', '', '0.00', '', 0, ''),
(17, '', '', '0.00', '', 0, ''),
(18, 'Procesador i8', '8', '23.00', 'intel2', 22, ''),
(19, '', '', '0.00', '', 0, ''),
(20, '.,mm', '50', '50.75', 'intel', 22, ''),
(21, '', NULL, '0.00', '', 0, ''),
(22, '', NULL, '0.00', '', 0, ''),
(24, '', NULL, '0.00', '', 0, ''),
(25, '', NULL, '0.00', '', 0, ''),
(26, '', NULL, '0.00', '', 0, ''),
(27, '', NULL, '0.00', '', 0, ''),
(28, '', '', '0.00', '', 0, ''),
(4, 'Procesador i7', '74', '50.40', 'intel', 22, ''),
(6, 'Procesador i7', '51', '350.50', 'intel', 22, ''),
(10, '', '', '0.00', '', 0, ''),
(11, '', '', '0.00', '', 0, ''),
(12, '', '', '0.00', '', 0, ''),
(13, '', '', '0.00', '', 0, ''),
(14, 'Procesador i8', '55', '99.98', ' hhihijijij', 22, ''),
(15, '', '', '0.00', '', 0, ''),
(16, '', '', '0.00', '', 0, ''),
(17, '', '', '0.00', '', 0, ''),
(18, 'Procesador i8', '8', '23.00', 'intel2', 22, ''),
(19, '', '', '0.00', '', 0, ''),
(20, '.,mm', '50', '50.75', 'intel', 22, ''),
(21, '', NULL, '0.00', '', 0, ''),
(22, '', NULL, '0.00', '', 0, ''),
(24, '', NULL, '0.00', '', 0, ''),
(25, '', NULL, '0.00', '', 0, ''),
(26, '', NULL, '0.00', '', 0, ''),
(27, '', NULL, '0.00', '', 0, ''),
(28, '', '', '0.00', '', 0, ''),
(4, 'Procesador i7', '74', '50.40', 'intel', 22, ''),
(6, 'Procesador i7', '51', '350.50', 'intel', 22, ''),
(10, '', '', '0.00', '', 0, ''),
(11, '', '', '0.00', '', 0, ''),
(12, '', '', '0.00', '', 0, ''),
(13, '', '', '0.00', '', 0, ''),
(14, 'Procesador i8', '55', '99.98', ' hhihijijij', 22, ''),
(15, '', '', '0.00', '', 0, ''),
(16, '', '', '0.00', '', 0, ''),
(17, '', '', '0.00', '', 0, ''),
(18, 'Procesador i8', '8', '23.00', 'intel2', 22, ''),
(19, '', '', '0.00', '', 0, ''),
(20, '.,mm', '50', '50.75', 'intel', 22, ''),
(21, '', NULL, '0.00', '', 0, ''),
(22, '', NULL, '0.00', '', 0, ''),
(24, '', NULL, '0.00', '', 0, ''),
(25, '', NULL, '0.00', '', 0, ''),
(26, '', NULL, '0.00', '', 0, ''),
(27, '', NULL, '0.00', '', 0, ''),
(28, '', '', '0.00', '', 0, ''),
(0, 'ETS', '50', '5.00', 'Hola', 23, 'Sistema de Ventas.pdf'),
(0, 'Sangre', '42', '4.00', 'Globulos', 24, 'Sistema de Ventas.pdf'),
(0, 'rere', '50', '5.00', 'hjh', 23, 'Sistema de Ventas.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `medico`
--

CREATE TABLE `medico` (
  `id_med` int(11) NOT NULL,
  `identificacion_med` varchar(15) DEFAULT NULL,
  `apellido_med` varchar(150) DEFAULT NULL,
  `nombre_med` varchar(150) DEFAULT NULL,
  `especialidad_med` varchar(150) DEFAULT NULL,
  `telefono_med` varchar(15) DEFAULT NULL,
  `direccion_med` varchar(250) DEFAULT NULL,
  `email_med` varchar(150) DEFAULT NULL,
  `estado_med` varchar(25) DEFAULT NULL,
  `foto_med` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pais`
--

CREATE TABLE `pais` (
  `id_pais` bigint(20) UNSIGNED NOT NULL,
  `nombre_pais` varchar(150) DEFAULT NULL,
  `estado_pais` varchar(10) DEFAULT 'ACTIVO'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pais`
--

INSERT INTO `pais` (`id_pais`, `nombre_pais`, `estado_pais`) VALUES
(1, 'Ecuador', 'ACTIVO'),
(2, 'Colombia', 'ACTIVO'),
(3, 'Canada', 'ACTIVO'),
(4, 'España', 'ACTIVO'),
(1, 'Ecuador', 'ACTIVO'),
(2, 'Colombia', 'ACTIVO'),
(3, 'Canada', 'ACTIVO'),
(4, 'España', 'ACTIVO'),
(1, 'Ecuador', 'ACTIVO'),
(2, 'Colombia', 'ACTIVO'),
(3, 'Canada', 'ACTIVO'),
(4, 'España', 'ACTIVO'),
(1, 'Ecuador', 'ACTIVO'),
(2, 'Colombia', 'ACTIVO'),
(3, 'Canada', 'ACTIVO'),
(4, 'España', 'ACTIVO');

-- --------------------------------------------------------

--
-- Table structure for table `producto2`
--

CREATE TABLE `producto2` (
  `id_pro2` bigint(20) UNSIGNED NOT NULL,
  `nombre_pro2` varchar(150) DEFAULT NULL,
  `cantidad_pro2` varchar(150) DEFAULT NULL,
  `precio_pro2` varchar(150) DEFAULT NULL,
  `descripcion2_pro` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE `usuario` (
  `id_usu` int(11) NOT NULL,
  `nombre_usu` varchar(50) NOT NULL,
  `apellido_usu` varchar(50) NOT NULL,
  `email_usu` varchar(150) DEFAULT NULL,
  `password_usu` varchar(15) DEFAULT NULL,
  `estado_usu` varchar(15) DEFAULT '1',
  `perfil_usu` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `usuario`
--

INSERT INTO `usuario` (`id_usu`, `nombre_usu`, `apellido_usu`, `email_usu`, `password_usu`, `estado_usu`, `perfil_usu`) VALUES
(13, 'Manuelo', 'Valencia', 'manuel@gmail.com', '141119', '1', 'ADMINISTRADOR'),
(24, 'Luis', 'Valencia', 'prueba@gmail.com', 'e10adc3949ba59a', '1', 'ADMINISTRADOR');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cita`
--
ALTER TABLE `cita`
  ADD PRIMARY KEY (`id_cit`);

--
-- Indexes for table `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id_cli`);

--
-- Indexes for table `empleado`
--
ALTER TABLE `empleado`
  ADD PRIMARY KEY (`id_emp`);

--
-- Indexes for table `especialidadmedica`
--
ALTER TABLE `especialidadmedica`
  ADD PRIMARY KEY (`id_esp`);

--
-- Indexes for table `figuras`
--
ALTER TABLE `figuras`
  ADD PRIMARY KEY (`id_fig`);

--
-- Indexes for table `hcategoria`
--
ALTER TABLE `hcategoria`
  ADD PRIMARY KEY (`id_hcat`);

--
-- Indexes for table `medico`
--
ALTER TABLE `medico`
  ADD PRIMARY KEY (`id_med`);

--
-- Indexes for table `producto2`
--
ALTER TABLE `producto2`
  ADD PRIMARY KEY (`id_pro2`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usu`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cita`
--
ALTER TABLE `cita`
  MODIFY `id_cit` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id_cli` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `empleado`
--
ALTER TABLE `empleado`
  MODIFY `id_emp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `medico`
--
ALTER TABLE `medico`
  MODIFY `id_med` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
