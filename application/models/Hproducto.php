<?php
    class Hproducto extends CI_Model{
      public function __construct(){
        parent::__construct();
      }
      //funcion para insertar
      public function insertar($datos){
          return $this->db->insert("hproducto",$datos);
      }
      //funcion para actualizar
      public function actualizar($id_hpro,$datos){
        $this->db->where("id_hpro",$id_hpro);
        return $this->db->update("hproducto",$datos);

      }
      //funcion para saacr el detalle de un cliente
      public function consultarPorId($id_hpro){
        $this->db->where("id_hpro",$id_hpro);
        $this->db->join("hcategoria","hcategoria.id_hcat=hproducto.fk_id_hcat");
        $hproducto=$this->db->get("hproducto");
        if($hproducto->num_rows()>0){
              return $hproducto->row();//cuando SI hay clientes
            }else{
              return false;//cuando NO hay clientes
            }
        }


      //funcion para consultar todos lo clientes
      public function consultarTodos(){
        $this->db->join("hcategoria","hcategoria.id_hcat=hproducto.fk_id_hcat");
          $listadoHproductos=$this->db->get("hproducto");
          if($listadoHproductos->num_rows()>0){
            return $listadoHproductos;//cuando SI hay clientes
          }else{
            return false;//cuando NO hay clientes
          }
      }


      public function eliminar($id_hpro){
        $this->db->where("id_hpro",$id_hpro);
        return $this->db->delete("hproducto");
      }

      public function capturarImagen($id_hpro){
       $this->db->select("foto_hpro");
       $this->db->where("id_hpro",$id_hpro);
       $this->db->from("empleado");
       $resultado = $this->db->get();
       return $resultado->row();
       }




   }//cierre de la clase



   //
 ?>
