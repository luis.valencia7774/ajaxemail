<?php
    class Hcategoria extends CI_Model{
      public function __construct(){
        parent::__construct();
      }

      public function consultarTodos(){
        $listadoHcategorias=$this->db->get("hcategoria");
        if($listadoHcategorias->num_rows() >0){
          return $listadoHcategorias;
        }else{
          return false;
        }

      }
      }
?>
