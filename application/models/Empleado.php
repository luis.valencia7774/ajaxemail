<?php
class Empleado extends CI_Model{

  public function __construct(){
    parent::__construct();
  }//cierre del constructor


public function insertar($datos){//array para el ingreso de nuevos datos
return $this->db->insert('empleado',$datos);
}//cierre de la funcion insertar

public function actualizar($id_emp,$datos){
  $this->db->where("id_emp",$id_emp);
  return $this->db->update("empleado",$datos);
}

public function consultarPorId($id_emp){
  $this->db->where("id_emp",$id_emp);
  $this->db->join("especialidadmedica","especialidadmedica.id_esp=empleado.fk_id_esp");
  $empleado=$this->db->get('empleado');
  if ($empleado->num_rows()>0) {
    return $empleado->row();
  }else{
    return false;
  }
}
//funcion consultar datos de empleados fk_id_esp
public function consultarTodos(){
$this->db->join("especialidadmedica","especialidadmedica.id_esp=empleado.fk_id_esp");
$listadoEmpleados=$this->db->get('empleado');
if($listadoEmpleados->num_rows()>0){//cuando tenemos datos
return $listadoEmpleados;
}else{//cuando no existen datos
return false;
  }
}//cierre de la funcion consultar

public function consultarTodos1(int $fk_id_esp){
  $this->db->join("especialidadmedica","especialidadmedica.id_esp=empleado.fk_id_esp");
  $listadoEmpleados=$this->db->get_where('empleado',array('fk_id_esp'=>$fk_id_esp));
  if($listadoEmpleados->num_rows()>0){//cuando tenemos datos
  return $listadoEmpleados;
  }else{//cuando no existen datos
  return false;
    }
}//cierre de la funcion consultar


//funcion eliminar datos de empleados
public function eliminar($id_emp){
  $this->db->where('id_emp',$id_emp);
  return $this->db->delete("empleado");
}//cierre de la funcion eliminar

 //editar
 public function capturarImagen($id_emp){
  $this->db->select("foto_emp");
  $this->db->where("id_emp",$id_emp);
  $this->db->from("empleado");
  $resultado = $this->db->get();
  return $resultado->row();
  }




}//cierre de la clase




 ?>
