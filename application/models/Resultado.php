<?php
    class Resultado extends CI_Model{

      public function __construct(){
        parent::__construct();
      }

      public function insertar($datos){
        return $this->db->insert('producto2',$datos);
      }

      public function actualizar($id_pro2,$datos){
        $this->db->where("id_pro2",$id_pro2);
        return $this->db->update("producto2",$datos);
       }

      public function consultarTodos(){
         $listadoProductos=$this->db->get('producto2');
         if($listadoProductos->num_rows()>0){
           return $listadoProductos;
         }else{
           return false;
         }
      }

      public function eliminar($id_pro2){
      $this->db->where('id_pro2',$id_pro2);
      return $this->db->delete("producto2");
    }//cierre de la funcion eliminar


       public function consultarPorId($id_pro2){
         $this->db->where("id_pro2",$id_pro2);
         $producto=$this->db->get('producto2');
         if($producto->num_rows()>0) {
             return $producto->row();
         }else{
           return false;
         }
       }

    }//cierre de la clase Producto

     ?>
