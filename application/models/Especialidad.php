<?php
    class Especialidad extends CI_Model{
      public function __construct(){
        parent::__construct();
      }
      //funcion para insertar
      public function insertar($datos){
          return $this->db->insert("especialidad",$datos);
      }
      //funcion para actualizar
      public function actualizar($id_med,$datos){
        $this->db->where("id_med",$id_med);
        return $this->db->update("medico",$datos);
      }
      //funcion para sacar el detalle de un cliente
      public function consultarPorId($id_med){
        $this->db->where("id_med",$id_med);

        $medico=$this->db->get("especialidad");
        if($especialidad->num_rows()>0){
          return $especialidad->row();//cuando SI hay clientes
        }else{
          return false;//cuando NO hay clientes
        }
      }
      //funcion para consultar todos lo clientes
      public function consultarTodos(){

          $listadoMedicos=$this->db->get("medico");
          if($listadoMedicos->num_rows()>0){
            return $listadoMedicos;//cuando SI hay clientes
          }else{
            return false;//cuando NO hay clientes
          }
      }

      public function eliminar($id_med){
        $this->db->where("id_med",$id_med);
        return $this->db->delete("medico");
      }


   }//cierre de la clase



   //
 ?>
