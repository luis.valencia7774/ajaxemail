<?php
    class Cliente extends CI_Model{
      public function __construct(){
        parent::__construct();
      }
      //funcion para insertar
      public function insertar($datos){
          return $this->db->insert("cliente",$datos);
      }
      //funcion para actualizar
      public function actualizar($id_cli,$datos){
        $this->db->where("id_cli",$id_cli);
        return $this->db->update("cliente",$datos);

      }
      //funcion para saacr el detalle de un cliente
      public function consultarPorId($id_cli){
        $this->db->where("id_cli",$id_cli);
        $this->db->join("hcategoria","hcategoria.id_hcat=cliente.fk_id_hcat");
        $cliente=$this->db->get("cliente");
        if($cliente->num_rows()>0){
              return $cliente->row();//cuando SI hay clientes
            }else{
              return false;//cuando NO hay clientes
            }
        }

      //funcion para consultar todos lo clientes
      public function consultarTodos(){
        $this->db->join("hcategoria","hcategoria.id_hcat=cliente.fk_id_hcat");
        $listadoClientes=$this->db->get("cliente");
        if($listadoClientes->num_rows()>0){
          return $listadoClientes;//cuando SI hay clientes
        }else{
          return false;//cuando NO hay clientes
        }
      }

      public function consultarTodos1(int $fk_id_hcat){
        $this->db->join("hcategoria","hcategoria.id_hcat=cliente.fk_id_hcat");
        $listadoClientes=$this->db->get_where('cliente',array('fk_id_hcat'=>$fk_id_hcat));
        if($listadoClientes->num_rows()>0){//cuando tenemos datos
        return $listadoClientes;
        }else{//cuando no existen datos
        return false;
          }
      }//cierre de la funcion consultar


      public function eliminar($id_cli){
        $this->db->where("id_cli",$id_cli);
        return $this->db->delete("cliente");
      }
      public function capturarImagen($id_emp){
       $this->db->select("foto_emp");
       $this->db->where("id_emp",$id_emp);
       $this->db->from("empleado");
       $resultado = $this->db->get();
       return $resultado->row();
       }

       public function obtenerClientesPorEstado($estado){
         $this->db->
       }



   }//cierre de la clase



   //
 ?>
