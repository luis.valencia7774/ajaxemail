<?php
class EspecialidadMedica extends CI_Model{
  public function __construct(){
    parent::__construct();
  }//cierre del constructor

  public function insertar($datos){
    return $this->db->insert('especialidadmedica',$datos);
  }//cierre de la funcion insertar

  public function consultarTodos(){
    $listadoEspecialidadesM=$this->db->get('especialidadmedica');
    if($listadoEspecialidadesM->num_rows()>0){
      return $listadoEspecialidadesM;
    }else{
      return false;
    }
  }//cierre de la funcion consultar

public function eliminar($id_esp){
  $this->db->where('id_esp',$id_esp);
  return $this->db->delete("especialidadmedica");
}//cierre de la funcion eliminar
public function actualizar($id_esp,$datos){
   $this->db->where("id_esp",$id_esp);
   return $this->db->update("especialidadmedica",$datos);
   }
   public function consultarPorId($id_esp){
     $this->db->where("id_esp",$id_esp);
     $especialidad=$this->db->get('especialidadmedica');
     if ($especialidad->num_rows()>0) {
         return $especialidad->row();
     }else{
       return false;
     }

   }
}//cierre de la clase Producto


 ?>
