<?php
    class Departamento extends CI_Model{
      public function __construct(){
        parent::__construct();
      }
      //funcion para insertar
      public function insertar($datos){
          return $this->db->insert("departamento",$datos);
      }
      //funcion para actualizar
      public function actualizar($id_dep,$datos){
        $this->db->where("id_dep",$id_dep);
        return $this->db->update("departamento",$datos);
      }
      //funcion para sacar el detalle de un cliente
      public function consultarPorId($id_dep){
        $this->db->where("id_dep",$id_dep);

        $departamento=$this->db->get("departamento");
        if($departamento->num_rows()>0){
          return $departamento->row();//cuando SI hay clientes
        }else{
          return false;//cuando NO hay clientes
        }
      }
      //funcion para consultar todos lo clientes
      public function consultarTodos(){

          $listadoDepartamentos=$this->db->get("departamento");
          if($listadoDepartamentos->num_rows()>0){
            return $listadoDepartamentos;//cuando SI hay clientes
          }else{
            return false;//cuando NO hay clientes
          }
      }

      public function eliminar($id_dep){
        $this->db->where("id_dep",$id_dep);
        return $this->db->delete("departamento");
      }


   }//cierre de la clase



   //
 ?>
