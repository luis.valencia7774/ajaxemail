<?php
    class Cita extends CI_Model{
      public function __construct(){
        parent::__construct();
      }
      //funcion para insertar
      public function insertar($datos){
          return $this->db->insert("cita",$datos);
      }
      //funcion para actualizar
      public function actualizar($id_cit,$datos){
        $this->db->where("id_cit",$id_cit);
        return $this->db->update("cita",$datos);
      }
      //funcion para sacar el detalle de un cliente
      public function consultarPorId($id_cit){
        $this->db->where("id_cit",$id_cit);

        $cita=$this->db->get("cita");
        if($cita->num_rows()>0){
          return $cita->row();//cuando SI hay clientes
        }else{
          return false;//cuando NO hay clientes
        }
      }
      //funcion para consultar todos lo clientes
      public function consultarTodos(){

          $listadoCitas=$this->db->get("cita");
          if($listadoCitas->num_rows()>0){
            return $listadoCitas;//cuando SI hay clientes
          }else{
            return false;//cuando NO hay clientes
          }
      }

      public function eliminar($id_cit){
        $this->db->where("id_cit",$id_cit);
        return $this->db->delete("cita");
      }


   }//cierre de la clase



   //
 ?>
