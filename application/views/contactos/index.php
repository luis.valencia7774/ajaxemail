<section class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_1.jpg');" data-stellar-background-ratio="0.5">
     <div class="overlay"></div>
     <div class="container">
       <div class="row no-gutters slider-text align-items-center justify-content-center">
         <div class="col-md-9 ftco-animate text-center">
           <h1 class="mb-2 bread">Contactanos</h1>
           <p class="breadcrumbs"><span class="mr-2"><a href="<?php echo site_url(); ?>">Inicio <i class="ion-ios-arrow-forward"></i></a></span> <span>Contactos <i class="ion-ios-arrow-forward"></i></span></p>
         </div>
       </div>
     </div>
   </section>

   <section class="ftco-section ftco-no-pt ftco-no-pb contact-section">
     <div class="container">
       <div class="row d-flex align-items-stretch no-gutters">
         <div class="col-md-6 p-4 p-md-5 order-md-last bg-light">
           <h1>Laboratorio CLinico San Andres</h1>
         </div>
         <div class="col-md-6 d-flex align-items-stretch">
           <div id="map"></div>
         </div>
       </div>
     </div>
   </section>

   <section class="ftco-section contact-section">
     <div class="container">
       <div class="row d-flex mb-5 contact-info">
         <div class="col-md-12 mb-4">
           <h2 class="h4">Informacion de Contactos</h2>
         </div>
         <div class="w-100"></div>
         <div class="col-md-3 d-flex">
           <div class="bg-light d-flex align-self-stretch box p-4">
             <p><span>Direccion:</span> Cotopaxi, Latacunga, Calle 12 de Noviembre
</p>
           </div>
         </div>
         <div class="col-md-3 d-flex">
           <div class="bg-light d-flex align-self-stretch box p-4">
             <p><span>Celular:</span> <a href="tel://1234567920">0998841527</a></p>
           </div>
         </div>
         <div class="col-md-3 d-flex">
           <div class="bg-light d-flex align-self-stretch box p-4">
             <p><span>Correo Electronico:</span> <a href="">labsanandres@gmail.com</a></p>
           </div>
         </div>

       </div>
     </div>
   </section>
