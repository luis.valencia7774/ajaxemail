<br>
<center>
  <h2>Especialidades Medicas</h2>
</center>
<hr>
<br>
<div class="row" style=" margin: 0 20px 0 20px;">

  <div class="col-md-6 text-center" >
    <br>
     <button class="btn btn-primary btn-lg"><a href="<?php echo site_url(); ?>" style=" color:white;"><i class="fa fa-angle-left"> Volver </i></a> </button>
   </div>

  <div class="col-md-6 text-center" style="padding-top:30px;">

    <button class="btn btn-primary btn-lg"> <a href="<?php echo site_url(); ?>/EspecialidadesMedicas/nuevo " style=" color:white;"> <i class="fa fa-plus"> Agregar </i> </a> </button>
  </div>

</div>
<hr>

<?php if ($listadoEspecialidadesM): ?>

  <table class="table table-hover" id="tbl-esp">
    <thead>
    <tr>
      <th class="text-center">ID</th>
      <th class="text-center">NOMBRE</th>
      <th class="text-center">DESCRIPCION</th>
      <th class="text-center">OPCIONES</th>
    </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoEspecialidadesM->result() as $filaEspecialidad): ?>
        <tr>
          <td class="text-center"> <?php echo $filaEspecialidad->id_esp; ?></td>
          <td class="text-center"> <?php echo $filaEspecialidad->nombre_esp; ?></td>
          <td class="text-center"> <?php echo $filaEspecialidad->descripcion_esp; ?></td>

          <td class="text-center">
            <a onclick="confirmarEliminacion('<?php echo $filaEspecialidad->id_esp; ?>')" class="btn btn-danger"><i class="fa fa-trash"></i></a>
            <a   class="btn btn-success"  href="<?php echo site_url() ?>/especialidadesmedicas/editar/<?php echo $filaEspecialidad->id_esp; ?>"><i class="fa fa-pen"></i></a>

             </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>

<?php else: ?>
  <div class="alert alert-danger">
    <h3>No se encontraron Especialidades registradas</h3>

  </div>
<?php endif; ?>
<script type="text/javascript">
    function confirmarEliminacion(id_esp){
          iziToast.question({
              timeout: 20000,
              close: false,
              overlay: true,
              displayMode: 'once',
              id: 'question',
              zindex: 999,
              title: 'CONFIRMACIÓN',
              message: '¿Esta seguro de eliminar la Especialidad Medica de forma pernante?',
              position: 'center',
              buttons: [
                  ['<button><b>SI</b></button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                      window.location.href=
                      "<?php echo site_url(); ?>/especialidadesmedicas/Eliminacion/"+id_esp;

                  }, true],
                  ['<button>NO</button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                  }],
              ]
          });
    }
</script>
<script type="text/javascript">
  $("#tbl-esp").DataTable();
</script>
