<div class="row">
  <div class="col-md-12 text-center">
    <b style="color:#7485e1;"><h3>Editar Especialidad Medica</h3></b>
    <hr>
  </div>
  </div>

  <div class="row">
    <div class="col-md-12" style="">
      <form  class="" id="frm_nuevo_esp"  method="post" enctype="multipart/form-data"action="<?php echo site_url(); ?>/especialidadesmedicas/procesarActualizacion" >
        <br>
        <input  type="hidden" name="id_esp"  id="id_esp"value="<?php echo $especialidad->id_esp;?>">
        <br>
        <label  for="">Nombre:</label>
        <br>
        <input value="<?php echo $especialidad->nombre_esp; ?>"class="form-control " required="required" type="text"  name="nombre_esp" id="nombre_esp" placeholder="Ingrese el nombre" value="">
         <br>
       <label for="">Descripción:</label>
       <br>
       <input value="<?php echo $especialidad->descripcion_esp; ?>" class="form-control" required="required"  type="text"name="descripcion_esp" id="descripcion_esp"  placeholder="Ingrese la descripción del especialidad"value="">
       <br>

  <button class="btn btn-warning" type="submit" name="button">Guardar</button>
  <a class="btn btn-primary" type="submit" href="<?php echo site_url(); ?>/especialidadesmedicas/index">Cancelar</a>

  <br>
  <br>

  </form>
    </div>

    <script type="text/javascript">
    $("#frm_nuevo_esp").validate({
      rules:{
        nombre_esp:{
          requerid:true
        },
        descripcion_esp:{
          requerid:true
        }
      },
      messages:{
        nombre_esp:{
          requerid:"Ingrese el nombre de la especialidad"
        },
        descripcion_esp:{
          requerid:"Ingrese la descripcion"
        }
      }
    })

    </script>
