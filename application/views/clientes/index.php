<br>
<div class="row">
  <div class="col-md-12 text-center">
    <h2>LISTADO DE PROFESIONALES</h2>
  </div>
</div>

<div class="row" style=" margin: 0 20px 0 20px;">

  <div class="col-md-6 text-center" >
    <br>
     <button class="btn btn-primary btn-lg"><a href="<?php echo site_url(); ?>" style=" color:white;"><i class="fa fa-angle-left"> Volver </i></a> </button>
   </div>

  <div class="col-md-6 text-center" style="padding-top:30px;">

    <button class="btn btn-primary btn-lg"> <a href="<?php echo site_url(); ?>/clientes/nuevo " style=" color:white;"> <i class="fa fa-plus"> Agregar </i> </a> </button>
  </div>

</div>
<br>

<?php if ($listadoClientes): ?>

  <table class="table" id="tbl-clientes">
    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">FOTO</th>
        <th class="text-center">IDENTIFICACIÓN</th>
        <th class="text-center">APELLIDO</th>
        <th class="text-center">NOMBRE</th>
        <th class="text-center">TELEFONO</th>
        <th class="text-center">DIRECCION</th>
        <th class="text-center">EMAIL</th>
        <th class="text-center">ESTADO</th>
        <th class="text-center">PROFESION</th>
        <th class="text-center">OPCIONES</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoClientes->result() as $filaTemporal): ?>
        <tr>
          <td class="text-center">
            <?php echo $filaTemporal->id_cli;?>
          </td>
          <td class="text-center">
            <?php if ($filaTemporal->foto_cli!=""): ?>
              <img src="<?php echo base_url(); ?>/uploads/clientes/<?php echo $filaTemporal->foto_cli; ?>" height="80px"
                width="100px"
                alt="">
            <?php else: ?>
                N/A
            <?php endif; ?>
         </td>
          <td class="text-center">
            <?php echo $filaTemporal->identificacion_cli;?>
          </td>
          <td class="text-center">
          <?php echo $filaTemporal->apellido_cli;?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->nombre_cli;?>
          </td>
          <td class="text-center">
          <?php echo $filaTemporal->telefono_cli;?>
          </td>
          <td class="text-center">
          <?php echo $filaTemporal->direccion_cli;?>
          </td>
          <td class="text-center">
          <?php echo $filaTemporal->email_cli;?>
          </td>

          <td class="text-center">

            <?php if ($filaTemporal->estado_cli=="Activo"): ?>
              <div class="alert-danger">
                <?php echo $filaTemporal->estado_cli;?>
              </div>
            <?php else: ?>
              <div class="alert-primary">
                <?php echo $filaTemporal->estado_cli;?>
              </div>
            <?php endif; ?>

          </td>

          <td class="text-center">
            <?php echo $filaTemporal->nombre_hcat; ?>
          </td>


          <td class="text-center">
            <a href="<?php echo site_url(); ?>/clientes/editar/<?php echo $filaTemporal->id_cli; ?>" class="btn btn-primary"> <i class="fa fa-pen"> </i> </a>
            <a onclick="confirmarEliminacion('<?php echo $filaTemporal->id_cli ?>')" class="btn btn-danger"> <i class="fa fa-trash"> </i> </a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>

<?php else: ?>
  <div class="alert alert-damger">
    <h3>NO SE ENCONTRARON PACIENTES REGISTRADOS</h3>
  </div>

<?php endif; ?>
<br><br>

<script type="text/javascript">
    function confirmarEliminacion(id_cli){
          iziToast.question({
              timeout: 20000,
              close: false,
              overlay: true,
              displayMode: 'once',
              id: 'question',
              zindex: 999,
              title: 'CONFIRMACIÓN',
              message: '¿Esta seguro de eliminar el paciente de forma pernante?',
              position: 'center',
              buttons: [
                  ['<button><b>SI</b></button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                      window.location.href=
                      "<?php echo site_url(); ?>/clientes/procesarEliminacion/"+id_cli;

                  }, true],
                  ['<button>NO</button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                  }],
              ]
          });
    }
</script>
<script type="text/javascript">
  $('#tbl-clientes').DataTable();
</script>
