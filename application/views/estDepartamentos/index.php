

<section class="ftco-section">
  <div class="container">
    <div class="row justify-content-center mb-5 pb-2">
      <div class="col-md-8 text-center heading-section ftco-animate">
        <span class="subheading">Departmentos</span>
        <h2 class="mb-4">Departmentos Clinicos</h2>
      
      </div>
    </div>
    <div class="ftco-departments">
      <div class="row">
        <div class="col-md-12 nav-link-wrap">
          <div class="nav nav-pills d-flex text-center" id="v-pills-tab" role="tablist" aria-orientation="vertical">
            <a class="nav-link ftco-animate active" id="v-pills-1-tab" data-toggle="pill" href="#v-pills-1" role="tab" aria-controls="v-pills-1" aria-selected="true">Hematología</a>

            <a class="nav-link ftco-animate" id="v-pills-2-tab" data-toggle="pill" href="#v-pills-2" role="tab" aria-controls="v-pills-2" aria-selected="false">Medicina</a>

            <a class="nav-link ftco-animate" id="v-pills-3-tab" data-toggle="pill" href="#v-pills-3" role="tab" aria-controls="v-pills-3" aria-selected="false">Genética</a>

            <a class="nav-link ftco-animate" id="v-pills-4-tab" data-toggle="pill" href="#v-pills-4" role="tab" aria-controls="v-pills-4" aria-selected="false">Microbiología</a>

            <a class="nav-link ftco-animate" id="v-pills-5-tab" data-toggle="pill" href="#v-pills-5" role="tab" aria-controls="v-pills-5" aria-selected="false">Microbiología</a>

          </div>
        </div>
        <div class="col-md-12 tab-wrap">

          <div class="tab-content bg-light p-4 p-md-5 ftco-animate" id="v-pills-tabContent">

            <div class="tab-pane py-2 fade show active" id="v-pills-1" role="tabpanel" aria-labelledby="day-1-tab">
              <div class="row departments">
                <div class="col-lg-4 order-lg-last d-flex align-items-stretch">
                  <div class="img d-flex align-self-stretch" style="background-image: url(assets/images/dept-1.jpg);"></div>
                </div>
                <div class="col-lg-8">
                  <h2> Deparmento de Hematologia</h2>
                  <p>LAB San Andres, es una organización sin fines de lucro, y el dinero recaudado con la publicidad en Internet apoya nuestra misión. Mayo Clinic no respalda ningún producto ni servicios de terceros que se anuncien.</p>

                </div>
              </div>
            </div>

            <div class="tab-pane fade" id="v-pills-2" role="tabpanel" aria-labelledby="v-pills-day-2-tab">
              <div class="row departments">
                <div class="col-lg-4 order-lg-last d-flex align-items-stretch">
                  <div class="img d-flex align-self-stretch" style="background-image: url(assets/images/dept-2.jpg);"></div>
                </div>
                <div class="col-md-8">
                  <h2>Deparmento de Medicina</h2>
                  <p>LAB San Andres, es una organización sin fines de lucro, y el dinero recaudado con la publicidad en Internet apoya nuestra misión. Mayo Clinic no respalda ningún producto ni servicios de terceros que se anuncien.</p>

                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="v-pills-3" role="tabpanel" aria-labelledby="v-pills-day-3-tab">
              <div class="row departments">
                <div class="col-lg-4 order-lg-last d-flex align-items-stretch">
                  <div class="img d-flex align-self-stretch" style="background-image: url(assets/images/dept-3.jpg);"></div>
                </div>
                <div class="col-md-8">
                  <h2>Deparmento de Genetica</h2>
                  <p>LAB San Andres, es una organización sin fines de lucro, y el dinero recaudado con la publicidad en Internet apoya nuestra misión. Mayo Clinic no respalda ningún producto ni servicios de terceros que se anuncien.</p>

                </div>
              </div>
            </div>

            <div class="tab-pane fade" id="v-pills-4" role="tabpanel" aria-labelledby="v-pills-day-4-tab">
              <div class="row departments">
                <div class="col-lg-4 order-lg-last d-flex align-items-stretch">
                  <div class="img d-flex align-self-stretch" style="background-image: url(assets/images/dept-4.jpg);"></div>
                </div>
                <div class="col-md-8">
                  <h2>Deparmento de Microbiologia</h2>
                  <p>LAB San Andres, es una organización sin fines de lucro, y el dinero recaudado con la publicidad en Internet apoya nuestra misión. Mayo Clinic no respalda ningún producto ni servicios de terceros que se anuncien.</p>

                </div>
              </div>
            </div>

            <div class="tab-pane fade" id="v-pills-5" role="tabpanel" aria-labelledby="v-pills-day-5-tab">
              <div class="row departments">
                <div class="col-lg-4 order-lg-last d-flex align-items-stretch">
                  <div class="img d-flex align-self-stretch" style="background-image: url(assets/images/dept-5.jpg);"></div>
                </div>
                <div class="col-md-8">
                  <h2>Deparmento de Microbiologia</h2>
                  <p>LAB San Andres, es una organización sin fines de lucro, y el dinero recaudado con la publicidad en Internet apoya nuestra misión. Mayo Clinic no respalda ningún producto ni servicios de terceros que se anuncien.</p>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
