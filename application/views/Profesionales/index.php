
<?php if ($listadoClientes): ?>

      <?php foreach ($listadoClientes->result() as $filaEmpleado): ?>

      <div class="row">
        <div class="col-md-4 form-group" style="padding:20px;">

                  <br>
                  <input type="hidden" class="filtro-laboratorio" value="SMART GRID">

                  <div class="card" style="border-radius:20px !important;">
                                  <a href="ojo">
                        <img class="card-img-top" src="<?php echo base_url(); ?>/uploads/empleados/<?php echo $filaEmpleado->foto_cli; ?>" alt="" style="border-top-left-radius:20px; border-top-right-radius: 20px;" width="200px" height="220px">
                      </a>
                                <div class="card-body text-center" style="margin-top:-5px ;">
                      <h5 class="card-title" style="color:#312783; font-weight:bold;"><?php echo $filaEmpleado->nombre_cli; ?>
                      <?php echo $filaEmpleado->apellido_cli; ?></h5>
                      <!-- <p class="card-text">El laboratorio de Smart Grid cuenta con módulos de ultima tecnología, capaces de simular los diferentes sistemas de producción energética, su operación según las condiciones posibles en un medio normal, así también su correcta forma de interconexión con la red nacional y de igual forma ente las diferentes tipos de generadoras eléctricas.</p> -->

                      <div class="row">

                        <div class="table-responsive">
                          <table class="table">
                            <tbody><tr style="border:none;">

                              <td style="border:none;">
                                <?php if ($filaEmpleado->foto_cli!=""): ?>

                                  <center><img src="<?php echo base_url(); ?>/uploads/empleados/<?php echo $filaEmpleado->foto_cli; ?>" alt="" style="border:3px solid #2F2B8C; width:50px; height:50px; border-radius:100px;">
                                    <center>                        <br>

                                <?php else: ?>
                                    N/A
                                <?php endif; ?>

                                <div class="separador"></div>
                                <h6 style="font-size:13px;">
                                  <b>
                                    DATOS DE CONTACTO:
                                  </b>
                                </h6>
                                <p style="font-size:13px;">


                                 <?php echo $filaEmpleado->direccion_cli; ?><br>
                                 <?php echo $filaEmpleado->email_cli; ?><br>
                                 <?php echo $filaEmpleado->telefono_cli; ?><br>
                                            </p>
                              </td>

                            </tr>
                          </tbody></table>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-6 form-group">
                          <a href="ojo" class="btn btn-outline-principal btn-block">
                            <i class="fa fa-eye"></i> Ver Más
                          </a>
                        </div>
                        <div class="col-md-6 form-group">
                          <a href="ojo" class="btn btn-outline-principal btn-block">
                            <i class="fa fa-calendar"></i> Reservar
                          </a>
                        </div>
                      </div>

                    </div>
                  </div>

                </div>

      <?php endforeach; ?>

</div>
<?php else: ?>
  <br><br>
  <div class="alert alert-danger text-center">

    <h3>No se encontraron Profesionales registrados</h3>
  </div>
<?php endif; ?>
