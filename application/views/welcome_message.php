
    <div class="main-banner" id="top">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    <div class="left-content">
                        <div class="thumb">
                            <div class="inner-content container ">
                                <h4>SanJosePLUS</h4>

                                  <span>Servicio de Bolsa de Empleo, ademas encontraras Productores
                                  <span>agricolas dispuestos a entregar encargos de  productos como
                                  <span>maiz, papas entre otros.
                                  <span>Adicionalmente, una  seccion  de  Profesionales dispuestos a
                                  <span>cumplir la labor que requieras.</span>
                                  <span>Productores, Profesionales y empleos del Barrio San Jose de Tanicuchi</span>



                                <div class="main-border-button">
                                    <a href="#">Ingresa AHORA!</a>
                                </div>
                            </div>
                            <img src="<?php echo base_url(); ?>/assets3/images/1b.jpg" alt="" width="600px" height="480px">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="right-content">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="right-first-image">
                                    <div class="thumb">
                                        <div class="inner-content">
                                            <h4>Productores</h4>
                                            <span>Campesinos Productores Agricolas
                                            <span> de Hortalizas, Verduras, Etc</span>
                                        </div>
                                        <div class="hover-content">
                                            <div class="inner">
                                                <h4>Productores</h4>
                                                <p>Ingrese a la plataforma para adquirir Proveedores de Productos Agricolas</p>
                                                <div class="main-border-button">
                                                    <a href="#">Ingresa AHORA!</a>
                                                </div>
                                            </div>
                                        </div>
                                        <img src="<?php echo base_url(); ?>/assets3/images/1a.jpg" width="280px" height="235px">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="right-first-image">
                                    <div class="thumb">
                                        <div class="inner-content">
                                            <h4>Profesionales</h4>
                                            <span>Encuentra Profesionales como Obreros, Carpinteros, etc.</span>
                                        </div>
                                        <div class="hover-content">
                                            <div class="inner">
                                                <h4>Profesionales</h4>
                                                <p>Variedad de Profesionales dispuestos a cumpli cualquier tipo de trabajo</p>
                                                <div class="main-border-button">
                                                    <a href="#">Ingresa AHORA!</a>
                                                </div>
                                            </div>
                                        </div>
                                        <img src="<?php echo base_url(); ?>/assets3/images/3.jpeg" width="280px" height="235px">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                            </div>

                            <div class="col-lg-6">
                                <div class="right-first-image">
                                    <div class="thumb">
                                        <div class="inner-content">
                                            <h4>Empleos</h4>
                                            <span>Trabajos Disponibles en el Barrio De San Jose De Tanicuchi</span>
                                        </div>
                                        <div class="hover-content">
                                            <div class="inner">
                                                <h4>Empleos</h4>
                                                <p>Encontraras Variedades de Trabajos Permanentes o Temporales.</p>
                                                <div class="main-border-button">
                                                    <a href="#">Ingresa AHORA!</a>
                                                </div>
                                            </div>
                                        </div>
                                        <img src="<?php echo base_url(); ?>/assets3/images/4.jpg" width="280px" height="235px">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                            </div>
                            <!-- <div class="col-lg-6">
                                <div class="right-first-image">
                                    <div class="thumb">
                                        <div class="inner-content">
                                            <h4>Accessories</h4>
                                            <span>Best Trend Accessories</span>
                                        </div>
                                        <div class="hover-content">
                                            <div class="inner">
                                                <h4>Accessories</h4>
                                                <p>Lorem ipsum dolor sit amet, conservisii ctetur adipiscing elit incid.</p>
                                                <div class="main-border-button">
                                                    <a href="#">Discover More</a>
                                                </div>
                                            </div>
                                        </div>
                                        <img src="<?php echo base_url(); ?>/assets3/images/baner-right-image-04.jpg">
                                    </div>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ***** Main Banner Area End ***** -->

    <!-- ***** Men Area Starts ***** -->
    <section class="section" id="dos">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-heading">
                        <h2>Productores Agricolas </h2><br>
                        <span>En el Barrio San Jose, una zona rural, su gente se caracteriza por producir una gran variedad de Verduras, Hortalizas, legumbres, entre otros.</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="men-item-carousel">
                        <div class="owl-men-item owl-carousel">
                            <div class="item">
                                <div class="thumb">
                                    <div class="hover-content">
                                        <ul>
                                            <li><a href="single-product.html"><i class="fa fa-eye"></i></a></li>

                                        </ul>
                                    </div>
                                    <img src="<?php echo base_url(); ?>/assets3/images/5.jpg" width="350px" height="231px"alt="">
                                </div>
                                <div class="down-content">
                                    <h4>Zanahorias</h4>
                                    <span>Consulte Precios y Proveedores</span>

                                </div>
                            </div>
                            <div class="item">
                                <div class="thumb">
                                    <div class="hover-content">
                                        <ul>
                                            <li><a href="single-product.html"><i class="fa fa-eye"></i></a></li>

                                        </ul>
                                    </div>
                                    <img src="<?php echo base_url(); ?>/assets3/images/6.jpg" width="350px" height="231px"alt="">
                                </div>
                                <div class="down-content">
                                    <h4>Papas</h4>
                                    <span>Consulte Precios y Proveedores</span>

                                </div>
                            </div>
                            <div class="item">
                                <div class="thumb">
                                    <div class="hover-content">
                                        <ul>
                                            <li><a href="single-product.html"><i class="fa fa-eye"></i></a></li>

                                        </ul>
                                    </div>
                                    <img src="<?php echo base_url(); ?>/assets3/images/7.jpg" width="350px" height="231px"alt="">
                                </div>
                                <div class="down-content">
                                    <h4>Cebollas</h4>
                                    <span>Consulte Precios y Proveedores</span>

                                </div>
                            </div>
                            <div class="item">
                                <div class="thumb">
                                    <div class="hover-content">
                                        <ul>
                                            <li><a href="single-product.html"><i class="fa fa-eye"></i></a></li>

                                        </ul>
                                    </div>
                                    <img src="<?php echo base_url(); ?>/assets3/images/8.jpg" width="350px" height="231px"alt="">
                                </div>
                                <div class="down-content">
                                    <h4>Brocoli</h4>
                                    <span>Consulte Precios y Proveedores</span>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Men Area Ends ***** -->
    <section class="section" id="men">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-heading">
                        <h2>Profesionales del Barrio San Jose </h2><br>
                        <span>Necesitas un profesional para que realice o cumpla una labor en tu hogar, organizacion o empresa?
                        <span> Ingresa al Portal de SanJosePlus, y encontraras Profesionales como los siguientes:</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="men-item-carousel">
                        <div class="owl-men-item owl-carousel">
                            <div class="item">
                                <div class="thumb">
                                    <div class="hover-content">
                                        <ul>
                                            <li><a href="single-product.html"><i class="fa fa-eye"></i></a></li>

                                        </ul>
                                    </div>
                                    <img src="<?php echo base_url(); ?>/assets3/images/9.jpg" width="350px" height="231px"alt="">
                                </div>
                                <div class="down-content">
                                    <h4>Albañiles</h4>
                                    <span>Coordine Sueldos con los Profesionales</span>

                                </div>
                            </div>
                            <div class="item">
                                <div class="thumb">
                                    <div class="hover-content">
                                        <ul>
                                            <li><a href="single-product.html"><i class="fa fa-eye"></i></a></li>

                                        </ul>
                                    </div>
                                    <img src="<?php echo base_url(); ?>/assets3/images/10.jpg" width="350px" height="231px"alt="">
                                </div>
                                <div class="down-content">
                                    <h4>Electricistas</h4>
                                    <span>Coordine Sueldos con los Profesionales</span>

                                </div>
                            </div>
                            <div class="item">
                                <div class="thumb">
                                    <div class="hover-content">
                                        <ul>
                                            <li><a href="single-product.html"><i class="fa fa-eye"></i></a></li>

                                        </ul>
                                    </div>
                                    <img src="<?php echo base_url(); ?>/assets3/images/11.jpg" width="350px" height="231px" alt="">
                                </div>
                                <div class="down-content">
                                    <h4>Obreros</h4>
                                    <span>Coordine Sueldos con los Profesionales</span>

                                </div>
                            </div>
                            <div class="item">
                                <div class="thumb">
                                    <div class="hover-content">
                                        <ul>
                                            <li><a href="single-product.html"><i class="fa fa-eye"></i></a></li>

                                        </ul>
                                    </div>
                                    <img src="<?php echo base_url(); ?>/assets3/images/12.jpg" width="350px" height="231px"alt="">
                                </div>
                                <div class="down-content">
                                    <h4>Carpinteros</h4>
                                    <span>Coordine Sueldos con los Profesionales</span>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section" id="men">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-heading">
                        <h2>Empleos</h2><br>
                        <span>El Barrio San Jose se encuentra constantemente en crecimiento, la ciudad crece de manera exponencial, por lo que surgen de manera aleatoria empleos permanentes o temporales en varios puntos de la comunidad. Ingresa al Portal para encontrar un empleo segun tus aptitudes.</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="men-item-carousel">
                        <div class="owl-men-item owl-carousel">
                            <div class="item">
                                <div class="thumb">
                                    <div class="hover-content">
                                        <ul>
                                            <li><a href="single-product.html"><i class="fa fa-eye"></i></a></li>

                                        </ul>
                                    </div>
                                    <img src="<?php echo base_url(); ?>/assets3/images/13.jpg" width="350px" height="231px"alt="">
                                </div>
                                <div class="down-content">
                                    <h4>Se Busca Chofer Profesional</h4>
                                    <span>Consulte sueldos con el empleador</span>

                                </div>
                            </div>
                            <div class="item">
                                <div class="thumb">
                                    <div class="hover-content">
                                        <ul>
                                            <li><a href="single-product.html"><i class="fa fa-eye"></i></a></li>

                                        </ul>
                                    </div>
                                    <img src="<?php echo base_url(); ?>/assets3/images/14.jpg"width="350px" height="231px" alt="">
                                </div>
                                <div class="down-content">
                                    <h4>Busco Empleada domestica</h4>
                                    <span>Consulte sueldos con el empleador</span>

                                </div>
                            </div>
                            <div class="item">
                                <div class="thumb">
                                    <div class="hover-content">
                                        <ul>
                                            <li><a href="single-product.html"><i class="fa fa-eye"></i></a></li>

                                        </ul>
                                    </div>
                                    <img src="<?php echo base_url(); ?>/assets3/images/15.jpg" width="350px" height="231px"alt="">
                                </div>
                                <div class="down-content">
                                    <h4>Se solicita Pintor de Interiores</h4>
                                    <span>Consulte sueldos con el empleador</span>

                                </div>
                            </div>
                            <div class="item">
                                <div class="thumb">
                                    <div class="hover-content">
                                        <ul>
                                            <li><a href="single-product.html"><i class="fa fa-eye"></i></a></li>

                                        </ul>
                                    </div>
                                    <img src="<?php echo base_url(); ?>/assets3/images/16.jpg" width="350px" height="231px" alt="">
                                </div>
                                <div class="down-content">
                                    <h4>Se necesita Veterinario</h4>
                                    <span>Consulte sueldos con el empleadors</span>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- ***** Explore Area Starts ***** -->
    <section class="section" id="explore">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="left-content">
                      <center>
                        <h2>EXplicacion Del Proyecto "SanJosePLUS"</h2></center>
                        <span>En el barrio San José de Tanicuchi existe un índice de pobreza, ya que varios moradores no
cuentan con un trabajo estable, las personas del barrio trabajan y viven del día a día.
En el barrio San Jose de Tanicuchi se encontró que varias personas trabajan en la
agricultura,por ende el dinero que ingresa al hogar es del día a día, por lo tanto los moradores
no cuentan con un salario básico, ni un seguro de vida además existen personas que están sin
trabajo. Para poder obtener esta información se realizó la técnica de encuestas hacía varias
casas en torno al mismo barrio. Los moradores del barrio no cuentan con un centro de salud,
para realizar los chequeos se realizan en la parroquia de Tanicuchi.</span>
                        <div class="quote">
                            <i class="fa fa-quote-left"></i><p>Las pequeñas cosas son las responsables de los grandes cambios.    -Paulo Coelho.</p>
                        </div>
                      <span>El proyecto SanJosePLUS, busca promover a los Productores agricolas, Profesionales y promocionar empleos que existan en esta localidad.
                        <span>Los Productores Agricolas podrian recibir pedidos de los Productos que generen.
                        <span>Los Profesionales que existen podrian adquirir un empleo temporal o permanente.
                        <span>Y podrian los moradores del Barrio o sus exteriores, anunciar empleos.</span>
                        <div class="main-border-button">
                            <a href="products.html">Ingresa al Portal</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="right-content">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="leather">
                                    <h4>Barrio</h4>
                                    <span>San Jose</span>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="first-image">
                                    <img src="<?php echo base_url(); ?>/assets3/images/1e.jpg"width="250px" height="210px" alt="">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="second-image">
                                    <img src="<?php echo base_url(); ?>/assets3/images/2.jpg"width="250px" height="210px" alt="">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="types">
                                    <h4>Parroquia</h4>
                                    <span>Tanicuchi</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Explore Area Ends ***** -->

    <!-- ***** Social Area Starts ***** -->
    <!-- <section class="section" id="social">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-heading">
                        <h2>Redes Sociales</h2>
                        <span>Visita Nuestras Redes Sociales</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row images">
                <div class="col-2">
                    <div class="thumb">
                        <div class="icon">
                            <a href="http://instagram.com">
                                <h6>Fashion</h6>
                                <i class="fa fa-instagram"></i>
                            </a>
                        </div>
                        <img src="<?php echo base_url(); ?>/assets3/images/instagram-01.jpg" alt="">
                    </div>
                </div>
                <div class="col-2">
                    <div class="thumb">
                        <div class="icon">
                            <a href="http://instagram.com">
                                <h6>New</h6>
                                <i class="fa fa-instagram"></i>
                            </a>
                        </div>
                        <img src="<?php echo base_url(); ?>/assets3/images/instagram-02.jpg" alt="">
                    </div>
                </div>
                <div class="col-2">
                    <div class="thumb">
                        <div class="icon">
                            <a href="http://instagram.com">
                                <h6>Brand</h6>
                                <i class="fa fa-instagram"></i>
                            </a>
                        </div>
                        <img src="<?php echo base_url(); ?>/assets3/images/instagram-03.jpg" alt="">
                    </div>
                </div>
                <div class="col-2">
                    <div class="thumb">
                        <div class="icon">
                            <a href="http://instagram.com">
                                <h6>Makeup</h6>
                                <i class="fa fa-instagram"></i>
                            </a>
                        </div>
                        <img src="<?php echo base_url(); ?>/assets3/images/instagram-04.jpg" alt="">
                    </div>
                </div>
                <div class="col-2">
                    <div class="thumb">
                        <div class="icon">
                            <a href="http://instagram.com">
                                <h6>Leather</h6>
                                <i class="fa fa-instagram"></i>
                            </a>
                        </div>
                        <img src="<?php echo base_url(); ?>/assets3/images/instagram-05.jpg" alt="">
                    </div>
                </div>
                <div class="col-2">
                    <div class="thumb">
                        <div class="icon">
                            <a href="http://instagram.com">
                                <h6>Bag</h6>
                                <i class="fa fa-instagram"></i>
                            </a>
                        </div>
                        <img src="<?php echo base_url(); ?>/assets3/images/instagram-06.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    <!-- ***** Social Area Ends ***** -->

    <!-- ***** Subscribe Area Starts ***** -->
<section class="section" id="sus">
    <div class="subscribe">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="section-heading">
                        <h2>Nos contactamos Contigo?</h2>

                        <span>Ingresa tu Nombre y Correo electronico por favor, para que un encargado se comunique contigo</span>
                    </div>
                    <form id="subscribe" action="" method="get">
                        <div class="row">
                          <div class="col-lg-5">
                            <fieldset>
                              <input name="name" type="text" id="name" placeholder="Su Nombre" required="">
                            </fieldset>
                          </div>
                          <div class="col-lg-5">
                            <fieldset>
                              <input name="email" type="text" id="email" pattern="[^ @]*@[^ @]*" placeholder="Su Correo Electronico" required="">
                            </fieldset>
                          </div>
                          <div class="col-lg-2">
                            <fieldset>
                              <button type="submit" id="form-submit" class="main-dark-button"><i class="fa fa-paper-plane"></i></button>
                            </fieldset>
                          </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-4">
                    <div class="row">
                        <div class="col-6">
                            <ul>
                                <li>Direccion:<br><span>Barrio San Jose, Tanicuchi, Cotopaxi, Ecuador</span></li>
                                <li>Telefono:<br><span>099 884 1527</span></li>
                                <li>Correo Electronico:<br><span>sanjoseplus@gmail.com</span></li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    </section>
    <!-- ***** Subscribe Area Ends ***** -->
