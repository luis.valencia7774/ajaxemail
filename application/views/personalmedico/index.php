<section class="ftco-section ftco-no-pt">
  <div class="container">
    <div class="row justify-content-center mb-5 pb-2">
      <div class="col-md-8 text-center heading-section ftco-animate">
        <span class="subheading">Doctores</span>
        <h2 class="mb-4">Personal Medico</h2>
        <p>Los mejores especialistas medicos para atender cualquier tipo de examenes clinicos</p>
      </div>
    </div>
    <div class="row">

      <div class="col-md-4 col-lg-3 ftco-animate">
        <div class="staff">
          <div class="img-wrap d-flex align-items-stretch">
            <div class="img align-self-stretch" style="background-image: url(<?php echo base_url(); ?>/assets/images/bg_2.jpg);"></div>
          </div>
          <div class="text pt-3 text-center">
            <h3>Dr. Ramon Cruz</h3>
            <span class="position mb-2">Bioquimico Especialista</span>
            <div class="faded">


            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4 col-lg-3 ftco-animate">
        <div class="staff">
          <div class="img-wrap d-flex align-items-stretch">
            <div class="img align-self-stretch" style="background-image: url(<?php echo base_url(); ?>/assets/images/bg_1.jpg);"></div>
          </div>
          <div class="text pt-3 text-center">
            <h3>Dr. Luis Valencia</h3>
            <span class="position mb-2">Microbiologo</span>
            <div class="faded">


            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4 col-lg-3 ftco-animate">
        <div class="staff">
          <div class="img-wrap d-flex align-items-stretch">
            <div class="img align-self-stretch" style="background-image: url(<?php echo base_url(); ?>/assets/images/bg_1.jpg);"></div>
          </div>
          <div class="text pt-3 text-center">
            <h3>Dr. Edito Cumbajin</h3>
            <span class="position mb-2">Hematologo</span>
            <div class="faded">


            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4 col-lg-3 ftco-animate">
        <div class="staff">
          <div class="img-wrap d-flex align-items-stretch">
            <div class="img align-self-stretch" style="background-image: url(<?php echo base_url(); ?>/assets/images/doc-1.jpg);"></div>
          </div>
          <div class="text pt-3 text-center">
            <h3>Dr. Juan Perez</h3>
            <span class="position mb-2">Bioquimico</span>
            <div class="faded">
            
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
