</div>
<!-- content-wrapper ends -->
<!-- partial:./partials/_footer.html -->
<footer class="footer">
  <div class="card">
    <div class="card-body">
      <div class="d-sm-flex justify-content-center justify-content-sm-between py-2">
        <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © <a href="https://www.bootstrapdash.com/" target="_blank">bootstrapdash.com </a>2021</span>
        <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Only the best <a href="https://www.bootstrapdash.com/" target="_blank"> Bootstrap dashboard </a> templates</span>
      </div>
    </div>
  </div>
</footer>
<!-- partial -->
</div>
<!-- main-panel ends -->
</div>
<!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->

<!-- base:js -->
<script src="vendors/js/vendor.bundle.base.js"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<script src="vendors/chart.js/Chart.min.js"></script>
<script src="js/jquery.cookie.js" type="text/javascript"></script>
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="js/off-canvas.js"></script>
<script src="js/hoverable-collapse.js"></script>
<script src="js/template.js"></script>
<!-- endinject -->
<!-- plugin js for this page -->
<script src="js/jquery.cookie.js" type="text/javascript"></script>
<!-- End plugin js for this page -->
<!-- Custom js for this page-->
<script src="js/dashboard.js"></script>
<!-- End custom js for this page-->
</body>

</html>
<?php if ($this->session->flashdata("edicion")): ?>
  <script type="text/javascript">
    iziToast.success({
      title: 'CONFIRMACION',
      message: "<?php echo $this->session->flashdata("edicion"); ?>",
      position: 'topRight',
    });
    </script>
<?php endif; ?>

<?php if ($this->session->flashdata("eliminacion")): ?>
  <script type="text/javascript">
    iziToast.success({
      title: 'CONFIRMACION',
      message: "<?php echo $this->session->flashdata("eliminacion"); ?>",
      position: 'topRight',
    });
    </script>
<?php endif; ?>


<?php if ($this->session->flashdata("confirmacion")): ?>
  <script type="text/javascript">
    iziToast.success({
      title: 'CONFIRMACION',
      message: "<?php echo $this->session->flashdata("confirmacion"); ?>",
      position: 'topRight',
    });
    </script>
<?php endif; ?>

<?php if ($this->session->flashdata("error")): ?>
  <script type="text/javascript">
    iziToast.danger({
      title: 'ERROR',
      message: '<?php echo $this->session->flashdata("error"); ?>',
      position: 'topRight',
    });
    </script>
  <?php endif; ?>

  <?php if ($this->session->flashdata("bienvenida")): ?>
    <script type="text/javascript">
      iziToast.info({
        title: 'CONFIRMACION',
        message: '<?php echo $this->session->flashdata("bienvenida"); ?>',
        position: 'topRight',
      });
      </script>
    <?php endif; ?>

// color de jquery validation
    <style media="screen">
      .error{
        color:red;
        font-size: 16px;
      }
      input.error, select.error{
        border: 2px solid red;
      }
  </style>

  
