<br>
<center><<div  class="col-12 user-img">
  <img style="width:700px" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSzn6QiZmIrNiu7O2DC49bxYMTLg6cRDB5OKxPFR66q7MQVuRQU6oYKPmcOqkvx_GmOH0Y&usqp=CAU">
</div></center>
<br>
<br>
<br>
<center>

    <h2 class="text-info">FORMULARIO INGRESO DE DEPARTAMENTOS:</h2>
</center>
<div class="container">
<div class="row">
<h3></h3>
<div class="col-md-12">

<form action="<?php echo site_url(); ?>/departamentos/guardarDepartamento"
  method="post"
  id="frm_nuevo_departamento"
  enctype="multipart/form-data"
  >
    <br>
    <br>


    <label for="">CODIGO</label>
    <input class="form-control"  type="number" name="codigo_dep" id="codigo_dep" placeholder="Por favor Ingrese la Identificacion">
    <br>
    <br>
    <label for="">NOMBRE</label>
    <input class="form-control"  type="text" name="nombre_dep" id="nombre_dep" placeholder="Por favor Ingrese el nombre">
    <br>
    <br>
    <label for="">SERVICIOS</label>
    <select class="form-control" name="servicios_dep" id="servicios_dep">
        <option value="">--Seleccione--</option>
        <option value="CARDIOLOGIA">CARDIOLOGIA</option>
        <option value="NEUROLOGIA">NEUROLOGIA</option>
        <option value="REVISION">REVISION</option>
    </select>
    <br>
    <br>
    <label for="">TELEFONO</label>
    <input class="form-control"  type="number" name="telefono_dep" id="telefono_dep" placeholder="Por favor Ingrese el telefono">
    <br>
    <br>
    <label for="">CORREO ELECTRÓNICO</label>
    <input class="form-control"  type="email" name="email_dep" id="email_dep" placeholder="Por favor Ingrese el correo">
    <br>
    <br>
    <label for="">ESTADO</label>
    <select class="form-control" name="estado_dep" id="estado_dep">
        <option value="">--Seleccione--</option>
        <option value="ACTIVO">ACTIVO</option>
        <option value="INACTIVO">INACTIVO</option>
    </select>
    <br>
    <br>

  <center><button type="submit" name="button" class="btn btn-primary">
      GUARDAR
    </button>
    &nbsp;&nbsp;&nbsp;
    <a href="<?php echo site_url(); ?>/departamentos/index"
      class="btn btn-warning">
      <i class="fa fa-times"> </i>
      CANCELAR
    </a></center>
    <br>
    <br>



</form>
</div>
</div>
</div>

<script type="text/javascript">
    $("#frm_nuevo_departamento").validate({
      rules:{

        codigo_dep:{
          required:true,
          minlength:10,
          maxlength:10,
          digits:true
        },

        nombre_dep:{
          letras:true,
          required:true
      },
      servicios_dep:{
        letras:true,
        required:true
    },
    telefono_dep:{
      letras:true,
      required:true
  },

email_dep:{
  letras:true,
  required:true
},
estado_dep:{
  letras:true,
  required:true
},
      },
      messages:{

        codigo_dep:{
          required:"Por favor ingrese codigo",
          minlength:"La cédula debe tener mínimo 10 digitos",
          maxlength:"La cédula debe tener máximo 10 digitos",
          digits:"La cédula solo acepta números"
        },

        nombre_dep:{
          letras:"Nombre Incorrecto",
          required:"Por favor ingrese el nombre"
        },
        servicios_dep:{
          required:"Por favor eliga el servicio"
        },
        telefono_dep:{
                  required:"Por favor ingrese el telefono"
        },

        email_dep:{
                  required:"Por favor ingrese el email"
        },
        estado_dep:{
                  required:"Por favor ingrese el esatdo"
        },
      }
    });
</script>



<!--  -->
