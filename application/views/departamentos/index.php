<br>
<center>


      <h2 class="text-success">LISTADO DE DEPARTAMENTOS:</h2>
  </center>
</center>
<hr>
<br>
<center>
    <a href="<?php echo site_url(); ?>/departamentos/nuevo" class="btn btn-primary"> <i class="fa fa-plus-circle"></i>
      Agregar Nuevo Departamento
    </a>
</center>
<br>
<br>
      <?php if ($listadoDepartamentos): ?>
        <table class="table" id="tbl-departamentos">
        <thead>
          <tr>
            <th class="text-center">ID</th>
          <th class="text-center">CODIGO</th>
            <th class="text-center">NOMBRE</th>
              <th class="text-center">SERVICIOS</th>
            <th class="text-center">TELEFONO</th>
            <th class="text-center">EMAIL</th>
            <th class="text-center">ESTADO</th>
            <th class="text-center">OPCIONES</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($listadoDepartamentos->result() as $filaTemporal): ?>
            <tr>
              <td class="text-center">
                <?php echo $filaTemporal->id_dep;?>
              </td>

              <td class="text-center">
                <?php echo $filaTemporal->codigo_dep;?>
              </td>
              <td class="text-center">
                <?php echo $filaTemporal->nombre_dep;?>
              </td>
              <td class="text-center">
                <?php echo $filaTemporal->servicios_dep;?>
              </td>
              <td class="text-center">
              <?php echo $filaTemporal->telefono_dep;?>
              </td>
                <td class="text-center">
              <?php echo $filaTemporal->email_dep;?>
              </td>
              <td class="text-center">
              <?php $filaTemporal->estado_dep;?>
              <?php if($filaTemporal->estado_dep=="ACTIVO"): ?>
              <div class="alert alert-success">
                <?php echo $filaTemporal->estado_dep; ?>
             </div>
            <?php else: ?>
              <div class="alert alert-danger">
                <?php echo $filaTemporal->estado_dep; ?>
              </div>
            <?php endif; ?>
              </td>

              <td class="text-center">
                        <a href="<?php echo site_url(); ?>/departamentos/editar/<?php echo $filaTemporal->id_dep; ?>" class="btn btn-warning">

                        <i class="fa fa-pen"></i>

                        </a>

                   <a href="javascript:void(0)"
                     onclick="confirmarEliminacion ('<?php echo $filaTemporal->id_dep; ?>');"
                     class="btn btn-warning">
                     <i class="fa fa-trash"></i>
                     <?php if ($this->session->userdata("c0nectadoUTC")->perfil_usu=="ADMINISTRADOR"): ?>

                                           <?php endif; ?>
                   </a>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>

    <?php else: ?>
      <center><div class="alert alert-damger">
        <h3>NO SE ENCONTRARON CLIENTES REGISTRADOS</h3>
      </div></center>
    <?php endif; ?>

    <script type="text/javascript">
          function confirmarEliminacion(id_dep){
                iziToast.question({
                    timeout: 20000,
                    close: false,
                    overlay: true,
                    displayMode: 'once',
                    id: 'question',
                    zindex: 999,
                    title: 'CONFIRMACIÓN',
                    message: '¿Esta seguro de eliminar el cliente de forma pernante?',
                    position: 'center',
                    buttons: [
                        ['<button><b>SI</b></button>', function (instance, toast) {

                            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                            window.location.href=
                            "<?php echo site_url(); ?>/departamentos/procesarEliminacion/"+id_dep;

                        }, true],
                        ['<button>NO</button>', function (instance, toast) {

                            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                        }],
                    ]
                });
          }
      </script>

      <script type="text/javascript">
          //Deber incorporar botones de EXPORTACION
          $("#tbl-medicos").DataTable();
      </script>


      <center><button type="button" name="button" class="btn btn-primary"
       onclick="cargarListadoMedicos();"  >Actualizar</button></center>
      <script type="text/javascript">
          function cargarListadoMedicos(){
            $("#contenedor-listado-medicos")
            .html('<i class="fa fa-spin fa-lg fa-spinner"></i>');
      $("#contenedor-listado-medicos")
            .load("<?php echo site_url(); ?>/medicos/ListadoMedicos");
          }
          cargarListadoMedicos();
      </script>







      <!--  -->

      <script type="text/javascript">
      $("#tbl-clientes").DataTable();
        </script>

  <script type="text/javascript">
  $(document).ready(function() {
  $('#tbl-clientes').DataTable( {
      dom: 'Bfrtip',
      buttons: [
          'copy', 'csv', 'excel', 'pdf', 'print'
      ]
  } );
} );
  </script>
