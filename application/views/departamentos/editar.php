<br>
<center>
  <h2 class="text-primary">ACTUALIZAR LA LISTA DE DEPARTAMENTOS:</h2>

</center>
<div class="container">
<div class="row">

<div class="col-md-12">

<form
action="<?php echo site_url(); ?>/departamentos/procesarActualizacion"
  method="post"
  >
  <input type="hidden" name="id_dep" id="id_dep"
   value="<?php echo $departamento->id_dep; ?>">
    <br>
    <br>

    <label for="">CODIGO</label>
    <input class="form-control"
    value="<?php echo $departamento->codigo_dep; ?>"
    type="number" name="codigo_dep"
    id="codigo_dep"
    placeholder="Por favor Ingrese la Identificacion">
    <br>
    <br>

    <label for="">NOMBRE</label>
    <input class="form-control" value="<?php echo $departamento->nombre_dep; ?>"   type="text" name="nombre_dep" id="nombre_dep" placeholder="Por favor Ingrese el nombre">
    <br>
    <br>
    <label for="">SERVICIOS</label>
    <select class="form-control" name="servicios_dep" id="servicios_dep">
        <option value="">--Seleccione--</option>
        <option value="CARDIOLOGIA">CARDIOLOGIA</option>
        <option value="NEUROLOGIA">NEUROLOGIA</option>
        <option value="REVISION">REVISION</option>
    </select>
    <br>
    <br>
    <label for="">TELEFONO</label>
    <input class="form-control" value="<?php echo $departamento->telefono_dep; ?>"    type="number" name="telefono_dep" id="telefono_dep" placeholder="Por favor Ingrese el telefono">
    <br>
    <br>
    <label for="">EMAIL</label>
    <input class="form-control" value="<?php echo $departamento->email_dep; ?>"    type="text" name="email_dep" id="email_dep" placeholder="Por favor Ingrese el email">
    <br>
    <br>

    <label for="">ESTADO</label>
    <select class="form-control" name="estado_dep" id="estado_dep">
        <option value="">--Seleccione--</option>
        <option value="ACTIVO">ACTIVO</option>
        <option value="INACTIVO">INACTIVO</option>
    </select>
  <br>
    <br>
  <center>  <button type="submit" name="button" class="btn btn-primary">
      GUARDAR
    </button>
    &nbsp;&nbsp;&nbsp;
    <a href="<?php echo site_url(); ?>/departamentos/index"
      class="btn btn-warning">
      <i class="fa fa-times"> </i> CANCELAR
    </a></center>
    <br>
    <br>


</form>
</div>
</div>
</div>

<script type="text/javascript">
   //Activando  el pais seleccionado para el cliente
   $("#fk_id_pais")
   .val("<?php echo $cliente->fk_id_pais; ?>");

   $("#estado_cli")
   .val("<?php echo $cliente->estado_cli; ?>");

</script>
