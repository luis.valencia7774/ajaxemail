</div>


          </div>
          <!-- / Content -->




<!-- Footer -->
<footer class="content-footer footer bg-footer-theme">
  <div class="container-xxl d-flex flex-wrap justify-content-between py-2 flex-md-row flex-column">
    <div class="mb-2 mb-md-0">
      © <script>
      document.write(new Date().getFullYear())
      </script>
      , Realizado por Septimo Sistemas de Información <a href="https://themeselection.com" target="_blank" class="footer-link fw-bolder"></a>
    </div>
    <div>



    </div>
  </div>
</footer>
<!-- / Footer -->


          <div class="content-backdrop fade"></div>
        </div>
        <!-- Content wrapper -->
      </div>
      <!-- / Layout page -->
    </div>



    <!-- Overlay -->
    <div class="layout-overlay layout-menu-toggle"></div>


  </div>
  <!-- / Layout wrapper -->







  <!-- Core JS -->
  <!-- build:js assets/vendor/js/core.js -->
  <script src="<?php echo base_url() ?>/assets/assets/vendor/libs/jquery/jquery.js"></script>
  <script src="<?php echo base_url() ?>/assets/assets/vendor/libs/popper/popper.js"></script>
  <script src="<?php echo base_url() ?>/assets/assets/vendor/js/bootstrap.js"></script>
  <script src="<?php echo base_url() ?>/assets/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js"></script>

  <script src="<?php echo base_url() ?>/assets/assets/vendor/js/menu.js"></script>
  <!-- endbuild -->

  <!-- Vendors JS -->
  <script src="<?php echo base_url() ?>/assets/assets/vendor/libs/apex-charts/apexcharts.js"></script>

  <!-- Main JS -->
  <script src="<?php echo base_url() ?>/assets/assets/js/main.js"></script>

  <!-- Page JS -->
  <script src="<?php echo base_url() ?>/assets/assets/js/dashboards-analytics.js"></script>

  <!-- Place this tag in your head or just before your close body tag. -->
  <script async defer src="https://buttons.github.io/buttons.js"></script>
  <?php if ($this->session->flashdata("confirmacion")): ?>
  <script type="text/javascript">
  iziToast.success({
     title: 'Confirmacion',
     message: '<?php echo $this->session->flashdata("confirmacion"); ?>',
     position: 'topRight',
 });

  </script>

<?php endif; ?>
<?php if ($this->session->flashdata("edicion")): ?>
<script type="text/javascript">
iziToast.warning({
   title: 'Edicion',
   message: '<?php echo $this->session->flashdata("edicion"); ?>',
   position: 'topRight',
});

</script>

<?php endif; ?>
<?php if ($this->session->flashdata("Bienvenida")): ?>
  <script type="text/javascript">
    iziToast.info({
         title: 'CONFIRMACIÓN',
         message: '<?php echo $this->session->flashdata("Bienvenida"); ?>',
         position: 'topRight',
       });
  </script>
<?php endif; ?>
<?php if ($this->session->flashdata("error")): ?>
  <script type="text/javascript">
  iziToast.danger({
     title: 'Error',
     message: '<?php echo $this->session->flashdata("error"); ?>',
     position: 'topRight',
 });

  </script>

<?php endif; ?>
<style media="screen">
  .error{
    color:red;
    font-size: 16px;
  }

  input.error,select.error{
    border:2px solid red;
  }
</style>
<script type="text/javascript" src="//cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js">
      </script>
      <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
      <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>


</body>

</html>


<!-- jQuery -->
<script src="<?php echo base_url(); ?>/assets3/js/jquery-2.1.0.min.js"></script>

<!-- Bootstrap -->
<script src="<?php echo base_url(); ?>/assets3/js/popper.js"></script>
<script src="<?php echo base_url(); ?>/assets3/js/bootstrap.min.js"></script>

<!-- Plugins -->
<script src="<?php echo base_url(); ?>/assets3/js/owl-carousel.js"></script>
<script src="<?php echo base_url(); ?>/assets3/js/accordions.js"></script>
<script src="<?php echo base_url(); ?>/assets3/js/datepicker.js"></script>
<script src="<?php echo base_url(); ?>/assets3/js/scrollreveal.min.js"></script>
<script src="<?php echo base_url(); ?>/assets3/js/waypoints.min.js"></script>
<script src="<?php echo base_url(); ?>/assets3/js/jquery.counterup.min.js"></script>
<script src="<?php echo base_url(); ?>/assets3/js/imgfix.min.js"></script>
<script src="<?php echo base_url(); ?>/assets3/js/slick.js"></script>
<script src="<?php echo base_url(); ?>/assets3/js/lightbox.js"></script>
<script src="<?php echo base_url(); ?>/assets3/js/isotope.js"></script>

<!-- Global Init -->
<script src="<?php echo base_url(); ?>/assets3/js/custom.js"></script>

<script>

  $(function() {
      var selectedClass = "";
      $("p").click(function(){
      selectedClass = $(this).attr("data-rel");
      $("#portfolio").fadeTo(50, 0.1);
          $("#portfolio div").not("."+selectedClass).fadeOut();
      setTimeout(function() {
        $("."+selectedClass).fadeIn();
        $("#portfolio").fadeTo(50, 1);
      }, 500);

      });
  });

</script>
