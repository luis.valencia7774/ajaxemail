<br>
<center>


      <h2 class="text-success">LISTADO DE MEDICOS:</h2>
</center>
<hr>
<br>
<center>
    <a href="<?php echo site_url(); ?>/medicos/nuevo" class="btn btn-primary"> <i class="fa fa-plus-circle"></i>
      Agregar Nuevo Medico
    </a>
</center>
<br>
<br>
      <?php if ($listadoMedicos): ?>
        <table class="table" id="tbl-medicos">
        <thead>
          <tr>
            <th class="text-center">ID</th>
            <th class="text-center">FOTO</th>
            <th class="text-center">IDENTIFICACION</th>
            <th class="text-center">APELLIDO</th>
            <th class="text-center">NOMBRE</th>
              <th class="text-center">ESPECIALIDAD</th>
            <th class="text-center">TELEFONO</th>
            <th class="text-center">DIRECCION</th>
            <th class="text-center">EMAIL</th>
            <th class="text-center">ESTADO</th>
            <th class="text-center">OPCIONES</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($listadoMedicos->result() as $filaTemporal): ?>
            <tr>
              <td class="text-center">
                <?php echo $filaTemporal->id_med;?>
              </td>
              <td class="text-center">
                    <?php if ($filaTemporal->foto_med!=""): ?>
                      <img
                      src="<?php echo base_url(); ?>/uploads/medicos/<?php echo $filaTemporal->foto_med; ?>"
                      height="80px"
                      width="100px"
                      alt="">
                    <?php else: ?>
                      N/A
                    <?php endif; ?>
                  </td>
              <td class="text-center">
                <?php echo $filaTemporal->identificacion_med;?>
              </td>
              <td class="text-center">
              <?php echo $filaTemporal->apellido_med;?>
              </td>
              <td class="text-center">
                <?php echo $filaTemporal->nombre_med;?>
              </td>
              <td class="text-center">
                <?php echo $filaTemporal->especialidad_med;?>
              </td>
              <td class="text-center">
              <?php echo $filaTemporal->telefono_med;?>
              </td>
              <td class="text-center">
              <?php echo $filaTemporal->direccion_med;?>
              </td>
                          <td class="text-center">
              <?php echo $filaTemporal->email_med;?>
              </td>
              <td class="text-center">
              <?php $filaTemporal->estado_med;?>
              <?php if($filaTemporal->estado_med=="ACTIVO"): ?>
              <div class="alert alert-success">
                <?php echo $filaTemporal->estado_med; ?>
             </div>
            <?php else: ?>
              <div class="alert alert-danger">
                <?php echo $filaTemporal->estado_med; ?>
              </div>
            <?php endif; ?>
              </td>

              <td class="text-center">
                        <a href="<?php echo site_url(); ?>/medicos/editar/<?php echo $filaTemporal->id_med; ?>" class="btn btn-warning">
  <i class="fa fa-pen"></i>


                        </a>

                   <a href="javascript:void(0)"
                     onclick="confirmarEliminacion ('<?php echo $filaTemporal->id_med; ?>');"
                     class="btn btn-warning">
                     <i class="fa fa-trash"></i>
                     <?php if ($this->session->userdata("c0nectadoUTC")->perfil_usu=="ADMINISTRADOR"): ?>

                                           <?php endif; ?>
                   </a>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>

    <?php else: ?>
      <center><div class="alert alert-damger">
        <h3>NO SE ENCONTRARON CLIENTES REGISTRADOS</h3>
      </div></center>
    <?php endif; ?>

    <script type="text/javascript">
          function confirmarEliminacion(id_med){
                iziToast.question({
                    timeout: 20000,
                    close: false,
                    overlay: true,
                    displayMode: 'once',
                    id: 'question',
                    zindex: 999,
                    title: 'CONFIRMACIÓN',
                    message: '¿Esta seguro de eliminar el cliente de forma pernante?',
                    position: 'center',
                    buttons: [
                        ['<button><b>SI</b></button>', function (instance, toast) {

                            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                            window.location.href=
                            "<?php echo site_url(); ?>/medicos/procesarEliminacion/"+id_med;

                        }, true],
                        ['<button>NO</button>', function (instance, toast) {

                            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                        }],
                    ]
                });
          }
      </script>

      <script type="text/javascript">
          //Deber incorporar botones de EXPORTACION
          $("#tbl-medicos").DataTable();
      </script>


      <center><button type="button" name="button" class="btn btn-primary"
       onclick="cargarListadoMedicos();"  >Actualizar</button></center>
      <script type="text/javascript">
          function cargarListadoMedicos(){
            $("#contenedor-listado-medicos")
            .html('<i class="fa fa-spin fa-lg fa-spinner"></i>');
      $("#contenedor-listado-medicos")
            .load("<?php echo site_url(); ?>/medicos/ListadoMedicos");
          }
          cargarListadoMedicos();
      </script>







      <!--  -->

      <script type="text/javascript">
      $("#tbl-clientes").DataTable();
        </script>

  <script type="text/javascript">
  $(document).ready(function() {
  $('#tbl-clientes').DataTable( {
      dom: 'Bfrtip',
      buttons: [
          'copy', 'csv', 'excel', 'pdf', 'print'
      ]
  } );
} );
  </script>
