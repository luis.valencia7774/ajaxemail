<br>
<center>


  <h2 class="text-primary">ACTUALIZAR LA LISTA MEDICOS:</h2>
</center>
<div class="container">
<div class="row">

<div class="col-md-12">

<form
action="<?php echo site_url(); ?>/medicos/procesarActualizacion"
  method="post"
  >
  <input type="hidden" name="id_med" id="id_med"
   value="<?php echo $medico->id_med; ?>">
    <br>
    <br>

    <label for="">IDENTIFICACIÓN</label>
    <input class="form-control"
    value="<?php echo $medico->identificacion_med; ?>"
    type="number" name="identificacion_med"
    id="identificacion_med"
    placeholder="Por favor Ingrese la Identificacion">
    <br>
    <br>
    <label for="">APELLIDO</label>
    <input class="form-control"
    value="<?php echo $medico->apellido_med; ?>"
    type="text" name="apellido_med"
    id="apellido_med"
    placeholder="Por favor Ingrese el apellido">
    <br>
    <br>
    <label for="">NOMBRE</label>
    <input class="form-control" value="<?php echo $medico->nombre_med; ?>"   type="text" name="nombre_med" id="nombre_med" placeholder="Por favor Ingrese el nombre">
    <br>
    <label for="">ESPECIALIDAD</label>
    <select class="form-control" name="especialidad_med" id="especialidad_med">
        <option value="">--Seleccione--</option>
        <option value="MEDICINA INTERNA">MEDICINA INTERNA</option>
        <option value="MEDICINA INTERNA">MEDICINA INTERNA</option>
        <option value="NUTRIOLOGIA">NUTRIOLOGIA</option>
    </select>
    <br>
    <label for="">TELEFONO</label>
    <input class="form-control" value="<?php echo $medico->telefono_med; ?>"    type="number" name="telefono_med" id="telefono_med" placeholder="Por favor Ingrese el telefono">
    <br>
    <br>
    <label for="">DIRECCIÓN</label>
    <input class="form-control" value="<?php echo $medico->direccion_med; ?>"    type="text" name="direccion_med" id="direccion_med" placeholder="Por favor Ingrese la dirección">
    <br>
    <br>
    <label for="">CORREO ELECTRÓNICO</label>
    <input class="form-control" value="<?php echo $medico->email_med; ?>"    type="email" name="email_med" id="email_med" placeholder="Por favor Ingrese el correo">
    <br>
    <br>
    <label for="">ESTADO</label>
    <select class="form-control" name="estado_med" id="estado_med">
        <option value="">--Seleccione--</option>
        <option value="ACTIVO">ACTIVO</option>
        <option value="INACTIVO">INACTIVO</option>
    </select>
<br>
<br>
    <label for="">FOTOGRAFIA</label>
      <input  class="form-control"  value="<?php echo $medico->foto_med; ?>"  type="file" name="foto_me"
    accept="image/*"
    id="foto_med"   >
<br>
    <br>
  <center>  <button type="submit" name="button" class="btn btn-primary">
      GUARDAR
    </button>
    &nbsp;&nbsp;&nbsp;
    <a href="<?php echo site_url(); ?>/medicos/index"
      class="btn btn-warning">
      <i class="fa fa-times"> </i> CANCELAR
    </a></center>
    <br>
    <br>

</form>
</div>
</div>
</div>

<script type="text/javascript">
   //Activando  el pais seleccionado para el cliente
   $("#fk_id_pais")
   .val("<?php echo $cliente->fk_id_pais; ?>");

   $("#estado_cli")
   .val("<?php echo $cliente->estado_cli; ?>");

</script>
