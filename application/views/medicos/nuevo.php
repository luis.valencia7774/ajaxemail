<br>
<center><<div  class="col-12 user-img">
  <img style="width:800px" src="https://hvcm.gob.ec/wp-content/uploads/2022/02/HVCM-LOGO.jpg">
</div></center
<br>
<br>
<br>
<center>

      <h2 class="text-info">FORMULARIO INGRESO DE MEDICOS:</h2>
</center>
<div class="container">
<div class="row">
<h3></h3>
<div class="col-md-12">

<form action="<?php echo site_url(); ?>/medicos/guardarMedico"
  method="post"
  id="frm_nuevo_medico"
  enctype="multipart/form-data"
  >
    <br>
    <br>


    <label for="">IDENTIFICACIÓN</label>
    <input class="form-control"  type="number" name="identificacion_med" id="identificacion_med" placeholder="Por favor Ingrese la Identificacion">
    <br>
    <br>
    <label for="">APELLIDO</label>
    <input class="form-control"  type="text" name="apellido_med" id="apellido_med" placeholder="Por favor Ingrese el apellido">
    <br>
    <br>
    <label for="">NOMBRE</label>
    <input class="form-control"  type="text" name="nombre_med" id="nombre_med" placeholder="Por favor Ingrese el nombre">
    <br>
    <br>
    <label for="">ESPECIALIDAD</label>
    <select class="form-control" name="especialidad_med" id="especialidad_med">
        <option value="">--Seleccione--</option>
        <option value="MEDICINA INTERNA">MEDICINA INTERNA</option>
        <option value="MEDICINA INTERNA">MEDICINA INTERNA</option>
        <option value="NUTRIOLOGIA">NUTRIOLOGIA</option>
    </select>
    <br>
    <label for="">TELEFONO</label>
    <input class="form-control"  type="number" name="telefono_med" id="telefono_med" placeholder="Por favor Ingrese el telefono">
    <br>
    <br>
    <label for="">DIRECCIÓN</label>
    <input class="form-control"  type="text" name="direccion_med" id="direccion_med" placeholder="Por favor Ingrese la dirección">
    <br>
    <br>
    <label for="">CORREO ELECTRÓNICO</label>
    <input class="form-control"  type="email" name="email_med" id="email_med" placeholder="Por favor Ingrese el correo">
    <br>
    <br>
    <label for="">ESTADO</label>
    <select class="form-control" name="estado_med" id="estado_med">
        <option value="">--Seleccione--</option>
        <option value="ACTIVO">ACTIVO</option>
        <option value="INACTIVO">INACTIVO</option>
    </select>
    <br>
    <br>
    <label for="">FOTOGRAFIA</label>
    <input type="file" name="foto_med"
    accept="image/*"
    id="foto_med"  value="" >
    <br>
    <br>
  <center>  <button type="submit" name="button" class="btn btn-primary">
      GUARDAR
    </button>
    &nbsp;&nbsp;&nbsp;
    <a href="<?php echo site_url(); ?>/medicos/index"
      class="btn btn-warning">
      <i class="fa fa-times"> </i>
      CANCELAR
    </a></center>
    <br>
    <br>
</form>
</div>
</div>
</div>

<script type="text/javascript">
    $("#frm_nuevo_medico").validate({
      rules:{

        identificacion_med:{
          required:true,
          minlength:10,
          maxlength:10,
          digits:true
        },
        apellido_med:{
          letras:true,
          required:true
        },
        nombre_med:{
          letras:true,
          required:true
      },
      especialidad_med:{
        letras:true,
        required:true
    },
    telefono_med:{
      letras:true,
      required:true
  },
  direccion_med:{
    letras:true,
    required:true
},
email_med:{
  letras:true,
  required:true
},
estado_med:{
  letras:true,
  required:true
},
      },
      messages:{

        identificacion_med:{
          required:"Por favor ingrese el número de cédula",
          minlength:"La cédula debe tener mínimo 10 digitos",
          maxlength:"La cédula debe tener máximo 10 digitos",
          digits:"La cédula solo acepta números"
        },
        apellido_med:{
          letras:"Apellido Incorrecto",
          required:"Por favor ingrese el apellido"
        },
        nombre_med:{
          letras:"Nombre Incorrecto",
          required:"Por favor ingrese el nombre"
        },
        especialidad_med:{
          required:"Por favor eliga la especialidad"
        },
        telefono_med:{
                  required:"Por favor ingrese el telefono"
        },
        direccion_med:{
                  required:"Por favor ingrese la direccion"
        },
        email_med:{
                  required:"Por favor ingrese el email"
        },
        estado_med:{
                  required:"Por favor ingrese el esatdo"
        },
      }
    });
</script>

<script type="text/javascript">
      $("#foto_cli").fileinput({
        allowedFileExtensions:["jpeg","jpg","png"],
        dropZoneEnabled:true
      });
</script>
