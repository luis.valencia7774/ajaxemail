<section class="ftco-section ftco-no-pt ftc-no-pb">
			<div class="container">
				<div class="row no-gutters">
					<div class="col-md-5 p-md-5 img img-2 mt-5 mt-md-0" style="background-image: url(<?php echo base_url(); ?>/assets/images/bg_1.jpg);">
					</div>
					<div class="col-md-7 wrap-about py-4 py-md-5 ftco-animate">
	          <div class="heading-section mb-5">
	          	<div class="pl-md-5 ml-md-5">
		          	<span class="subheading">Acerca de LAB San Andres</span>
		            <h2 class="mb-4" style="font-size: 28px;">Es una organización sin fines de lucro, y el dinero recaudado con la publicidad en Internet apoya nuestra misión. LAB San Andres no respalda ningún producto ni servicios de terceros que se anuncien.</h2>
	            </div>
	          </div>

					</div>
				</div>
			</div>
		</section>
