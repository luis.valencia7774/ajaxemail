<br>
<div class="row">
  <div class="col-md-12 text-center">
    <h2>Registrar Nuevo Socio</h2>
  </div>
</div>
<div class="row" style="margin: 0 20px 0 20px;">
  <div class="col-md-12">
    <div class="row">

      <!--Primera columna -->
      <br>
      <div class="col-md-12">

        <form action="<?php echo site_url(); ?>/empleados/guardarEmpleado" method="post" id="frm_nuevo_empleado">

          <input type="hidden" class="form-control" value="" name="id_emp" id="id_emp" value="" placeholder="Por favor ingrese la identificación">

          <label for="">Nombre:</label> <br>
          <input class="form-control" type="text" value="" name="nombre_emp" id="nombre_emp"  value="" placeholder="Ingrese el nombre">
          <br><br>

          <label for="">Apellido:</label> <br>
          <input class="form-control" type="text" value="" name="apellido_emp" id="apellido_emp"  value="" placeholder="Ingrese el apellido">
          <br><br>

          <label for="">Dirección:</label> <br>
          <input type="text" class="form-control" value="" name="direccion_emp" id="direccion_emp"  value="" placeholder="Ingrese su dirección">
          <br><br>

          <label for="">E-mail:</label> <br>
          <input class="form-control" type="email" value="" name="email_emp" id="email_emp"  value="" placeholder="Ingrese su email">
          <br><br>

          <label for="">Telefono:</label> <br>
          <input class="form-control" type="text" value="" name="telefono_emp" id="telefono_emp"  value="" placeholder="Ingrese su teléfono">
          <br><br>

          <label for="">Proveedor de:</label>
          <select class="form-control" name="fk_id_esp" id="fk_id_esp" required>
            <option value="">-- Seleccione una opcion --</option>
            <?php if ($listadoProductos): ?>
              <?php foreach ($listadoProductos->result() as $productoTemporal): ?>
                <option value="<?php echo $productoTemporal->id_esp; ?>">
                  <?php echo $productoTemporal->nombre_esp; ?>
                </option>

              <?php endforeach; ?>

            <?php endif; ?>
          </select>
<br><br>
          <label for="">FOTOGRAFIA</label>
          <input type="file" name="foto_emp" accept="image/*" id="foto_emp" value="">
          <br>
          <br>

          <br><br><br>
          <button type="submit" name="button" class="btn btn-primary">
            GUARDAR
          </button>
          &nbsp;&nbsp;&nbsp;
          <a href="<?php echo site_url(); ?>/empleados/index"
            class="btn btn-warning">
            CANCELAR
          </a>

        </form>

      </div><br>


  </div>  <!--termina primer row interno -->





</div>  <!--cierre col-md-12 -->
</div> <!--termina primer row principal -->


<script type="text/javascript">
    $("#frm_nuevo_empleado").validate({

      rules:{
        fk_id_esp:{
          required:true
        },
        nombre_emp:{
          letras:true,
          required:true
        },

        apellido_emp:{
          letras:true,
          required:true
        },

        direccion_emp:{
          required:true
        },

        email_emp:{
          required:true
        },

        telefono_emp:{
          required:true,
          minlength:10,
          maxlength:10,
          digits:true
        },

        foto_emp:{
          required:true
        }

      },

      messages:{
        fk_id_esp:{
          required:"Por favor seleccione una Especialidad"
        },
        nombre_emp:{
          required:"Por favor ingrese el nombre",
          letras:"El nombre solo acepta letras"
        },
        apellido_emp:{
          required:"Por favor ingrese el apellido",
          letras:"El nombre solo acepta letras"
        },
        direccion_emp:{
          required:"Por favor ingrese la direccion"
        },
        email_emp:{
          required:"Por favor ingrese el correo electronico"
        },
        telefono_emp:{
          required:"Por favor ingrese el número de telefono",
          minlength:"El telefono debe tener mínimo 10 digitos",
          maxlength:"El telefono debe tener máximo 10 digitos",
          digits:"El telefono solo acepta números"
        },
        foto_emp:{
          required:"Ingrese una fotografia por favor"
        }

      }
    });
</script>

<script type="text/javascript">
  $("#foto_emp").fileinput({
    allowedFileExtensions:["jpeg","jpg","png"],
    dropZoneEnabled:true,
    language:"es"
  });

</script>
