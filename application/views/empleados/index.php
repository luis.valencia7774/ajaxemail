<br>
<div class="row">
  <div class="col-md-12 text-center">
    <h2>Lista de Productores</h2>
  </div>
</div>
<div class="row" style=" margin: 0 20px 0 20px;">

  <div class="col-md-6 text-center" >
    <br>
     <button class="btn btn-primary btn-lg"><a href="<?php echo site_url(); ?>" style=" color:white;"><i class="fa fa-angle-left"> Volver </i></a> </button>
   </div>

  <div class="col-md-6 text-center" style="padding-top:30px;">

    <button class="btn btn-primary btn-lg"> <a href="<?php echo site_url(); ?>/empleados/nuevo " style=" color:white;"> <i class="fa fa-plus"> Agregar </i> </a> </button>
  </div>

</div>


<br>

<?php if ($listadoEmpleados): ?>

  <table class="table" id="tbl-clientes">
    <thead>
    <tr>
      <th class="text-center">Codigo</th>
      <th class="text*center">Foto</th>
      <th class="text-center">Nombre</th>
      <th class="text-center">Apellido</th>
      <th class="text-center">Direccion</th>
      <th class="text-center">Email</th>
      <th class="text-center">Telefono</th>
      <th class="text-center">Proveedor de</th>
      <th class="text-center">Opciones</th>

    </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoEmpleados->result() as $filaEmpleado): ?>
        <tr>
          <td class="text-center"> <?php echo $filaEmpleado->id_emp; ?></td>
          <td class="text-center">
            <?php if ($filaEmpleado->foto_emp!=""): ?>
              <img src="<?php echo base_url(); ?>/uploads/empleados/<?php echo $filaEmpleado->foto_emp; ?>" height="80px"
                width="100px"
                alt="">
            <?php else: ?>
                N/A
            <?php endif; ?>
         </td>
          <td class="text-center"> <?php echo $filaEmpleado->nombre_emp; ?></td>
          <td class="text-center"> <?php echo $filaEmpleado->apellido_emp; ?></td>
          <td class="text-center"> <?php echo $filaEmpleado->direccion_emp; ?></td>
          <td class="text-center"> <?php echo $filaEmpleado->email_emp; ?></td>
          <td class="text-center"> <?php echo $filaEmpleado->telefono_emp; ?></td>
          <td class="text-center"> <?php echo $filaEmpleado->nombre_esp;?> </td>



          <td class="text-center">
            <a href="<?php echo site_url(); ?>/empleados/editar/<?php echo $filaEmpleado->id_emp; ?>" class="btn btn-primary"> <i class="fa fa-pen"> </i> </a>
            <a onclick="confirmarEliminacion('<?php echo $filaEmpleado->id_emp ?>')" class="btn btn-danger"> <i class="fa fa-trash"> </i> </a>


            </td>

        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>


<?php else: ?>
  <br><br>
  <div class="alert alert-danger text-center">

    <h3>No se encontraron clientes registrados</h3>
  </div>
<?php endif; ?>

<script type="text/javascript">
    function confirmarEliminacion(id_emp){
          iziToast.question({
              timeout: 20000,
              close: false,
              overlay: true,
              displayMode: 'once',
              id: 'question',
              zindex: 999,
              title: 'CONFIRMACIÓN',
              message: '¿Esta seguro de eliminar el Doctor de forma pernante?',
              position: 'center',
              buttons: [
                  ['<button><b>SI</b></button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                      window.location.href=
                      "<?php echo site_url(); ?>/empleados/procesarEliminacion/"+id_emp;

                  }, true],
                  ['<button>NO</button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                  }],
              ]
          });
    }
</script>

<script type="text/javascript">
//debe incorporar botones de Exportacion
    $("#tbl-clientes").DataTable();

</script>
