
    <section class="section" id="explore">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="left-content">
                      <center>
                        <h2>EXplicacion Del Proyecto "SanJosePLUS"</h2></center>
                        <span>En el barrio San José de Tanicuchi existe un índice de pobreza, ya que varios moradores no
cuentan con un trabajo estable, las personas del barrio trabajan y viven del día a día.
En el barrio San Jose de Tanicuchi se encontró que varias personas trabajan en la
agricultura,por ende el dinero que ingresa al hogar es del día a día, por lo tanto los moradores
no cuentan con un salario básico, ni un seguro de vida además existen personas que están sin
trabajo. Para poder obtener esta información se realizó la técnica de encuestas hacía varias
casas en torno al mismo barrio. Los moradores del barrio no cuentan con un centro de salud,
para realizar los chequeos se realizan en la parroquia de Tanicuchi.</span>
                        <div class="quote">
                            <i class="fa fa-quote-left"></i><p>Las pequeñas cosas son las responsables de los grandes cambios.    -Paulo Coelho.</p>
                        </div>
                      <span>El proyecto SanJosePLUS, busca promover a los Productores agricolas, Profesionales y promocionar empleos que existan en esta localidad.
                        <span>Los Productores Agricolas podrian recibir pedidos de los Productos que generen.
                        <span>Los Profesionales que existen podrian adquirir un empleo temporal o permanente.
                        <span>Y podrian los moradores del Barrio o sus exteriores, anunciar empleos.</span>
                        
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="right-content">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="leather">
                                    <h4>Barrio</h4>
                                    <span>San Jose</span>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="first-image">
                                    <img src="<?php echo base_url(); ?>/assets3/images/1e.jpg"width="250px" height="210px" alt="">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="second-image">
                                    <img src="<?php echo base_url(); ?>/assets3/images/2.jpg"width="250px" height="210px" alt="">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="types">
                                    <h4>Parroquia</h4>
                                    <span>Tanicuchi</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
