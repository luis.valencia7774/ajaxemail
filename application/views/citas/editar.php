<br>
<center>

<h2 class="text-primary">ACTUALIZAR LA LISTA DE CITAS MEDICAS:</h2>
</center>
<div class="container">
<div class="row">

<div class="col-md-12">

<form
action="<?php echo site_url(); ?>/citas/procesarActualizacion"
  method="post"
  >
  <input type="hidden" name="id_cit" id="id_cit"
   value="<?php echo $cita->id_cit; ?>">
    <br>
    <br>

    <label for="">IDENTIFICACIÓN</label>
    <input class="form-control"
    value="<?php echo $cita->identificacion_cit; ?>"
    type="number" name="identificacion_cit"
    id="identificacion_cit"
    placeholder="Por favor Ingrese la Identificacion">
    <br>
    <br>
    <label for="">APELLIDO</label>
    <input class="form-control"
    value="<?php echo $cita->apellido_cit; ?>"
    type="text" name="apellido_cit"
    id="apellido_cit"
    placeholder="Por favor Ingrese el apellido">
    <br>
    <br>
    <label for="">NOMBRE</label>
    <input class="form-control" value="<?php echo $cita->nombre_cit; ?>"  type="text" name="nombre_cit" id="nombre_cit" placeholder="Por favor Ingrese el nombre">
    <br>
    <br>

    <label for="">TELEFONO</label>
    <input class="form-control" value="<?php echo $cita->telefono_cit; ?>"  type="number" name="telefono_cit" id="telefono_cit" placeholder="Por favor Ingrese el telefono">
    <br>
    <br>

      <select class="form-control" name="servicios_cit" id="servicios_cit">
      <option value="">Selecione una opcion</option>
      <option value="CARDIOLOGIA">CARDIOLOGIA</option>
      <option value="NEUROLOGIA">NEUROLOGIA</option>
        <option value="REVISION">REVISION</option>
      </select>


    <br>
    <center><button type="submit" name="button" class="btn btn-primary">
      GUARDARR
    </button>
    &nbsp;&nbsp;&nbsp;
    <a href="<?php echo site_url(); ?>/citas/index"
      class="btn btn-warning">
      <i class="fa fa-times"> </i> CANCELAR
    </a></center>
    <br>
    <br>

</form>
</div>
</div>
</div>

<script type="text/javascript">
   //Activando  el pais seleccionado para el cliente
   $("#fk_id_pais")
   .val("<?php echo $cliente->fk_id_pais; ?>");

   $("#estado_cli")
   .val("<?php echo $cliente->estado_cli; ?>");

</script>
