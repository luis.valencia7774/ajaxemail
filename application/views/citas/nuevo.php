<br>
<center><div  class="col-12 user-img">
  <img style="width:700px" src="https://www.tramitesbasicos.com/wp-content/uploads/2018/03/Citas-medicas-IESS-fechas-horas.jpg">
</div></center>
<br>
<br>
<br>


<center>
    <h2 class="text-info">FORMULARIO REGISTRO DE CITAS MEDICAS:</h2>
</center>
<div class="container">
<div class="row">
<center><h3></h3></center>
<div class="col-md-12">

<form action="<?php echo site_url(); ?>/citas/guardarCita"
  method="post"
  id="frm_nuevo_medico"
  enctype="multipart/form-data"
  >
    <br>
    <br>


    <label for="">IDENTIFICACIÓN</label>
    <input class="form-control"  type="number" name="identificacion_cit" id="identificacion_cit" placeholder="Por favor Ingrese la Identificacion">
    <br>
    <br>
    <label for="">APELLIDO</label>
    <input class="form-control"  type="text" name="apellido_cit" id="apellido_cit" placeholder="Por favor Ingrese el apellido">
    <br>
    <br>
    <label for="">NOMBRE</label>
    <input class="form-control"  type="text" name="nombre_cit" id="nombre_cit" placeholder="Por favor Ingrese el nombre">
    <br>
    <br>

    <label for="">TELEFONO</label>
    <input class="form-control"  type="number" name="telefono_cit" id="telefono_cit" placeholder="Por favor Ingrese el telefono">
    <br>
    <br>
    <label for="">DIRECCIÓN</label>
    <input class="form-control"  type="text" name="direccion_cit" id="direccion_cit" placeholder="Por favor Ingrese la dirección">
    <br>
    <br>
    <label for="">SERVICIOS</label>
        <br>
        <select class="form-control" type="text" name="servicios_cit" id="servicios_cit">
        <option value="">Selecione una opcion</option>
        <option value="CARDIOLOGIA">CARDIOLOGIA</option>
        <option value="NEUROLOGIA">NEUROLOGIA</option>
          <option value="REVISION">REVISION</option>
        </select>
    <br>
    <br>
    <center><button type="submit" name="button" class="btn btn-primary">
      GUARDAR
    </button>
    &nbsp;&nbsp;&nbsp;
    <a href="<?php echo site_url(); ?>"
      class="btn btn-warning">
      <i class="fa fa-times"> </i>
      CANCELAR
    </a></center>
    <br>
    <br>

</form>
</div>
</div>
</div>


<script type="text/javascript">
    $("#frm_nuevo_cita").validate({
      rules:{

        identificacion_cit:{
          required:true,
          minlength:10,
          maxlength:10,
          digits:true
        },
        apellido_cit:{
          letras:true,
          required:true
        },
        nombre_cit:{
          letras:true,
          required:true
      },

    telefono_cit:{
      letras:true,
      required:true
  },
  direccion_cit:{
    letras:true,
    required:true
},

servicios_cit:{
  letras:true,
  required:true
},
      },
      messages:{

        identificacion_cit:{
          required:"Por favor ingrese el número de cédula",
          minlength:"La cédula debe tener mínimo 10 digitos",
          maxlength:"La cédula debe tener máximo 10 digitos",
          digits:"La cédula solo acepta números"
        },
        apellido_cit:{
          letras:"Apellido Incorrecto",
          required:"Por favor ingrese el apellido"
        },
        nombre_cit:{
          letras:"Nombre Incorrecto",
          required:"Por favor ingrese el nombre"
        },

        telefono_cit:{
                  required:"Por favor ingrese el telefono"
        },
        direccion_cit:{
                  required:"Por favor ingrese la direccion"
        },

        servicios_cit:{
                  required:"Por favor ingrese el esatdo"
        },
      }
    });
</script>



<!--  -->
