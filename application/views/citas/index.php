
<br>
<center>


    <h2 class="text-success">LISTADO DE CITAS MEDICAS:</h2>
</center>
<hr>
<br>
<center>
    <a href="<?php echo site_url(); ?>/citas/nuevo" class="btn btn-primary"> <i class="fa fa-plus-circle"></i>
      Agregar Nueva Cita
    </a>
</center>
<br>
<br>
      <?php if ($listadoCitas): ?>
        <table class="table" id="tbl-citas">
        <thead>
          <tr>
            <th class="text-center">id</th>
            <th class="text-center">CODIGO</th>
            <th class="text-center">APELLIDO</th>
            <th class="text-center">NOMBRE</th>
            <th class="text-center">TELEFONO</th>
            <th class="text-center">SERVICIOS</th>
            <th class="text-center">OPCIONES</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($listadoCitas->result() as $filaTemporal): ?>
            <tr>
              <td class="text-center">
                <?php echo $filaTemporal->id_cit;?>
              </td>

              <td class="text-center">
                <?php echo $filaTemporal->identificacion_cit;?>
              </td>
              <td class="text-center">
              <?php echo $filaTemporal->apellido_cit;?>
              </td>
              <td class="text-center">
                <?php echo $filaTemporal->nombre_cit;?>
              </td>
              <td class="text-center">
              <?php echo $filaTemporal->telefono_cit;?>
              </td>
              <td class="text-center">
              <?php echo $filaTemporal->servicios_cit;?>
              </td>


              <td class="text-center">
                        <a href="<?php echo site_url(); ?>/citas/editar/<?php echo $filaTemporal->id_cit; ?>" class="btn btn-warning">

                            <i class="fa fa-pen"></i>

                        </a>

                   <a href="javascript:void(0)"
                     onclick="confirmarEliminacion ('<?php echo $filaTemporal->id_cit; ?>');"
                     class="btn btn-warning">
                     <i class="fa fa-trash"></i>

                   </a>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>

    <?php else: ?>
      <center><div class="alert alert-damger">
        <h3>NO SE ENCONTRARON CITAS REGISTRADOS</h3>
      </div></center>
    <?php endif; ?>

    <script type="text/javascript">
          function confirmarEliminacion(id_cit){
                iziToast.question({
                    timeout: 20000,
                    close: false,
                    overlay: true,
                    displayMode: 'once',
                    id: 'question',
                    zindex: 999,
                    title: 'CONFIRMACIÓN',
                    message: '¿Esta seguro de eliminar el cliente de forma pernante?',
                    position: 'center',
                    buttons: [
                        ['<button><b>SI</b></button>', function (instance, toast) {

                            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                            window.location.href=
                            "<?php echo site_url(); ?>/citas/procesarEliminacion/"+id_cit;

                        }, true],
                        ['<button>NO</button>', function (instance, toast) {

                            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                        }],
                    ]
                });
          }
      </script>

      <script type="text/javascript">
      $("#tbl-clientes").DataTable();
        </script>

  <script type="text/javascript">
  $(document).ready(function() {
  $('#tbl-clientes').DataTable( {
      dom: 'Bfrtip',
      buttons: [
          'copy', 'csv', 'excel', 'pdf', 'print'
      ]
  } );
} );
  </script>
