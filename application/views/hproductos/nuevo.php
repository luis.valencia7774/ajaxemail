<br>
<div class="row">
  <div class="col-md-12 text-center">
    <h2>Registrar Examen Clinico</h2>
  </div>
</div>
<div class="container">
<div class="row">

<div class="col-md-12">

        <form action="<?php echo site_url(); ?>/hproductos/guardarHproducto" method="post" id="frm_nuevo_hproductos">
          <br>
          <br>
          <label for="">NOMBRE</label>
          <input class="form-control"  type="text" name="nombre_hpro"  id="nombre_hpro" placeholder="Por favor Ingrese el nombre">
          <br>
          <br>
          <label for="">VALOR PREDETERMINADO</label>
          <input class="form-control"  type="number" name="cantidad_hpro"  id="cantidad_hpro" placeholder="Por favor Ingrese la cantidad ">
          <br>
          <br>
          <label for="">PRECIO</label>
          <input class="form-control"  type="decimal" name="precio_hpro"  id="precio_hpro" placeholder="Por favor Ingrese el precio">
          <br>
          <br>
          <label for="">DESCRIPCION</label>
          <input class="form-control"  type="text" name="descripcion_hpro"  id="descripcion_hpro" placeholder="Por favor Ingrese la descripcion">
          <br>
          <br>
          <label for="">CATEGORIA</label>
          <select class="form-control" name="fk_id_hcat" id="fk_id_hcat" required>
            <option value="">--Seleccione la categoria--</option>
            <?php if ($listadoHcategorias): ?>
              <?php foreach ($listadoHcategorias->result() as $hcategoriaTemporal): ?>
                <option value="<?php echo $hcategoriaTemporal->id_hcat; ?>">
                  <?php echo $hcategoriaTemporal->nombre_hcat; ?>
                </option>

              <?php endforeach; ?>
            <?php endif; ?>
          </select>
          <br><br>
          <label for="">DOCUMENTO</label>
          <input type="file" name="doc_hpro"  id="doc_hpro" value="">
          <br>
          <br>
          <button type="submit" name="button" class="btn btn-primary">
            GUARDAR
          </button>
          &nbsp;&nbsp;&nbsp;
          <a href="<?php echo site_url(); ?>/hproductos/index"
            class="btn btn-warning">
            CANCELAR
          </a>
        </form>
      </div>
      </div>
      </div>


<script type="text/javascript">
    $("#frm_nuevo_hproductos").validate({
      rules:{
        nombre_hpro:{
          required:true
        },
        cantidad_hpro:{
          required:true
        },
        precio_hpro:{
          required:true,
          number:true
        },
        descripcion_hpro:{
          required:true
        },
        fk_id_hcat:{
          required:true
        }
      },
      messages:{
        nombre_hpro:{
          required:"Por favor ingrese el nombre"
        },
        cantidad_hpro:{
          required:"Por favor ingrese la cantidad"
        },
        //
        precio_hpro:{
          required:"Por favor ingrese el precio",
          number:"Ingrese numeros decimales"
        },
        //
        descripcion_hpro:{
          required:"Por favor ingrese la descripcion"
        },
        fk_id_hcat:{
          required:"Por favor seleccione una categoría"
        }
      }
    });
</script>

<script type="text/javascript">
  $("#doc_hpro").fileinput({
    allowedFileExtensions:["pdf"],
    dropZoneEnabled:true,
    language:"es"
  });

</script>
