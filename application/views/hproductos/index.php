<br>
<div class="row">
  <div class="col-md-12 text-center">
    <h2>LISTADO DE EXAMENES CLINICOS</h2>
  </div>
</div>

<div class="row" style=" margin: 0 20px 0 20px;">

  <div class="col-md-6 text-center" >
    <br>
     <button class="btn btn-primary btn-lg"><a href="<?php echo site_url(); ?>" style=" color:white;"><i class="fa fa-angle-left"> Volver </i></a> </button>
   </div>

  <div class="col-md-6 text-center" style="padding-top:30px;">

    <button class="btn btn-primary btn-lg"> <a href="<?php echo site_url(); ?>/hproductos/nuevo " style=" color:white;"> <i class="fa fa-plus"> Agregar </i> </a> </button>
  </div>

</div>
<br>


<?php if ($listadoHproductos): ?>

  <table class="table" id="tbl-hproductos">
    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">NOMBRE</th>
        <th class="text-center">VALORES PREDETERMINADOS</th>
        <th class="text-center">PRECIO</th>
        <th class="text-center">DESCRIPCION</th>
        <th class="text-center">CATEGORIA</th>
        <th class="text-center">DOCUMENTO</th>
        <th class="text-center">OPCIONES</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoHproductos->result() as $filaTemporal): ?>
        <tr>
          <td class="text-center">
            <?php echo $filaTemporal->id_hpro;?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->nombre_hpro;?>
          </td>
          <td class="text-center">
          <?php echo $filaTemporal->cantidad_hpro;?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->precio_hpro;?>
          </td>
          <td class="text-center">
          <?php echo $filaTemporal->descripcion_hpro;?>
          </td>

          <td class="text-center">
            <?php echo $filaTemporal->nombre_hcat; ?>
          </td>
          <td class="text-center">
            <?php if ($filaTemporal->doc_hpro!=""): ?>
              <a href="<?php echo base_url(); ?>/uploads/documentos/<?php $filaTemporal->doc_hpro; ?>"  >
                <?php echo $filaTemporal->doc_hpro; ?>
               </a>
            <?php else: ?>
                N/A
            <?php endif; ?>
         </td>
         </td>

          <td class="text-center">
            <a href="<?php echo site_url(); ?>/Hproductos/editar/<?php echo $filaTemporal->id_hpro; ?>" class="btn btn-primary"> <i class="fa fa-pen"> </i> </a>
            <a onclick="confirmarEliminacion('<?php echo $filaTemporal->id_hpro ?>')" class="btn btn-danger"> <i class="fa fa-trash"> </i> </a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>

<?php else: ?>
  <div class="alert alert-damger">
    <h3>NO SE ENCONTRARON EXAMENES CLINICOS REGISTRADOS</h3>
  </div>

<?php endif; ?>
<br><br>

<script type="text/javascript">
    function confirmarEliminacion(id_hpro){
          iziToast.question({
              timeout: 20000,
              close: false,
              overlay: true,
              displayMode: 'once',
              id: 'question',
              zindex: 999,
              title: 'CONFIRMACIÓN',
              message: '¿Esta seguro de eliminar el examen clinico de forma pernante?',
              position: 'center',
              buttons: [
                  ['<button><b>SI</b></button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                      window.location.href=
                      "<?php echo site_url(); ?>/hproductos/procesarEliminacion/"+id_hpro;

                  }, true],
                  ['<button>NO</button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                  }],
              ]
          });
    }
</script>
<script type="text/javascript">
//debe incorporar botones de Exportacion
    $("#tbl-hproductos").DataTable();

</script>
