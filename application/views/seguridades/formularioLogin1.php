<!-- <center>
  <div class="row">
    <div class="col-md-6">
      <form class="" action="<?php echo site_url(); ?>/seguridades/validarAcceso" method="post">
        <label for=""> <b>Email:</b> </label><br>
        <input class="form-control"  type="email" name="email_usu"  id="email_usu" value="" placeholder="Ingrese su email" required>
        <br>
        <label for=""> <b>Contraseña:</b> </label><br>
        <input class="form-control" type="password" name="password_usu" id="password_usu" value="" placeholder="Ingrese la contraseña" required><br><br>
        <input type="submit" class="btn btn-success" value="Ingresar Sesion">
      </form>
      <?php if ($this->session->flashdata("error")): ?>
      <script type="text/javascript">
          alert("<?php echo $this->session->flashdata("error"); ?> ");
      </script>
      <?php endif; ?>
    </div>
  </div> -->
  <!doctype html>
  <html lang="es">

  <head>
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/js/iziToast.min.js" integrity="sha512-Zq9o+E00xhhR/7vJ49mxFNJ0KQw1E1TMWkPTxrWcnpfEFDEXgUiwJHIKit93EW/XxE31HSI5GEOW06G6BF1AtA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/css/iziToast.css" integrity="sha512-DIW4FkYTOxjCqRt7oS9BFO+nVOwDL4bzukDyDtMO7crjUZhwpyrWBFroq+IqRe6VnJkTpRAS6nhDvf0w+wHmxg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <!-- importacion de la libreria de jqueryValidacion -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.4/jquery.validate.min.js" integrity="sha512-FOhq9HThdn7ltbK8abmGn60A/EMtEzIzv1rvuh+DqzJtSGq8BRdEN0U+j0iKEIffiw/yEtVuladk6rsG4X6Uqg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.4/additional-methods.min.js" integrity="sha512-XJiEiB5jruAcBaVcXyaXtApKjtNie4aCBZ5nnFDIEFrhGIAvitoqQD6xd9ayp5mLODaCeaXfqQMeVs1ZfhKjRQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.4/localization/messages_es_AR.min.js" integrity="sha512-HHnzo0ssMRoNapdoTaORwzLpemBFMsg7GA8fr0d9xS1rEXKHazYMTUAUka2abGFCfsdXgZPVVyv3LCkXP1Fhsg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <!-- CSRF Token -->
      <meta name="csrf-token" content="AK0APyVbdURTyVROP56ltrgneWFUlqXMqzk3RrIy">

      <title>INICIO  de SESION</title>

      <!-- Scripts -->
      <script src="https://www4.doramasvip.com/js/app.js" defer></script>

      <!-- Fonts -->
      <link rel="dns-prefetch" href="//fonts.gstatic.com">

      <!-- Styles -->
      <link href="https://www4.doramasvip.com/css/app.css" rel="stylesheet">
  </head>

  <body class="bg-gray-900 relative font-body h-screen sm:overflow-hidden">
      <div id="app">
          <nav class="absolute w-full mx-auto px-16 z-50">
              <div class="w-full mx-auto flex items-center justify-between">
                  <!-- <a class="text-white font-bold text-2xl" href="https://www4.doramasvip.com">
                      Inicio de sesion
                  </a> -->
                  <div>
                      <button class="navbar-toggler" type="button" data-toggle="collapse"
                          data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                          aria-expanded="false" aria-label="Toggle navigation">
                          <span class="navbar-toggler-icon"></span>
                      </button>

                      <!-- <div class="collapse navbar-collapse" id="navbarSupportedContent">
                          <! Left Side Of Navbar -->
                          <ul class="navbar-nav mr-auto">

                          </ul>

                          <!-- Right Side Of Navbar -->
                          <ul class="flex items-center space-x-6 ">
                              <!-- Authentication Links -->
                                                              <li class="text-white font-bold text-sm">
                                      <!-- <a class="nav-link" href="https://www4.doramasvip.com/login">Iniciar Sesión</a> -->
                                  </li>
                                                                                      </ul>
                      </div> -->
                  </div>
              </div>
          </nav>

          <main class="relative w-full h-full py-40 min-h-screen">
              <div class="absolute top-0 w-full h-full bg-blueGray-900 bg-no-repeat bg-full"
                  style="background-image: url(https://www4.doramasvip.com/img/bg_login.png);">
              </div>
                  <div class="container h-full px-4 mx-auto">
          <div class="flex items-center content-center justify-center h-full">
              <div class="w-full px-4 lg:w-4/12">
                  <div class="relative flex flex-col w-full mb-6 bg-white rounded-lg shadow-lg">
                      <div class="px-6 py-6 mb-0">
                          <div class="flex-auto px-4 lg:px-10">
                              <div class="mb-4 text-4xl font-bold text-center text-gray-900 font-title">Ingrese sus Credenciales</div>

                              <div class="row">
                                <div class="col-md-6">
                                  <form class="" action="<?php echo site_url(); ?>/seguridades/validarAcceso" method="post">
                                    <label for=""> <b>Email:</b> </label><br>
                                    <input class="form-control"  type="email" name="email_usu"  id="email_usu" value="" placeholder="Ingrese su email" required>
                                    <br>
                                    <label for=""> <b>Contraseña:</b> </label><br>
                                    <input class="form-control" type="password" name="password_usu" id="password_usu" value="" placeholder="Ingrese la contraseña" required><br><br>
                                    <input type="submit" class="btn btn-success" value="Inicio de Sesion">
                                  </form>
                                  <?php if ($this->session->flashdata("error")): ?>
                                  <script type="text/javascript">
                                      alert("<?php echo $this->session->flashdata("error"); ?> ");
                                  </script>
                                  <?php endif; ?>
                                </div>
                              </div>
                              </div>

                          </div>
                      </div>
                  </div>
                  <div class="relative flex flex-wrap mt-6">
                      <!-- <div class="w-1/2">
                              <a class="text-gray-500" href="https://www4.doramasvip.com/password/reset">
                                  ¿Olvidaste tu contraseña?
                              </a>
                      </div> -->
                      <div class="w-1/2 text-right"></div>
                  </div>
              </div>
          </div>
      </div>
          </main>

          <!-- <footer class="w-full absolute bottom-0">
      <div class="container mx-auto px-4">
          <div class="items-center xl:justify-between flex flex-wrap -mx-4">
              <div class="px-4 relative w-full xl:w-6/12 w-full sm:w-full">
                  <div class="text-sm text-gray-200 text-center xl:text-left py-6">
                      Copyright © 2022
                      <a href="https://dtheme.es" target="_blank" class="text-gray-100 font-semibold ml-1">
                          Dtheme Studio
                      </a>. All rights reserved.
                  </div>
              </div>
              <div class="px-4 relative w-full xl:w-6/12 w-full sm:w-full">
                  <ul class="justify-center xl:justify-end mx-auto flex items-center flex-wrap list-none pl-0 mb-0">
                      <li>
                          <a href="https://dtheme.es" target="_blank"
                              class="text-sm block py-2 px-4 bg-transparent no-underline text-white hover:text-gray-100 py-4 md:py-6 mx-auto">
                              Dtheme Studio
                          </a>
                      </li>
                      <li>
                          <a href="https://dtheme.es" target="_blank"
                              class="text-sm block py-2 px-4 bg-transparent no-underline text-white hover:text-gray-100 py-4 md:py-6 mx-auto">
                              About Us
                          </a>
                      </li>
                      <dt-social-media class="text-white" />
                  </ul>
              </div>
          </div>
      </div>

      </footer> -->
      </div>
  </body>

  </html>
