<?php if ($listadoEmpleados): ?>

      <?php foreach ($listadoEmpleados->result() as $filaEmpleado): ?>
<center><h1>Productor de <?php echo $filaEmpleado->nombre_esp; ?></h1><center>
  <section class="section" id="product">
      <div class="container">
          <div class="row">
              <div class="col-lg-8">
              <div class="left-images">
                <?php if ($filaEmpleado->foto_emp!=""): ?>
                  <img src="<?php echo base_url(); ?>/uploads/empleados/<?php echo $filaEmpleado->foto_emp; ?>" alt="">
                <?php else: ?>
                    N/A
                <?php endif; ?>
              </div>
          </div>
          <div class="col-lg-4">
              <div class="right-content">
                <h5><?php echo $filaEmpleado->nombre_emp; ?>
                <?php echo $filaEmpleado->apellido_emp; ?></h5>
                  <span class="price">$25.00 * Quintal</span>

                  <span>Productor de <?php echo $filaEmpleado->direccion_emp; ?>, a 25 dolares el quital de <?php echo $filaEmpleado->nombre_esp; ?>, ventas al po mayor o menor.</span>

                <br><br>
                  <div class="total">
                    <h6 style="font-size:13px;">
                      <b>
                        DATOS DE CONTACTO:
                      </b>
                    </h6>
                    <p style="font-size:13px;">


                     <?php echo $filaEmpleado->direccion_emp; ?><br>
                     <?php echo $filaEmpleado->email_emp; ?><br>
                     <?php echo $filaEmpleado->telefono_emp; ?><br>
                                </p>

                      <div class="main-border-button"><a href="https://web.whatsapp.com/">Contactar</a></div>
                  </div>
              </div>
          </div>
          </div>
      </div>
  </section>

<?php endforeach; ?>


<?php else: ?>
<br><br>
<div class="alert alert-danger text-center">

<h3>No se encontraron Productores registrados</h3>
</div>
<?php endif; ?>
