
</div><!-- ***** Footer Start ***** -->
<br><br><br><br>
<footer>
  <div class="container">
      <div class="row">
          <div class="col-lg-3">
              <div class="first-item">
                  <div class="logo">
                      <img src="<?php echo base_url(); ?>/assets3/images/1.png" alt="hexashop ecommerce templatemo" width="90px" height="90px">
                      <h4>SanJosePLUS</h4>
                  </div>
                  <ul>
                      <li><a href="#">Barrio San Jose, Tanicuchi, Cotopaxi, Ecuador</a></li>
                      <li><a href="#">sanjoseplus@gmail.com</a></li>
                      <li><a href="#">099 884 1527</a></li>
                  </ul>
              </div>
          </div>
          <div class="col-lg-3">
              <h4>Que buscas?</h4>
              <ul>
                  <li><a href="#">Productores</a></li>
                  <li><a href="#">Profesionales</a></li>
                  <li><a href="#">Empleos</a></li>
              </ul>
          </div>
          <div class="col-lg-3">
              <h4>Navegacion</h4>
              <ul>
                  <li><a href="#">Quienes Somos</a></li>
                  <li><a href="#">Contactos</a></li>
                  <li><a href="#">Nos Contactamos Contigo?</a></li>

              </ul>
          </div>
          <div class="col-lg-3">
              <h4>Redes Sociales</h4>
              <ul>
                  <li><a href="#"><i class="fa fa-facebook"></i> Facebook</a></li>
                  <li><a href="#"><i class="fa fa-twitter"></i> twitter</a></li>
                  <li><a href="#"><i class="fa fa-youtube"></i> Youtube</a></li>
                  <li><a href="#"><i class="fa fa-whatsapp"></i> WhatsApp</a></li>
              </ul>
          </div>
          <div class="col-lg-12">
              <div class="under-footer">
                  <p>Copyright © 2022  LMV SOFT BYTE. Todos los derechos reservados

                  <br>Design: <a href="ojo" target="_parent" title="free css templates">LMV SOFT BYTE.</a>

                  <br>Distributed By: <a href="ojo" target="_blank" title="free & premium responsive templates">LMV SOFT BYTE.</a></p>
                  
              </div>
          </div>
      </div>
  </div>
</footer>


<!-- jQuery -->
<script src="<?php echo base_url(); ?>/assets3/js/jquery-2.1.0.min.js"></script>

<!-- Bootstrap -->
<script src="<?php echo base_url(); ?>/assets3/js/popper.js"></script>
<script src="<?php echo base_url(); ?>/assets3/js/bootstrap.min.js"></script>

<!-- Plugins -->
<script src="<?php echo base_url(); ?>/assets3/js/owl-carousel.js"></script>
<script src="<?php echo base_url(); ?>/assets3/js/accordions.js"></script>
<script src="<?php echo base_url(); ?>/assets3/js/datepicker.js"></script>
<script src="<?php echo base_url(); ?>/assets3/js/scrollreveal.min.js"></script>
<script src="<?php echo base_url(); ?>/assets3/js/waypoints.min.js"></script>
<script src="<?php echo base_url(); ?>/assets3/js/jquery.counterup.min.js"></script>
<script src="<?php echo base_url(); ?>/assets3/js/imgfix.min.js"></script>
<script src="<?php echo base_url(); ?>/assets3/js/slick.js"></script>
<script src="<?php echo base_url(); ?>/assets3/js/lightbox.js"></script>
<script src="<?php echo base_url(); ?>/assets3/js/isotope.js"></script>

<!-- Global Init -->
<script src="<?php echo base_url(); ?>/assets3/js/custom.js"></script>

<script>

  $(function() {
      var selectedClass = "";
      $("p").click(function(){
      selectedClass = $(this).attr("data-rel");
      $("#portfolio").fadeTo(50, 0.1);
          $("#portfolio div").not("."+selectedClass).fadeOut();
      setTimeout(function() {
        $("."+selectedClass).fadeIn();
        $("#portfolio").fadeTo(50, 1);
      }, 500);

      });
  });

</script>

</body>
</html>


<?php if ($this->session->flashdata("confirmacion")): ?>
<script type="text/javascript">
iziToast.success({
    title: 'CONFIRMACIÓN',
    message: '<?php echo $this->session->flashdata("confirmacion"); ?>',
    position: 'topRight',
  });
</script>
<?php endif; ?>

<?php if ($this->session->flashdata("error")): ?>
<script type="text/javascript">
iziToast.danger({
    title: 'ADVERTENCIA',
    message: '<?php echo $this->session->flashdata("error"); ?>',
    position: 'topRight',
  });
</script>
<?php endif; ?>


<?php if ($this->session->flashdata("bienvenida")): ?>
<script type="text/javascript">
iziToast.info({
    title: 'CONFIRMACIÓN',
    message: '<?php echo $this->session->flashdata("bienvenida"); ?>',
    position: 'topRight',
  });
</script>
<?php endif; ?>





<style media="screen">
.error{
 color:red;
 font-size: 16px;
}
input.error, select.error{
 border: 2px solid red;
}
</style>



<!-- ***** Header Area End ***** -->
