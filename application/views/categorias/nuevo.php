<br>
<div class="row">
  <div class="col-md-12 text-center">
    <h2>INGRESAR CATEGORIA DE PROFESIONALES</h2>
  </div>
</div>
<div class="container">
<div class="row">

<div class="col-md-12">

<form action="<?php echo site_url(); ?>/categorias/guardarCategoria"
  method="post"
  id="frm_nuevo_hcat2"
  >
    <br>
    <br>
    <label for="">NOMBRE:</label>
    <input class="form-control"  type="text" name="nombre_hcat" id="nombre_hcat" placeholder="Por favor Ingrese el nombre">
    <br>
    <br>

    <label for="">DESCRIPCIÓN:</label>
    <input class="form-control"  type="text" name="descripcion_hcat" id="descripcion_hcat" placeholder="Por favor Ingrese la dirección">
    <br>
    <br>

    <button type="submit" name="button" class="btn btn-primary">GUARDAR</button>

    <a href="<?php echo site_url(); ?>/categorias/index" class="btn btn-warning">CANCELAR</a>

</form>
</div>
</div>
</div>

<script type="text/javascript">
    $("#frm_nuevo_hcat2").validate({
      rules:{
        nombre_hcat:{
          letras:true,
          required:true
        },

        descripcion_hcat:{
          required:true
        },
      },
      messages:{
        nombre_hcat:{
          letras:"El nombre solo acepta letras",
          required:"Por favor ingrese el nombre"
        },

        descripcion_hcat:{
          required:"Por favor ingrese la descripción"
        },

      }
    });
</script>
