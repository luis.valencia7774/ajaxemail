<br>
<div class="row">
  <div class="col-md-12 text-center">
    <h2>CATEGORÍAS DE PROFESIONALES</h2>
  </div>
</div>

<div class="row" style=" margin: 0 20px 0 20px;">

  <div class="col-md-6 text-center" >
    <br>
     <button class="btn btn-primary btn-lg"><a href="<?php echo site_url(); ?>" style=" color:white;"><i class="fa fa-angle-left"> Volver </i></a> </button>
   </div>

  <div class="col-md-6 text-center" style="padding-top:30px;">

    <button class="btn btn-primary btn-lg"> <a href="<?php echo site_url(); ?>/categorias/nuevo " style=" color:white;"> <i class="fa fa-plus"> Agregar </i> </a> </button>
  </div>

</div>
<br>

</center>
<?php if ($listadoCategorias): ?>

  <table class="table" id="tbl-hcategorias">
    <thead>
    <tr>
      <th class="text-center">ID</th>
      <th class="text-center">NOMBRE</th>
        <th class="text-center">DESCRIPCION</th>
      <th class="text-center">OPCIONES</th>
    </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoCategorias->result() as $filaCategoria): ?>
        <tr>
          <td class="text-center"> <?php echo $filaCategoria->id_hcat; ?></td>
          <td class="text-center"> <?php echo $filaCategoria->nombre_hcat; ?></td>
          <td class="text-center"> <?php echo $filaCategoria->descripcion_hcat; ?></td>



            <td class="text-center">
              <a href="<?php echo site_url() ?>/categorias/editar/<?php echo $filaCategoria->id_hcat; ?>" class="btn btn-primary"> <i class="fa fa-pen"> </i> </a>
              <a onclick="confirmarEliminacion('<?php echo $filaCategoria->id_hcat ?>')" class="btn btn-danger"> <i class="fa fa-trash"> </i> </a>
            </td>

        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>

<?php else: ?>
  <div class="alert alert-danger">
    <h3>No se encontraron categorias registrados</h3>

  </div>
<?php endif; ?>

<script type="text/javascript">
    function confirmarEliminacion(id_hcat){
          iziToast.question({
              timeout: 20000,
              close: false,
              overlay: true,
              displayMode: 'once',
              id: 'question',
              zindex: 999,
              title: 'CONFIRMACIÓN',
              message: '¿Esta seguro de eliminar la categoria de forma pernante?',
              position: 'center',
              buttons: [
                  ['<button><b>SI</b></button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                      window.location.href=
                      "<?php echo site_url(); ?>/categorias/Eliminacion/"+id_hcat;

                  }, true],
                  ['<button>NO</button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                  }],
              ]
          });
    }
</script>
<script type="text/javascript">
//debe incorporar botones de Exportacion
    $("#tbl-hcategorias").DataTable();

</script>
