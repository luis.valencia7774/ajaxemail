<br>
<center>
  <h2>Resultados de Examenes Clinicos</h2>
</center>
<hr>
<br>
<div class="row" style=" margin: 0 20px 0 20px;">

  <div class="col-md-6 text-center" >
    <br>
     <button class="btn btn-primary btn-lg"><a href="<?php echo site_url(); ?>" style=" color:white;"><i class="fa fa-angle-left"> Volver </i></a> </button>
   </div>

  <div class="col-md-6 text-center" style="padding-top:30px;">

    <button class="btn btn-primary btn-lg"> <a href="<?php echo site_url(); ?>/resultados/nuevo " style=" color:white;"> <i class="fa fa-plus"> Agregar </i> </a> </button>
  </div>

</div>


<?php if ($listadoProductos): ?>

  <table class="table  table-hover">
    <thead>
    <tr>
      <th class="text-center">ID</th>
      <th class="text-center">NOMBRE</th>
      <th class="text-center">CANTIDAD</th>
      <th class="text-center">PRECIO</th>
      <th class="text-center">DESCRIPCION</th>
      <th class="text-center">OPCIONES</th>
    </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoProductos->result() as $filaProducto): ?>
        <tr>
          <td class="text-center"> <?php echo $filaProducto->id_pro2; ?></td>
          <td class="text-center"> <?php echo $filaProducto->nombre_pro2; ?></td>
          <td class="text-center"> <?php echo $filaProducto->cantidad_pro2; ?></td>
          <td class="text-center"> <?php echo $filaProducto->precio_pro2; ?></td>
          <td class="text-center"> <?php echo $filaProducto->descripcion2_pro; ?></td>

          <td class="text-center">
            <a onclick="return confirm('¿Esta seguro de eliminar?');"href="<?php echo site_url();?>/resultados/Eliminacion/<?php echo $filaProducto->id_pro2; ?>"class="btn btn-danger"><i class="fa fa-trash"></i></a>
            <a   class="btn btn-success"   href="<?php echo site_url() ?>/resultados/editar/<?php echo $filaProducto->id_pro2; ?>"><i class="fa fa-pen"></i></a>

             </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>

<?php else: ?>
  <br><br>
  <div class="alert alert-danger text-center">
    <h3>No se encontraron Resultados Clinicos registrados</h3>

  </div>
<?php endif; ?>
