
<html lang="en" class="light-style layout-menu-fixed " dir="ltr" data-theme="theme-default" data-assets-path="<?php echo base_url() ?>/assets/assets/" data-template="vertical-menu-template-free">

  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />

    <title>Portal SanJosePLUS</title>

    <meta name="description" content="Most Powerful &amp; Comprehensive Bootstrap 5 HTML Admin Dashboard Template built for developers!" />
    <meta name="keywords" content="dashboard, bootstrap 5 dashboard, bootstrap 5 design, bootstrap 5">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets2/vendors/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets2/vendors/css/vendor.bundle.base.css">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets2/css/style.css">
    <!-- endinject -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>/assets2/images/favicon.png" />
    <!-- endinject -->
    <link rel="shortcut icon" href="<?php echo base_url();?>/assets2/images/favicon.png" />

    <!-- Additional CSS Files -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets3/css/bootstrap.min.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets3/css/font-awesome.css">

    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets3/css/templatemo-hexashop.css">

    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets3/css/owl-carousel.css">

    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets3/css/lightbox.css">


      <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.5.0/js/fileinput.min.js" integrity="sha512-C9i+UD9eIMt4Ufev7lkMzz1r7OV8hbAoklKepJW0X6nwu8+ZNV9lXceWAx7pU1RmksTb1VmaLDaopCsJFWSsKQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.5.0/css/fileinput.min.css" integrity="sha512-XHMymTWTeqMm/7VZghZ2qYTdoJyQxdsauxI4dTaBLJa8d1yKC/wxUXh6lB41Mqj88cPKdr1cn10SCemyLcK76A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
      <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.5.0/js/locales/es.min.js" integrity="sha512-q2lXTQuccVsDwaOpJNHbGDL2c5DEK706u1MCjKuGAG4zz+q1Sja3l2RuymU3ySE6RfmTYZ/V4wY5Ol71sRvvWA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

      <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
      <!-- Latest compiled and minified CSS -->
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/js/iziToast.min.js" integrity="sha512-Zq9o+E00xhhR/7vJ49mxFNJ0KQw1E1TMWkPTxrWcnpfEFDEXgUiwJHIKit93EW/XxE31HSI5GEOW06G6BF1AtA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/css/iziToast.css" integrity="sha512-DIW4FkYTOxjCqRt7oS9BFO+nVOwDL4bzukDyDtMO7crjUZhwpyrWBFroq+IqRe6VnJkTpRAS6nhDvf0w+wHmxg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <!-- importacion de la libreria de jqueryValidacion -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.4/jquery.validate.min.js" integrity="sha512-FOhq9HThdn7ltbK8abmGn60A/EMtEzIzv1rvuh+DqzJtSGq8BRdEN0U+j0iKEIffiw/yEtVuladk6rsG4X6Uqg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.4/additional-methods.min.js" integrity="sha512-XJiEiB5jruAcBaVcXyaXtApKjtNie4aCBZ5nnFDIEFrhGIAvitoqQD6xd9ayp5mLODaCeaXfqQMeVs1ZfhKjRQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.4/localization/messages_es_AR.min.js" integrity="sha512-HHnzo0ssMRoNapdoTaORwzLpemBFMsg7GA8fr0d9xS1rEXKHazYMTUAUka2abGFCfsdXgZPVVyv3LCkXP1Fhsg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
    <script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
    <script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>

    <script type="text/javascript">
    jQuery.validator.addMethod("letras", function(value,  element) {
    //return this.optional(element) || /^[a-z]+$/i.test(value);
    return this.optional(element) || /^[A-Za-zÁÉÍÑÓÚáé íñó]*$/.test(value);
    }, "Este campo solo acepta letras");
    </script>



  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.5.0/js/fileinput.min.js" integrity="sha512-C9i+UD9eIMt4Ufev7lkMzz1r7OV8hbAoklKepJW0X6nwu8+ZNV9lXceWAx7pU1RmksTb1VmaLDaopCsJFWSsKQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.5.0/css/fileinput.min.css" integrity="sha512-XHMymTWTeqMm/7VZghZ2qYTdoJyQxdsauxI4dTaBLJa8d1yKC/wxUXh6lB41Mqj88cPKdr1cn10SCemyLcK76A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.5.0/js/locales/es.min.js" integrity="sha512-q2lXTQuccVsDwaOpJNHbGDL2c5DEK706u1MCjKuGAG4zz+q1Sja3l2RuymU3ySE6RfmTYZ/V4wY5Ol71sRvvWA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>



    <!-- Canonical SEO -->
    <link rel="canonical" href="https://themeselection.com/products/sneat-bootstrap-html-admin-template/">

    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="<?php echo base_url() ?>/assets/assets/img/favicon/favicon.ico" />

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet">

    <!-- Icons. Uncomment required icon fonts -->
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/assets/vendor/fonts/boxicons.css" />



    <!-- Core CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/assets/vendor/css/core.css" class="template-customizer-core-css" />
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/assets/vendor/css/theme-default.css" class="template-customizer-theme-css" />
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/assets/css/demo.css" />

    <!-- Vendors CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css" />

    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/assets/vendor/libs/apex-charts/apex-charts.css" />

    <!-- Page CSS -->

    <!-- Helpers -->
    <script src="<?php echo base_url() ?>/assets/assets/vendor/js/helpers.js"></script>

    <!--! Template customizer & Theme config files MUST be included after core stylesheets and helpers.js in the <head> section -->
    <!--? Config:  Mandatory theme config file contain global vars & default theme options, Set your preferred theme option in this file.  -->
    <script src="<?php echo base_url() ?>/assets/assets/js/config.js"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async="async" src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
    <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());
    gtag('config', 'GA_MEASUREMENT_ID');
    </script>
    <!-- Custom notification for demo -->
    <!-- beautify ignore:end -->

</head>
<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/js/iziToast.min.js" integrity="sha512-Zq9o+E00xhhR/7vJ49mxFNJ0KQw1E1TMWkPTxrWcnpfEFDEXgUiwJHIKit93EW/XxE31HSI5GEOW06G6BF1AtA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/css/iziToast.css" integrity="sha512-DIW4FkYTOxjCqRt7oS9BFO+nVOwDL4bzukDyDtMO7crjUZhwpyrWBFroq+IqRe6VnJkTpRAS6nhDvf0w+wHmxg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<!-- Importacion FILE INPUT -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.5.0/js/fileinput.min.js" integrity="sha512-C9i+UD9eIMt4Ufev7lkMzz1r7OV8hbAoklKepJW0X6nwu8+ZNV9lXceWAx7pU1RmksTb1VmaLDaopCsJFWSsKQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.5.0/css/fileinput.min.css" integrity="sha512-XHMymTWTeqMm/7VZghZ2qYTdoJyQxdsauxI4dTaBLJa8d1yKC/wxUXh6lB41Mqj88cPKdr1cn10SCemyLcK76A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.5.0/js/locales/es.min.js" integrity="sha512-q2lXTQuccVsDwaOpJNHbGDL2c5DEK706u1MCjKuGAG4zz+q1Sja3l2RuymU3ySE6RfmTYZ/V4wY5Ol71sRvvWA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<!-- Importacion jquery validation -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.4/jquery.validate.min.js" integrity="sha512-FOhq9HThdn7ltbK8abmGn60A/EMtEzIzv1rvuh+DqzJtSGq8BRdEN0U+j0iKEIffiw/yEtVuladk6rsG4X6Uqg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.4/additional-methods.min.js" integrity="sha512-XJiEiB5jruAcBaVcXyaXtApKjtNie4aCBZ5nnFDIEFrhGIAvitoqQD6xd9ayp5mLODaCeaXfqQMeVs1ZfhKjRQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.4/localization/messages_es_AR.min.js" integrity="sha512-HHnzo0ssMRoNapdoTaORwzLpemBFMsg7GA8fr0d9xS1rEXKHazYMTUAUka2abGFCfsdXgZPVVyv3LCkXP1Fhsg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
<script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js">
      </script>
      <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
      <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>

<script type="text/javascript">

jQuery.validator.addMethod("letras", function(value, element) {
		  //return this.optional(element) || /^[a-z]+$/i.test(value);
		  return this.optional(element) || /^[A-Za-zÁÉÍÑÓÚáé íñó]*$/.test(value);

		}, "Este campo solo acepta letras");

</script>

<body>

  <!-- Layout wrapper -->
<div class="layout-wrapper layout-content-navbar  ">
  <div class="layout-container">







<!-- Menu -->

<aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">


  <div class="app-brand demo ">
    <div class="">
      <a href="<?php echo site_url(); ?>/porcliente/index">
            <img src="<?php echo base_url(); ?>/assets3/images/1.png" width="70px" height="70px"  >
            <span>SanJosePLUS</span>
            </a>
    </div>

    <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto d-block d-xl-none">
      <i class="bx bx-chevron-left bx-sm align-middle"></i>
    </a>
  </div>

  <div class="menu"></div>

<nav class="sidebar sidebar-offcanvas" id="sidebar">
  <br>
<center>
  <h3>Bienvenido</h3>
<b> <?php echo $this->session->userdata("c0nectadoUTC")->perfil_usu; ?></b><br>
  <b> <?php echo $this->session->userdata("c0nectadoUTC")->nombre_usu; ?></b>
  <b> <?php echo $this->session->userdata("c0nectadoUTC")->apellido_usu; ?></b>
  <br>
</center>

  <ul class="nav">
    <li class="nav-item sidebar-category">
      <p>Proveedores</p>
      <span></span>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?php echo site_url(); ?>/porcliente/productores">
        <i class="mdi mdi-view-quilt menu-icon"></i>
        <span class="menu-title">Zanahorias</span>
        <!-- <div class="badge badge-info badge-pill">2</div> -->
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?php echo site_url(); ?>/porcliente/papas">
        <i class="mdi mdi-view-quilt menu-icon"></i>
        <span class="menu-title">Papas</span>
        <!-- <div class="badge badge-info badge-pill">2</div> -->
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?php echo site_url(); ?>/porcliente/Brocoli">
        <i class="mdi mdi-view-quilt menu-icon"></i>
        <span class="menu-title">Brocoli</span>
        <!-- <div class="badge badge-info badge-pill">2</div> -->
      </a>
    </li>
    <li class="nav-item sidebar-category">
      <p>Profesionales</p>
      <span></span>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="<?php echo site_url(); ?>/clientes/carpinteros">
        <i class="mdi mdi-view-quilt menu-icon"></i>
        <span class="menu-title">Carpinteros</span>
        <!-- <div class="badge badge-info badge-pill">2</div> -->
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="<?php echo site_url(); ?>/clientes/obreros">
        <i class="mdi mdi-view-quilt menu-icon"></i>
        <span class="menu-title">Obreros</span>
        <!-- <div class="badge badge-info badge-pill">2</div> -->
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="<?php echo site_url(); ?>/clientes/electricistas">
        <i class="mdi mdi-view-quilt menu-icon"></i>
        <span class="menu-title">Electricistas</span>
        <!-- <div class="badge badge-info badge-pill">2</div> -->
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="<?php echo site_url(); ?>/clientes/domestica">
        <i class="mdi mdi-view-quilt menu-icon"></i>
        <span class="menu-title">Empleada Domestica</span>
        <!-- <div class="badge badge-info badge-pill">2</div> -->
      </a>
    </li>
    <!-- <li class="nav-item sidebar-category">
      <p>Empleos</p>
      <span></span>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-bs-toggle="collapse" href="<?php echo site_url(); ?>/clientes/index" aria-expanded="false" aria-controls="ui-basic">
        <i class="mdi mdi-palette menu-icon"></i>
        <span class="menu-title">Mirar</span>

      </a>
      </li> -->

    <hr>


          <li class="nav-item">
            <a class="nav-link" >
              <i class="typcn typcn-device-desktop menu-icon" data-bs-toggle="collapse"></i>
              <span href="javascript:void(0)" onclick ="confirmarSalida('<?php echo site_url(); ?>/seguridades/cerrarSesion');" name="salir" class="menu-title">Salir</span>
            </a>
          </li>
          <script type="text/javascript">
              function confirmarSalida(id_usu){
                    iziToast.question({
                        timeout: 20000,
                        close: false,
                        overlay: true,
                        displayMode: 'once',
                        id: 'question',
                        zindex: 999,
                        title: 'CONFIRMACIÓN',
                        message: '¿Esta seguro de salir de la cuenta de forma permanente?',
                        position: 'center',
                        buttons: [
                            ['<button><b>SI</b></button>', function (instance, toast) {

                                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                                window.location.href=
                                "<?php echo site_url(); ?>/seguridades/formularioLogin/";

                            }, true],
                            ['<button>NO</button>', function (instance, toast) {

                                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                            }],
                        ]
                    });
              }

          </script>


</nav>
  <ul class="nav">
    <li class="nav-item sidebar-category">
      <p>Proveedores</p>
      <span></span>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?php echo site_url(); ?>/empleados/index">
        <i class="mdi mdi-view-quilt menu-icon"></i>
        <span class="menu-title">Zanahorias</span>
        <!-- <div class="badge badge-info badge-pill">2</div> -->
      </a>
    </li>
    <li class="menu-item active">
      <a href="<?php echo site_url() ?>/clientes/index" class="menu-link">
        <i class="menu-icon tf-icons bx bx-home-circle"></i>
        <div data-i18n="Analytics">Clientes</div>
      </a>
    </li>

    <li class="menu-item">
      <a href="<?php echo site_url() ?>/perfiles/index" class="menu-link">
        <i class="menu-icon tf-icons bx bx-collection"></i>
        <div data-i18n="Basic">Perfiles</div>
      </a>
    </li>
    <li class="menu-item">
      <a href="<?php echo site_url() ?>/pedidos/index" class="menu-link">
        <i class="menu-icon tf-icons bx bx-collection"></i>
        <div data-i18n="Basic">Pedidos</div>
      </a>
    </li>



    <!-- Layouts -->

    <li class="menu-item">
      <a href="" class="menu-link menu-toggle">
        <i class="menu-icon tf-icons bx bx-layout"></i>
        <div data-i18n="Layouts">Inventario</div>
      </a>

      <ul class="menu-sub">

        <li class="menu-item">
          <a href="<?php echo site_url() ?>/usuarios/index" class="menu-link">
            <div data-i18n="Without menu"> Usuarios</div>
          </a>
        </li>

        <li class="menu-item">
          <a href="<?php echo site_url() ?>/productos/index" class="menu-link">
            <div data-i18n="Without navbar">Productos</div>
          </a>
        </li>

      </ul>
    </li>

    <li class="menu-item">
      <a href="" class="menu-link menu-toggle">
        <i class="menu-icon tf-icons bx bx-layout"></i>
        <div data-i18n="Layouts">Proveedor</div>
      </a>

      <ul class="menu-sub">

        <li class="menu-item">
          <a href="<?php echo site_url() ?>/proveedores/index" class="menu-link">
            <div data-i18n="Without menu"> Proveedores</div>
          </a>
        </li>

      </ul>
    </li>




    <!-- Misc -->

</aside>
<!-- / Menu -->



    <!-- Layout container -->
    <div class="layout-page">





<!-- Navbar -->




<nav class="layout-navbar container-xxl navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme" id="layout-navbar">











      <div class="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0   d-xl-none ">
        <a class="nav-item nav-link px-0 me-xl-4" href="javascript:void(0)">
          <i class="bx bx-menu bx-sm"></i>
        </a>
      </div>


      <div class="navbar-nav-right d-flex align-items-center" id="navbar-collapse">





        <!-- Search -->
        <div class="navbar-nav align-items-center">
          <div class="nav-item d-flex align-items-center">
            <i class="bx bx-search fs-4 lh-0"></i>
            <input type="text" class="form-control border-0 shadow-none" placeholder="Buscar..." aria-label="Buscar...">
          </div>
        </div>
        <!-- /Search -->


        <ul class="navbar-nav flex-row align-items-center ms-auto">



          <!-- Place this tag where you want the button to render. -->
          <li class="nav-item lh-1 me-3">

          </li>



          <!-- User -->
          <li class="nav-item navbar-dropdown dropdown-user dropdown">
            <a class="nav-link dropdown-toggle hide-arrow" href="javascript:void(0);" data-bs-toggle="dropdown">
              <div class="avatar avatar-online">
                <img src="<?php echo base_url() ?>/assets/assets/img/avatars/usuario.png" alt class="w-px-40 h-auto rounded-circle">
              </div>
            </a>
            <ul class="dropdown-menu dropdown-menu-end">
              <li>
                <a class="dropdown-item" href="#">
                  <div class="d-flex">
                    <div class="flex-shrink-0 me-3">
                      <div class="avatar avatar-online">
                        <img src="<?php echo base_url() ?>/assets/assets/img/avatars/usuario.png" alt class="w-px-40 h-auto rounded-circle">
                      </div>
                    </div>
                    <div class="flex-grow-1">
                      <span class="fw-semibold d-block">  <?php echo
                        $this->session->userdata("c0nectadoTienda")->nombre_usu ?></span>
                        <span class="fw-semibold d-block">    <?php echo
                          $this->session->userdata("c0nectadoTienda")->apellido_usu ?></span>




                    </div>
                  </div>
                </a>
              </li>
              <li>
                <div class="dropdown-divider"></div>
              </li>



              <li>
                <a class="dropdown-item" onclick="confimacionCerrarSesion('<?php echo site_url(); ?>/seguridades/cerrarSesion');"href="javascript:void(0)" >
                  <i class="bx bx-power-off me-2"></i>
                  <span class="align-middle">Salir</span>
                </a>
              </li>
            </ul>
          </li>
          <!--/ User -->


        </ul>
      </div>



  </nav>



<!-- / Navbar -->



      <!-- Content wrapper -->
      <div class="content-wrapper">

        <!-- Content -->

          <div class="container-xxl flex-grow-1 container-p-y">

            <script type="text/javascript">
                        function confimacionCerrarSesion(id_usu){
                              iziToast.question({
                                  timeout: 20000,
                                  close: false,
                                  overlay: true,
                                  displayMode: 'once',
                                  id: 'question',
                                  zindex: 999,
                                  title: 'CONFIRMACIÓN',
                                  message: '¿Esta seguro que desea cerrar la sesion?',
                                  position: 'center',
                                  buttons: [
                                      ['<button><b>SI</b></button>', function (instance, toast) {

                                          instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                                          window.location.href=
                                          ""+id_usu;

                                      }, true],
                                      ['<button>NO</button>', function (instance, toast) {

                                          instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                                      }],
                                  ]
                              });
                        }
                    </script>
