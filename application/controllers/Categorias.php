<?php
class Categorias extends CI_Controller{

  public function __construct(){
    //llamamos al constructor(el constructor no devuelve valores)
    parent::__construct();
    //cargamos el modelo en el controlador
    $this->load->model('categoria');
  }//cierre del constructor

  public function index(){
    if ($this->session->userdata("c0nectadoUTC")) {

		}else{
			redirect("seguridades/formularioLogin");
		}
    $data["listadoCategorias"]=$this->categoria->consultarTodos();
    $this->load->view('header1');
    $this->load->view('categorias/index',$data);
    $this->load->view('footer1');
  }//cierre de la funcion index
  public function nuevo(){
    if ($this->session->userdata("c0nectadoUTC")) {

		}else{
			redirect("seguridades/formularioLogin");
		}
    $this->load->view('header1');
    $this->load->view('categorias/nuevo');
    $this->load->view('footer1');

  }

  public function editar($id_hcat){
    if ($this->session->userdata("c0nectadoUTC")) {

		}else{
			redirect("seguridades/formularioLogin");
		}
        $data["categoria"]=$this->categoria->consultarPorId($id_hcat);
        $this->load->view("header1");
        $this->load->view("categorias/editar",$data);
        $this->load->view("footer1");
  }
  public function guardarCategoria(){
    $datosNuevoCategoria=array(
      "nombre_hcat"=>$this->input->post("nombre_hcat"),
      "descripcion_hcat"=>$this->input->post("descripcion_hcat"),
    );
if($this->categoria->insertar($datosNuevoCategoria)){
  $this->session->set_flashdata("confirmacion","Categoria ingresada correctamente");
}else{
    $this->session->set_flashdata("error","Error al ingresar");

}
redirect("categorias/index");

}//cierre funcion guardarEmpleado

public function Eliminacion($id_hcat){

  if($this->categoria->eliminar($id_hcat)){

      $this->session->set_flashdata('eliminacion',"Categoria eliminada exitosamente.");

  }else{
      $this->session->set_flashdata("error","Error al procesar intente nuevamente.");
  }
redirect("categorias/index");
}//cierre funcion procesarElimacion



public function procesarActualizacion(){
    $id_hcat=$this->input->post("id_hcat");
    $datosNuevoEditado=array(
      "nombre_hcat"=>$this->input->post("nombre_hcat"),
      "descripcion_hcat"=>$this->input->post("descripcion_hcat")
    );

    if ($this->categoria->actualizar($id_hcat,$datosNuevoEditado)) {
      $this->session->set_flashdata('edicion',"Categoria editada exitosamente.");
      // code...
    }else {
    $this->session->set_flashdata("error","Error al procesar intente nuevamente.");
    }
    redirect("categorias/index");
  }


}//cierre de la clase



 ?>
