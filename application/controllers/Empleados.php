<?php
class Empleados extends CI_Controller{

  public function __construct(){
    //llamamos al constructor(el constructor no devuelve valores)
    parent::__construct();
    //cargamos el modelo en el controlador
    $this->load->model('empleado');
    $this->load->model('especialidadmedica');
    if ($this->session->userdata("c0nectadoUTC")) {

		}else{
			redirect("seguridades/formularioLogin");
		}
  }//cierre del constructor

 public function index(){

    $data["listadoEmpleados"]=$this->empleado->consultarTodos();
    $this->load->view('header1');
    $this->load->view('empleados/index',$data);
    $this->load->view('footer1');
  }//cierre de la funcion index
  public function nuevo(){

    $data["listadoProductos"]=$this->especialidadmedica->consultarTodos();
    $this->load->view('header1');
    $this->load->view('empleados/nuevo',$data);
    $this->load->view('footer1');

  }
  public function editar($id_emp){

    $data["listadoProductos"]=$this->especialidadmedica->consultarTodos();
    $data["empleado"]=$this->empleado->consultarPorId($id_emp);
    $this->load->view("header1");
    $this->load->view("empleados/editar",$data);
    $this->load->view("footer1");
  }


  public function guardarEmpleado(){
    $datosNuevoEmpleado=array(
      "nombre_emp"=>$this->input->post("nombre_emp"),
      "apellido_emp"=>$this->input->post("apellido_emp"),
      "direccion_emp"=>$this->input->post("direccion_emp"),
      "email_emp"=>$this->input->post("email_emp"),
      "telefono_emp"=>$this->input->post("telefono_emp"),
      "fk_id_esp"=>$this->input->post("fk_id_esp"),
      "foto_emp"=>$this->input->post("foto_emp")
    );

    //logica de negecio necesari para subir la fotografia del clientes
        $this->load->library("upload"); //carga de la liberia de subida  alaterio
        $nombreTemporal="foto_empleado_".time()."_".rand(1,5000);// generar un nombre aleatorio
        $config["file_name"]=$nombreTemporal;
        $config["upload_path"]=APPPATH.'../uploads/empleados/'; //LA RUTA
        $config["allowed_types"]="jpeg|jpg|png";// el tipo de formato
        $config["max_size"]=2*1024;//2MB PARA EL PESO
        $this->upload->initialize($config);
        if ($this->upload->do_upload("foto_emp")) {
          $dataSubida=$this->upload->data();
          $datosNuevoEmpleado["foto_emp"]=$dataSubida["file_name"];//no es mane
          // code...
        }
        if ($this->empleado->insertar($datosNuevoEmpleado)){
          $this->session->set_flashdata('confirmacion',"Doctor(a) insertado exitosamente.");
        }else{
          $this->session->set_flashdata("error","Error al procesar intente nuevamente.");
        }
        redirect("empleados/index");
    }//cierre funcion guardarEmpleado

public function procesarEliminacion($id_emp){

  if($this->empleado->eliminar($id_emp)){

    $this->session->set_flashdata('eliminacion',"Doctor(a) eliminado exitosamente.");

  }else{
    $this->session->set_flashdata("error","Error al procesar intente nuevamente.");
  }
redirect("empleados/index");
}//cierre funcion procesarElimacion

public function procesarActualizacion(){
$id_emp=$this->input->post("id_emp");
$datosEmpleadoEditado=array(
  "nombre_emp"=>$this->input->post("nombre_emp"),
  "apellido_emp"=>$this->input->post("apellido_emp"),
  "direccion_emp"=>$this->input->post("direccion_emp"),
  "email_emp"=>$this->input->post("email_emp"),
  "telefono_emp"=>$this->input->post("telefono_emp"),
  "fk_id_esp"=>$this->input->post("fk_id_esp"),
  "foto_emp"=>$this->input->post("foto_emp")
);

        $this->load->library("upload"); //carga de la liberia de subida  alaterio
        $nombreTemporal="foto_emp".time()."_".rand(1,5000);// generar un nombre aleatorio
        $config["file_name"]=$nombreTemporal;
        $config["upload_path"]=APPPATH.'../uploads/empleados/'; //LA RUTA
        $config["allowed_types"]="jpeg|jpg|png";// el tipo de formato
        $config["max_size"]=2*1024;//2MB PARA EL PESO
        $this->upload->initialize($config);
        if ($this->upload->do_upload("foto_emp")) {
          $dataSubida=$this->upload->data();
          $foto = $this->empleado->capturarImagen($id_emp);
          unlink('./uploads/empleados/'.$foto->foto_emp);
          $datosEmpleadoEditado["foto_emp"]=$dataSubida["file_name"];//no es mane
          // code...
        }

if($this->empleado->actualizar($id_emp,$datosEmpleadoEditado)){
  $this->session->set_flashdata('edicion',"Doctor(a) editado exitosamente.");

}else{
$this->session->set_flashdata("error","Error al procesar intente nuevamente.");
}
redirect("empleados/index");
}

}//cierre de la clase



 ?>
