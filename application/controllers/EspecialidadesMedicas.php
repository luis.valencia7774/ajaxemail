<?php
class EspecialidadesMedicas extends CI_Controller{
  public function __construct(){

    parent::__construct();
    $this->load->model('especialidadmedica');
  }//cierre del constructor

public function index(){
  if ($this->session->userdata("c0nectadoUTC")) {

  }else{
    redirect("seguridades/formularioLogin");
  }
$data["listadoEspecialidadesM"]=$this->especialidadmedica->consultarTodos();
$this->load->view('header1');
$this->load->view('especialidadesMedicas/index',$data);
$this->load->view('footer1');
}//cierre de la funcion Index

public function editar($id_esp){
  if ($this->session->userdata("c0nectadoUTC")) {

  }else{
    redirect("seguridades/formularioLogin");
  }
  $data["especialidad"]=$this->especialidadmedica->consultarPorId($id_esp);
  $this->load->view('header1');
  $this->load->view('especialidadesmedicas/editar',$data);
  $this->load->view('footer1');

}//cierre de la funcion nuevo

public function nuevo(){
  if ($this->session->userdata("c0nectadoUTC")) {

  }else{
    redirect("seguridades/formularioLogin");
  }
  $this->load->view('header1');
  $this->load->view('especialidadesMedicas/nuevo');
  $this->load->view('footer1');

}//cierre de la funcion nuevo

public function guardarProducto(){
$datosNuevoProducto=array(
  "nombre_esp"=>$this->input->post("nombre_esp"),
  "descripcion_esp"=>$this->input->post("descripcion_esp")
  );

 if($this->especialidadmedica->insertar($datosNuevoProducto)){
   $this->session->set_flashdata('confirmacion',"Especialidad insertada exitosamente.");
 }else{
   $this->session->set_flashdata("error","Error al procesar intente nuevamente.");
 }
redirect('especialidadesmedicas/index');
}//cierre de la funcion guardarProducto

public function Eliminacion($id_esp){
  if($this->especialidadmedica->eliminar($id_esp)){
  $this->session->set_flashdata('eliminacion',"Especialidad eliminada exitosamente.");


  }else{
    $this->session->set_flashdata("error","Error al procesar intente nuevamente.");
      }
redirect("especialidadesmedicas/index");
}//cierre de la funcion Eliminacion
public function consulta(){
  $listadoProductos=$this->db->get('especialidadmedica');
  if ($listadoProductos->num_rows()>0) {
    return $listadoProductos;
  }else {
    return false;
  }
}

public function procesarActualizacion(){
    $id_esp=$this->input->post("id_esp");
    $datosEspecialidadEditado=array(
      "nombre_esp"=>$this->input->post("nombre_esp"),
      "descripcion_esp"=>$this->input->post("descripcion_esp")
      );

    if ($this->especialidadmedica->actualizar($id_esp,$datosEspecialidadEditado)) {
      $this->session->set_flashdata('edicion',"Especialidad editada exitosamente.");
      // code...
    }else {
      $this->session->set_flashdata("error","Error al procesar intente nuevamente.");

    }
    redirect("especialidadesmedicas/index");
  }
}//cierre de la clase Productos





 ?>
