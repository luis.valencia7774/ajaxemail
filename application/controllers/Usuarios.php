<?php
      class Usuarios extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model("usuario");


        }
//NUEVA LINEA 6 DE JULIO
        public function index(){
          $this->load->view("header1");
          $this->load->view("usuarios/index");
          $this->load->view("footer1");
        }

        public function listado(){
          $data["listadoUsuarios"]=$this->usuario->obtenerTodos();
              $this->load->view("usuarios/listado",$data);

        }
        public function crear(){
          $this->load->view("header1");
          $this->load->view("usuarios/crear");
          $this->load->view("footer1");
        }

        public function crear1(){
          $this->load->view("header");
          $this->load->view("usuarios/crear");
          $this->load->view("footer");
        }
        public function insertarUsuario1(){
      $data=array(
        "apellido_usu"=>$this->input->post("apellido_usu"),
        "nombre_usu"=>$this->input->post("nombre_usu"),
        "email_usu"=>$this->input->post("email_usu"),
        "password_usu"=>md5($this->input->post("password_usu")),
        "perfil_usu"=>$this->input->post("perfil_usu")
      );

      if($this->usuario->insertarUsuario($data)){
        $this->session->set_flashdata("confirmacion","Credenciales Creadas exitosamente. Inicia Sesion Por favor");


      }else{
        $this->session->set_flashdata("error","Error al procesar, intente nuevamente.");
      }
      redirect("");
    }

    public function insertarUsuario(){
  $data=array(
    "apellido_usu"=>$this->input->post("apellido_usu"),
    "nombre_usu"=>$this->input->post("nombre_usu"),
    "email_usu"=>$this->input->post("email_usu"),
    "password_usu"=>md5($this->input->post("password_usu")),
    "perfil_usu"=>$this->input->post("perfil_usu")
  );

  if($this->usuario->insertarUsuario($data)){
    $this->session->set_flashdata("confirmacion","Credenciales Creadas exitosamente. Inicia Sesion Por favor");


  }else{
    $this->session->set_flashdata("error","Error al procesar, intente nuevamente.");
  }
  redirect("");
}
public function eliminarUsuario(){
    $id_usu=$this->input->post("id_usu");
    if($this->usuario->eliminar($id_usu)){
      echo json_encode(array("respuesta"=>"ok"));
    }else{
      echo json_encode(array("respuesta"=>"error"));
    }
}

public function editar($id_usu){
  $data["usuario"]=$this->usuario->obtenerPorId($id_usu);
  $this->load->view("usuarios/editar",$data);
}

public function actualizarUsuarioAjax(){
    $id_usu=$this->input->post("id_usu");
    $data=array(
        "apellido_usu"=>$this->input->post("apellido_usu"),
        "nombre_usu"=>$this->input->post("nombre_usu"),
        "email_usu"=>$this->input->post("email_usu"),
        "perfil_usu"=>$this->input->post("perfil_usu")
    );
    if($this->usuario->actualizar($data,$id_usu)){
        echo json_encode(array("respuesta"=>"ok"));
    }else{
        echo json_encode(array("respuesta"=>"error"));
    }
}



      }//cierre de la clase
 ?>
