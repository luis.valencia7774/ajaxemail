<?php
    class Especialidades extends CI_Controller{
      public function __construct(){
        parent:: __construct();
        $this->load->model("especialidad");

      }

      public function index(){
        $data["listadoEspecialidades"]=$this->especialidad->consultarTodos();
        $this->load-> view("header");
        $this->load-> view("especialidades/index",$data);
        $this->load-> view("footer");
      }
      public function nuevo(){
        $data["listadoNosotros"]=$this->nosotro->consultarTodos();
        $this->load-> view("header");
        $this->load-> view("especialidades/nuevo",$data);
        $this->load-> view("footer");
      }
      public function editar($id_nos){
        $data["listadoNosotros"]=$this->nosotro->consultarTodos();
        $data["nosotro"]=$this->nosotro->consultarPorId($id_nos);
        $this->load-> view("header");
        $this->load-> view("especialidades/editar",$data);
        $this->load-> view("footer");
      }

      public function procesarActualizacion(){
        $id_med=$this->input->post("id_med");
        $datosMedicoEditado=array(
          "identificacion_med"=>$this->input->post("identificacion_med"),
          "apellido_med"=>$this->input->post("apellido_med"),
          "nombre_med"=>$this->input->post("nombre_med"),
        "especialidad_med"=>$this->input->post("especialidad_med"),
          "telefono_med"=>$this->input->post("telefono_med"),
          "direccion_med"=>$this->input->post("direccion_med"),
            "email_med"=>$this->input->post("email_med"),
            "estado_med"=>$this->input->post("estado_med")

        );

      if ($this->medico->actualizar($id_med,$datosMedicoEditado)) {
        // echo "INSERCION EXITOSA";
        redirect("medicos/index");
      }
      else {
        echo "ERROR AL INSERTAR";
            }
          }


      public function guardarMedico(){
        $datosNuevoMedico=array(
          "identificacion_med"=>$this->input->post("identificacion_med"),
          "apellido_med"=>$this->input->post("apellido_med"),
          "nombre_med"=>$this->input->post("nombre_med"),
        "especialidad_med"=>$this->input->post("especialidad_med"),
          "telefono_med"=>$this->input->post("telefono_med"),
          "direccion_med"=>$this->input->post("direccion_med"),
            "email_med"=>$this->input->post("email_med"),
            "estado_med"=>$this->input->post("estado_med")
        );
        //Logica de Negocio necesaria para subir la FOTOGRAFIA del cliente
          $this->load->library("upload");//carga de la libreria de subida de archivos
          $nombreTemporal="foto_medico_".time()."_".rand(1,5000);
          $config["file_name"]=$nombreTemporal;
          $config["upload_path"]=APPPATH.'../uploads/medicos/';
          $config["allowed_types"]="jpeg|jpg|png";
          $config["max_size"]=2*1024; //2MB
          $this->upload->initialize($config);
          //codigo para subir el archivo y guardar el nombre en la BDD
          if($this->upload->do_upload("foto_med")){
            $dataSubida=$this->upload->data();
            $datosNuevoMedico["foto_med"]=$dataSubida["file_name"];
          }

      if ($this->medico->insertar($datosNuevoMedico)) {
        // echo "INSERCION EXITOSA";
        $this->session->set_flashdata("confirmacion","Cliente insertado exitosamente.");
      }
      else {
        $this->session->set_flashdata("error","Error al procesar, intente nuevamente.");
      }
      redirect("medicos/index");
    }

    // FUNCIÒN PARA PROCESAR LA ELIMINACIÒN
    public function procesarEliminacion($id_med){
      if($this->medico->eliminar($id_med)){
        redirect("medicos/index");
      }else
        echo "ERROR AL ELIMINAR";
      }
} //Cierre de la clase
?>
