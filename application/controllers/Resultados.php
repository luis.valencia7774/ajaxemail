<?php
class Resultados extends CI_Controller{
  public function __construct(){

    parent::__construct();
    $this->load->model('resultado');
  }//cierre del constructor

public function index(){
  if ($this->session->userdata("c0nectadoUTC")) {

  }else{
    redirect("seguridades/formularioLogin");
  }
$data["listadoProductos"]=$this->resultado->consultarTodos();
$this->load->view('header1');
$this->load->view('resultados/index',$data);
$this->load->view('footer1');
}//cierre de la funcion Index
public function nuevo(){
  if ($this->session->userdata("c0nectadoUTC")) {

  }else{
    redirect("seguridades/formularioLogin");
  }
  $this->load->view('header1');
  $this->load->view('resultados/nuevo');
  $this->load->view('footer1');

}//cierre de la funcion nuevo

public function editar($id_pro2){
  if ($this->session->userdata("c0nectadoUTC")) {

  }else{
    redirect("seguridades/formularioLogin");
  }
  // $data["listadoProductos"]=$this->producto->consultarTodos();
  $data["producto"]=$this->resultado->consultarPorId($id_pro2);
  $this->load->view("header1");
  $this->load->view("resultados/editar",$data);
  $this->load->view("footer1");
}

public function guardarProducto(){
$datosNuevoProducto=array(
  "nombre_pro2"=>$this->input->post("nombre_pro2"),
  "cantidad_pro2"=>$this->input->post("cantidad_pro2"),
  "precio_pro2"=>$this->input->post("precio_pro2"),
  "descripcion2_pro"=>$this->input->post("descripcion2_pro")
  );

 if($this->resultado->insertar($datosNuevoProducto)){

   redirect('resultados/index');

 }else{
    echo"Error al insertar datos";

 }

}//cierre de la funcion guardarProducto

public function Eliminacion($id_pro2){
  if($this->resultado->eliminar($id_pro2)){
    redirect("resultados/index");

  }else{
    echo"Error al eliminar";
  }

}//cierre de la funcion Eliminacion

public function procesarActualizacion(){
$id_pro2=$this->input->post("id_pro2");
$datosProductoEditado=array(
  "nombre_pro2"=>$this->input->post("nombre_pro2"),
  "cantidad_pro2"=>$this->input->post("cantidad_pro2"),
  "precio_pro2"=>$this->input->post("precio_pro2"),
  "descripcion2_pro"=>$this->input->post("descripcion2_pro")
);

if($this->resultado->actualizar($id_pro2,$datosProductoEditado)){
//echo "INSERCION EXITOSA";
redirect("resultados/index");
}else{
echo "ERROR AL INSERTAR";
}

}

}

 ?>
