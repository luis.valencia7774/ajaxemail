<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class porcliente extends CI_Controller {

	 public function __construct(){
     //llamamos al constructor(el constructor no devuelve valores)
     parent::__construct();
     //cargamos el modelo en el controlador
     $this->load->model('empleado');
     $this->load->model('especialidadmedica');

   }//cierre del constructor

	public function index()
	{
		if ($this->session->userdata("c0nectadoUTC")) {

		}else{
			redirect("seguridades/formularioLogin");
		}
		$this->load->view("header5");
		$this->load->view('welcome_message1');
		$this->load->view("footer5");
	}




	public function productores()
	{

$data["listadoEmpleados"]=$this->empleado->consultarTodos1(7);
		$this->load->view("header5");
		$this->load->view('proveedores/index',$data);
		$this->load->view("footer5");
	}

	public function papas(){
$data["listadoEmpleados"]=$this->empleado->consultarTodos1(5);
		$this->load->view("header5");
		$this->load->view('proveedores/index',$data);
		$this->load->view("footer5");
	}

	public function brocoli(){
$data["listadoEmpleados"]=$this->empleado->consultarTodos1(8);
		$this->load->view("header5");
		$this->load->view('proveedores/index',$data);
		$this->load->view("footer5");
	}



	public function verproductores()
	{
		$data["listadoEmpleados"]=$this->empleado->consultarTodos1(7);
		$this->load->view("header5");
		$this->load->view('proveedores/verproductos',$data);
		$this->load->view("footer5");
	}
}
