<?php
  class Seguridades extends CI_Controller {

    function __construct() {
      parent::__construct();
      $this->load->model("usuario");
    }
//funcion que encarga de erenderizar
//la vista con el formulario de Login
    public function formularioLogin(){
      $this->load->view('header');
      $this->load->view("seguridades/formularioLogin");

    }

    public function recuperar(){

      $this->load->view("seguridades/recuperar");

    }


    //funcion que valida las credenciales ingresadas
    public function validarAcceso(){
      $email_usu=$this->input->post("email_usu");
      $password_usu=$this->input->post("password_usu");
      $usuario=$this->usuario->buscarUsuarioPorEmailPassword($email_usu,$password_usu);
      if($usuario){
        //cuando el email y contraseña son correctas
        if($usuario->estado_usu>0){//validando estado
          //si esta activo
          ///creando la variable desision con el nombre c0nectdoUTC
          $this->session->set_userdata("c0nectadoUTC",$usuario);
          $this->session->set_flashdata("bienvenida","Saludos, bienvenido al sistema");
          redirect("clientes/index");//la primera vista que vera el usuario
        }else {
          $this->session->set_flashdata("error","Usuario Bloqueado");
          redirect("seguridades/formularioLogin");
        }
      }else{//cuando no existe
        $this->session->set_flashdata("error","Email o contraseña incorrectas");
        redirect("seguridades/formularioLogin");
      }

    }

    public function validarAcceso1(){
      $email_usu=$this->input->post("email_usu");
      $password_usu=$this->input->post("password_usu");
      $usuario=$this->usuario->buscarUsuarioPorEmailPassword($email_usu,$password_usu);
      if($usuario){
        //cuando el email y contraseña son correctas
        if($usuario->estado_usu>0){//validando estado
          //si esta activo
          ///creando la variable desision con el nombre c0nectdoUTC
          $this->session->set_userdata("c0nectadoUTC",$usuario);
          $this->session->set_flashdata("bienvenida","Saludos, bienvenido al sistema");
          redirect("empleados/index");//la primera vista que vera el usuario
        }else {
          $this->session->set_flashdata("error","Usuario Bloqueado");
          redirect("seguridades/formularioLogin");
        }
      }else{//cuando no existe
        $this->session->set_flashdata("error","Email o contraseña incorrectas");
        redirect("seguridades/formularioLogin");
      }

    }
    public function cerrarSesion(){
      $this->session->sess_destroy();//Matando la sesiones
      redirect("seguridades/formularioLogin");
    }

    public function pruebaEmail(){
      enviarEmail("luis.valencia7774@utc.edu.ec","PRUEBA","<h1>HOLA</h1><i>Luis</i>");
    }

    public function recuperarPassword(){
          $email=$this->input->post("email");
          $usuario=$this->usuario->obtenerPorEmail($email);
          if($usuario){
          $password_aleatorio=rand(111111,999999);
          $asunto="RECUPERAR PASSWORD";
          $contenido=
          "Su contraseña temporal es: <b>$password_aleatorio</b>";
          enviarEmail($email,$asunto,$contenido);
          $data=array("password_usu"=>$password_aleatorio );
          $this->usuario->actualizar($data,$usuario->id_usu);
          $this->session->set_flashdata("confirmacion","Hemos enviado una clave temporal a su direccion de email");
        }else{
          $this->session->set_flashdata("error","El email ingresado no existe");
          }
        redirect("seguridades/formularioLogin");
        }

  }//cierre de la clase


 ?>
