<?php
      class Clientes extends CI_Controller{

        public function __construct(){
            parent::__construct();
            $this->load->model("cliente");
            $this->load->model("categoria");

        }

        public function index(){
          if ($this->session->userdata("c0nectadoUTC")) {

          }else{
            redirect("seguridades/formularioLogin");
          }
          $data["listadoClientes"]=$this->cliente->consultarTodos();
          $this->load->view("header1");
          $this->load->view("clientes/index",$data);
          $this->load->view("footer1");
        }
        public function nuevo(){
          if ($this->session->userdata("c0nectadoUTC")) {

      		}else{
      			redirect("seguridades/formularioLogin");
      		}
          $data["listadoCategorias"]=$this->categoria->consultarTodos();
          $this->load->view("header1");
          $this->load->view("clientes/nuevo",$data);
          $this->load->view("footer1");
        }
        public function crear(){

        $data["listadoCategorias"]=$this->categoria->consultarTodos();
          $this->load->view("header");
          $this->load->view("clientes/crear",$data);
          $this->load->view("footer");
        }

        public function carpinteros(){
      $data["listadoClientes"]=$this->cliente->consultarTodos1(23);
      		$this->load->view("header5");
      		$this->load->view('profesionales/index',$data);
      		$this->load->view("footer5");
      	}

        public function obreros(){
      $data["listadoClientes"]=$this->cliente->consultarTodos1(24);
      		$this->load->view("header5");
      		$this->load->view('profesionales/index',$data);
      		$this->load->view("footer5");
      	}
        public function electricistas(){
      $data["listadoClientes"]=$this->cliente->consultarTodos1(22);
      		$this->load->view("header5");
      		$this->load->view('profesionales/index',$data);
      		$this->load->view("footer5");
      	}

        public function domestica(){
      $data["listadoClientes"]=$this->cliente->consultarTodos1(25);
      		$this->load->view("header5");
      		$this->load->view('profesionales/index',$data);
      		$this->load->view("footer5");
      	}



        public function editar($id_cli){
          if ($this->session->userdata("c0nectadoUTC")) {

      		}else{
      			redirect("seguridades/formularioLogin");
      		}
          $data["listadoCategorias"]=$this->categoria->consultarTodos();
          $data["cliente"]=$this->cliente->consultarPorId($id_cli);
          $this->load->view("header1");
          $this->load->view("clientes/editar",$data);
          $this->load->view("footer1");
        }

        public function guardarCliente(){
            $datosNuevoCliente=array(
                "identificacion_cli"=>$this->input->post("identificacion_cli"),
                "apellido_cli"=>$this->input->post("apellido_cli"),
                "nombre_cli"=>$this->input->post("nombre_cli"),
                "telefono_cli"=>$this->input->post("telefono_cli"),
                "direccion_cli"=>$this->input->post("direccion_cli"),
                "email_cli"=>$this->input->post("email_cli"),
                "estado_cli"=>$this->input->post("estado_cli"),
                "fk_id_hcat"=>$this->input->post("fk_id_hcat"),
                "foto_cli"=>$this->input->post("foto_cli")
            );
            //logica de negecio necesari para subir la fotografia del clientes
                $this->load->library("upload"); //carga de la liberia de subida  alaterio
                $nombreTemporal="foto_cliente".time()."_".rand(1,5000);// generar un nombre aleatorio
                $config["file_name"]=$nombreTemporal;
                $config["upload_path"]=APPPATH.'../uploads/cliente/'; //LA RUTA
                $config["allowed_types"]="jpeg|jpg|png";// el tipo de formato
                $config["max_size"]=2*1024;//2MB PARA EL PESO
                $this->upload->initialize($config);
                if ($this->upload->do_upload("foto_cli")) {
                  $dataSubida=$this->upload->data();
                  $datosNuevoCliente["foto_cli"]=$dataSubida["file_name"];//no es mane
                  // code...
                }

            if($this->cliente->insertar($datosNuevoCliente)){
                //echo "INSERCION EXITOSA";
                $this->session->set_flashdata("confirmacion","Cliente insertado exitosamente.");
            }else{

                $this->session->set_flashdata("error","Error al procesar, intente nuevamente.");

            }

            redirect("clientes/index");
        }

        public function guardarCliente1(){
            $datosNuevoCliente=array(
                "identificacion_cli"=>$this->input->post("identificacion_cli"),
                "apellido_cli"=>$this->input->post("apellido_cli"),
                "nombre_cli"=>$this->input->post("nombre_cli"),
                "telefono_cli"=>$this->input->post("telefono_cli"),
                "direccion_cli"=>$this->input->post("direccion_cli"),
                "email_cli"=>$this->input->post("email_cli"),
                "estado_cli"=>$this->input->post("estado_cli"),
                "fk_id_hcat"=>$this->input->post("fk_id_hcat"),
                "foto_cli"=>$this->input->post("foto_cli")
            );
            //logica de negecio necesari para subir la fotografia del clientes
                $this->load->library("upload"); //carga de la liberia de subida  alaterio
                $nombreTemporal="foto_cliente".time()."_".rand(1,5000);// generar un nombre aleatorio
                $config["file_name"]=$nombreTemporal;
                $config["upload_path"]=APPPATH.'../uploads/cliente/'; //LA RUTA
                $config["allowed_types"]="jpeg|jpg|png";// el tipo de formato
                $config["max_size"]=2*1024;//2MB PARA EL PESO
                $this->upload->initialize($config);
                if ($this->upload->do_upload("foto_cli")) {
                  $dataSubida=$this->upload->data();
                  $datosNuevoCliente["foto_cli"]=$dataSubida["file_name"];//no es mane
                  // code...
                }

            if($this->cliente->insertar($datosNuevoCliente)){
                //echo "INSERCION EXITOSA";
                $this->session->set_flashdata("confirmacion","Usuario insertado exitosamente.");
            }else{

                $this->session->set_flashdata("error","Error al procesar, intente nuevamente.");

            }

            redirect("usuarios/crear");
        }

        public function procesarEliminacion($id_cli){
          if ($this->cliente->eliminar($id_cli)) {
            $this->session->set_flashdata('eliminacion',"Paciente eliminado exitosamente.");
          }else{
            $this->session->set_flashdata("error","Error al procesar, intente nuevamente.");
          }
          redirect("clientes/index");
        }

        public function procesarActualizacion(){
          $id_cli=$this->input->post("id_cli");
          $datosClienteEditado=array(
              "identificacion_cli"=>$this->input->post("identificacion_cli"),
              "apellido_cli"=>$this->input->post("apellido_cli"),
              "nombre_cli"=>$this->input->post("nombre_cli"),
              "telefono_cli"=>$this->input->post("telefono_cli"),
              "direccion_cli"=>$this->input->post("direccion_cli"),
              "email_cli"=>$this->input->post("email_cli"),
              "estado_cli"=>$this->input->post("estado_cli"),
              "fk_id_hcat"=>$this->input->post("fk_id_hcat"),
              "foto_cli"=>$this->input->post("foto_cli")
          );
          $this->load->library("upload"); //carga de la liberia de subida  alaterio
          $nombreTemporal="foto_cli".time()."_".rand(1,5000);// generar un nombre aleatorio
          $config["file_name"]=$nombreTemporal;
          $config["upload_path"]=APPPATH.'../uploads/cliente/'; //LA RUTA
          $config["allowed_types"]="jpeg|jpg|png";// el tipo de formato
          $config["max_size"]=2*1024;//2MB PARA EL PESO
          $this->upload->initialize($config);
          if ($this->upload->do_upload("foto_cli")) {
            $dataSubida=$this->upload->data();
            $foto = $this->cliente->capturarImagen($id_cli);
            unlink('./uploads/cliente/'.$foto->foto_cli);
            $datosClienteEditado["foto_cli"]=$dataSubida["file_name"];//no es mane
            // code...
          }


          if($this->cliente->actualizar($id_cli,$datosClienteEditado)){
            $this->session->set_flashdata('edicion',"Paciente editado exitosamente.");
          }else{
            $this->session->set_flashdata("error","Error al procesar intente nuevamente.");
          }
          redirect("clientes/index");
         }

        }//cierre de la clase
?>
