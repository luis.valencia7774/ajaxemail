<?php
    class Departamentos extends CI_Controller{
      public function __construct(){
        parent:: __construct();
        $this->load->model("departamento");
      
      }

      public function index(){
        $data["listadoDepartamentos"]=$this->departamento->consultarTodos();
        $this->load-> view("header");
        $this->load-> view("departamentos/index",$data);
        $this->load-> view("footer");
      }
      public function nuevo(){
        $data["listadoDepartamentos"]=$this->departamento->consultarTodos();
        $this->load-> view("header");
        $this->load-> view("departamentos/nuevo",$data);
        $this->load-> view("footer");
      }
      public function editar($id_dep){
        $data["listadoDepartamentos"]=$this->departamento->consultarTodos();
        $data["departamento"]=$this->departamento->consultarPorId($id_dep);
        $this->load-> view("header");
        $this->load-> view("departamentos/editar",$data);
        $this->load-> view("footer");
      }

      public function procesarActualizacion(){
        $id_dep=$this->input->post("id_dep");
        $datosDepartamentoEditado=array(
          "codigo_dep"=>$this->input->post("codigo_dep"),
        "nombre_dep"=>$this->input->post("nombre_dep"),
        "servicios_dep"=>$this->input->post("servicios_dep"),
          "telefono_dep"=>$this->input->post("telefono_dep"),
          "email_dep"=>$this->input->post("email_dep"),
            "estado_dep"=>$this->input->post("estado_dep")

        );

      if ($this->departamento->actualizar($id_dep,$datosDepartamentoEditado)) {
        // echo "INSERCION EXITOSA";
        redirect("departamentos/index");
      }
      else {
        echo "ERROR AL INSERTAR";
            }
          }


      public function guardarDepartamento(){
        $datosNuevoDepartamento=array(
          "codigo_dep"=>$this->input->post("codigo_dep"),
        "nombre_dep"=>$this->input->post("nombre_dep"),
        "servicios_dep"=>$this->input->post("servicios_dep"),
          "telefono_dep"=>$this->input->post("telefono_dep"),
          "email_dep"=>$this->input->post("email_dep"),
            "estado_dep"=>$this->input->post("estado_dep")

        );
        //Logica de Negocio necesaria para subir la FOTOGRAFIA del cliente


      if ($this->departamento->insertar($datosNuevoDepartamento)) {
        // echo "INSERCION EXITOSA";
        $this->session->set_flashdata("confirmacion","Cliente insertado exitosamente.");
      }
      else {
        $this->session->set_flashdata("error","Error al procesar, intente nuevamente.");
      }
      redirect("departamentos/index");
    }

    // FUNCIÒN PARA PROCESAR LA ELIMINACIÒN
    public function procesarEliminacion($id_dep){
if ($this->session->userdata("c0nectadoUTC")->perfil_usu=="ADMINISTRADOR") {
      if($this->departamento->eliminar($id_dep)){
        redirect("departamentos/index");
      }else{
        echo "ERROR AL ELIMINAR";
      }
    } else {
          redirect("seguridades/formularioLogin");
        }
    }

} //Cierre de la clase
?>
