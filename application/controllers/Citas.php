<?php
    class Citas extends CI_Controller{
      public function __construct(){
        parent:: __construct();
        $this->load->model("cita");

      }

      public function index(){
        $data["listadoCitas"]=$this->cita->consultarTodos();
        $this->load-> view("header");
        $this->load-> view("citas/index",$data);
        $this->load-> view("footer");
      }
      public function nuevo(){
        $data["listadoCitas"]=$this->cita->consultarTodos();
        $this->load-> view("header");
        $this->load-> view("citas/nuevo",$data);
        $this->load-> view("footer");
      }
      public function editar($id_cit){
        $data["listadoCitas"]=$this->cita->consultarTodos();
        $data["cita"]=$this->cita->consultarPorId($id_cit);
        $this->load-> view("header");
        $this->load-> view("citas/editar",$data);
        $this->load-> view("footer");
      }

      public function procesarActualizacion(){
        $id_cit=$this->input->post("id_cit");
        $datosCitaEditado=array(
          "identificacion_cit"=>$this->input->post("identificacion_cit"),
          "apellido_cit"=>$this->input->post("apellido_cit"),
          "nombre_cit"=>$this->input->post("nombre_cit"),
          "telefono_cit"=>$this->input->post("telefono_cit"),
          "servicios_cit"=>$this->input->post("servicios_cit")


        );

      if ($this->cita->actualizar($id_cit,$datosCitaEditado)) {
        // echo "INSERCION EXITOSA";
        redirect("citas/index");
      }
      else {
        echo "ERROR AL INSERTAR";
            }
          }


      public function guardarCita(){
        $datosNuevoCita=array(
          "identificacion_cit"=>$this->input->post("identificacion_cit"),
          "apellido_cit"=>$this->input->post("apellido_cit"),
          "nombre_cit"=>$this->input->post("nombre_cit"),
          "telefono_cit"=>$this->input->post("telefono_cit"),
          "servicios_cit"=>$this->input->post("servicios_cit")
        );
        //Logica de Negocio necesaria para subir la FOTOGRAFIA del cliente


      if ($this->cita->insertar($datosNuevoCita)) {
        // echo "INSERCION EXITOSA";
        $this->session->set_flashdata("confirmacion","Cliente insertado exitosamente.");
      }
      else {
        $this->session->set_flashdata("error","Error al procesar, intente nuevamente.");
      }
      redirect("");
    }

    // FUNCIÒN PARA PROCESAR LA ELIMINACIÒN
    public function procesarEliminacion($id_cit){

      if($this->cita->eliminar($id_cit)){
        redirect("citas/index");
      }else{
        echo "ERROR AL ELIMINAR";
      }
    }

} //Cierre de la clase
?>
