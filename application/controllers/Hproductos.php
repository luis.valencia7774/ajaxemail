<?php
      class Hproductos extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model("hproducto");
            $this->load->model("hcategoria");
        }

        public function index(){
          if ($this->session->userdata("c0nectadoUTC")) {

      		}else{
      			redirect("seguridades/formularioLogin");
      		}
          $data["listadoHproductos"]=$this->hproducto->consultarTodos();
          $this->load->view("header1");
          $this->load->view("hproductos/index",$data);
          $this->load->view("footer1");
        }
        public function nuevo(){
          if ($this->session->userdata("c0nectadoUTC")) {

      		}else{
      			redirect("seguridades/formularioLogin");
      		}
          $data["listadoHcategorias"]=$this->hcategoria->consultarTodos();
          $this->load->view("header1");
          $this->load->view("hproductos/nuevo",$data);
          $this->load->view("footer1");
        }
        public function editar($id_hpro){
          if ($this->session->userdata("c0nectadoUTC")) {

      		}else{
      			redirect("seguridades/formularioLogin");
      		}
          $data["listadoHcategorias"]=$this->hcategoria->consultarTodos();
          $data["hproducto"]=$this->hproducto->consultarPorId($id_hpro);
          $this->load->view("header1");
          $this->load->view("hproductos/editar",$data);
          $this->load->view("footer1");
        }

        public function guardarhproducto(){
            $datosNuevoHproducto=array(
                "nombre_hpro"=>$this->input->post("nombre_hpro"),
                "cantidad_hpro"=>$this->input->post("cantidad_hpro"),
                "precio_hpro"=>$this->input->post("precio_hpro"),
                "descripcion_hpro"=>$this->input->post("descripcion_hpro"),
                "fk_id_hcat"=>$this->input->post("fk_id_hcat"),
                "doc_hpro"=>$this->input->post("doc_hpro")
            );
            $this->load->library("upload"); //carga de la liberia de subida  alaterio
            $nombreTemporal="doc_hpro_".time()."_".rand(1,5000);// generar un nombre aleatorio
            $config["file_name"]=$nombreTemporal;
            $config["upload_path"]=APPPATH.'../uploads/documentos/'; //LA RUTA
            $config["allowed_types"]="pdf";// el tipo de formato
            $config["max_size"]=5*1024;//2MB PARA EL PESO
            $this->upload->initialize($config);
            if ($this->upload->do_upload("doc_hpro")) {
              $dataSubida=$this->upload->data();
              $datosNuevoHproducto["doc_hpro"]=$dataSubida["file_name"];//no es mane
              // code...
            }
            if($this->hproducto->insertar($datosNuevoHproducto)){
                //echo "INSERCION EXITOSA";
                $this->session->set_flashdata("confirmacion","Examen Clinico insertado exitosamente.");
            }else{

                $this->session->set_flashdata("error","Error al procesar, intente nuevamente.");

            }

            redirect("hproductos/index");
        }

        public function procesarEliminacion($id_hpro){
          if ($this->hproducto->eliminar($id_hpro)) {
            $this->session->set_flashdata('eliminacion',"Examen Clinico eliminado exitosamente.");
          }else{
            $this->session->set_flashdata("error","Error al procesar intente nuevamente.");
          }
          redirect("hproductos/index");
        }

        public function procesarActualizacion(){
          $id_hpro=$this->input->post("id_hpro");
          $datoshproductoEditado=array(
              "nombre_hpro"=>$this->input->post("nombre_hpro"),
              "cantidad_hpro"=>$this->input->post("cantidad_hpro"),
              "precio_hpro"=>$this->input->post("precio_hpro"),
              "descripcion_hpro"=>$this->input->post("descripcion_hpro"),
              "fk_id_hcat"=>$this->input->post("fk_id_hcat"),
              "doc_hpro"=>$this->input->post("doc_hpro")
          );
          $this->load->library("upload"); //carga de la liberia de subida  alaterio
          $nombreTemporal="doc_hpro_".time()."_".rand(1,5000);// generar un nombre aleatorio
          $config["file_name"]=$nombreTemporal;
          $config["upload_path"]=APPPATH.'../uploads/documentos/'; //LA RUTA
          $config["allowed_types"]="pdf";// el tipo de formato
          $config["max_size"]=5*1024;//2MB PARA EL PESO
          $this->upload->initialize($config);
          if ($this->upload->do_upload("doc_hpro")) {
            $dataSubida=$this->upload->data();
            $doc=$this->empleado->capturarImagen($id_hpro);
            unlink('./uploads/documentos/'.$doc->doc_hpro);
            $datosHproductoEditado["doc_hpro"]=$dataSubida["file_name"];//no es mane
            // code...
          }

          if($this->hproducto->actualizar($id_hpro,$datoshproductoEditado)){
              $this->session->set_flashdata('edicion',"Examen Clinico editado exitosamente.");
          }else{
              $this->session->set_flashdata("error","Error al procesar intente nuevamente.");
          }
          redirect("hproductos/index");
        }



    }//cierre de la clase
?>
